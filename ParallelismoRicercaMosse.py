#script python che contiene le classi per gestire il parallelismo della ricerca delle mosse
from __future__ import annotations

from threading import Thread
from typing import Any, Callable, Iterable, List, Mapping

from EsitoAlgoritmoRicerca import EsitoAlgoritmoRicerca

class ThreadRicercaMosse(Thread):
	#finto thread per la ricerca delle mosse
	flag_terminato:bool
	esito_ricerca : EsitoAlgoritmoRicerca

	def __init__(self, group: None = ..., target: Callable[..., Any] | None = ..., name: str | None = ..., args: Iterable[Any] = ..., kwargs: Mapping[str, Any] | None = ..., *, daemon: bool | None = ...) -> None:
		super().__init__(group=group, target=target, name=name, args=list(args) + [self] , kwargs=kwargs, daemon=daemon) #agli argomenti ci metto il thread stesso perche dopo l'esito viene ripescato da qui
		self.flag_terminato = False

	def run(self) -> None:
		super().run()
		self.flag_terminato = True

class ListaThreadRicercaMosse:
	#classe che contiene la lista di tutti i thread 
	lista_thread : List[ThreadRicercaMosse]

	def __init__(self) -> None:
		self.lista_thread = []

	def aggiungi_thread()


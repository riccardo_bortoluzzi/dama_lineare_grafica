#script che contiene la classe per gestire una singola casella della tabella

from __future__ import annotations
from typing import List, Tuple

import Giocatore

class Casella:
	r:int #riga della casella
	c: int #colonna della casella
	dim_v : int #dimensione verticale dello schema
	dim_o : int #dimensione orizzontale dello schema
	is_pedina : bool #flag che indica se nella casella e presente una pedina
	is_generatrice : bool #flag che indica se la pedina e una generatrice o meno
	giocatore_appartenenza : Giocatore.Giocatore #giocatore a cui appartiene la pedina

	def __init__(self, r:int, c:int, dim_v:int, dim_o:int) -> None:
		#metodo costruttore

	#############################################################################################################################################################################
	#metodi generali

	def inserisci_pedina(self, giocatore:Giocatore.Giocatore, flag_generatrice:bool):
		#metodo per inserire una pedina nella casella
		self.is_pedina = True
		self.is_generatrice = flag_generatrice
		self.giocatore_appartenenza = giocatore
	
	def rimuovi_pedina(self):
		#metodo per rimuovere una pedina dalla casella
		self.is_pedina = False
		
	def copy(self) -> Casella:
		#metodo per copiare la casella attuale
	

	def calcola_mosse_disponibili_pedina_mangiare(self, schema_gioco:List[List[Casella]], flag_mangiare_proprie_pedine:bool) -> List[Tuple[int, int]]:
		#calcolo la lista delle mosse che puo eseguire la pedina per mangiare
		#se non e una poedina allora dico subito che non ha mosse disponibili
		if not(self.is_pedina):
			return []
		#mi calcolo le dimensioni dello schema
		dim_v = len(schema_gioco)
		dim_o = len(schema_gioco[0])
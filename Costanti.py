#script che contiene le costanti per il gioco della dama lineare

import os

cartella_attuale = os.path.abspath('.')
cartella_file = os.path.abspath(os.path.dirname(__file__))

lista_colori = [ 'green', 'red', 'blue', 'black']
lista_colori_selezione = ['lightgreen', 'pink', 'cyan', 'gray']
lista_traduzione_colori = ['Verde', 'Rosso', 'Blu', 'Nero']
colore_sfondo_default = 'lightgray'#'white'
colore_selezione_casella = 'yellow'

numero_casella_vuota = -1

nome_immagine_pedina = 'pedina'
nome_immagine_generatrice = 'generatrice'
cartella_immagini = os.path.join(cartella_file, 'immagini_pedine')
nome_file_immagine_pedina = os.path.join( cartella_immagini, 'pedina_{colore}.png' )
nome_file_immagine_generatrice = os.path.join( cartella_immagini, 'generatrice_{colore}.png' )
nome_file_immagine_damone = os.path.join( cartella_immagini, 'damone_{colore}.png' )

id_algoritmo_min_max = 0
id_algoritmo_simulazione_mosse_giocatori = 1
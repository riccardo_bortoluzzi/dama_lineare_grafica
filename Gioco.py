#script che gestisce il gioco della dama lineare

from __future__ import annotations
from typing import List, Tuple
import random

import math

import Giocatore, Costanti, EsitoRicercaListaMosse
from EsitoAlgoritmoRicerca import EsitoAlgoritmoRicerca
from NumeroPedineGiocatori import NumeroPedineGiocatori

class Gioco:
	dim_v: int
	dim_o : int
	lista_giocatori : List[Giocatore.Giocatore] #ogni giocatore e identificato da un numero e le pedine sono nel modo: len(giocatori)*valore + id_giocatore
	valore_pedina_generatrice : int
	valore_pedina_semplice : int
	schema_gioco : List[List[int]]
	id_algoritmo_ricerca : int
	valore_eliminazione_giocatore : int
	valore_vincita_partita : int
	numero_pedine_prima_generatrice : int
	flag_mangiare_proprie_pedine : bool
	flag_obbligatorieta_mangiare : bool
	id_partita : int #id che indica quale partita si sta giocando
	valore_moltiplicatore_mangiata : float #valore per cui moltiplicare il valore della pedian quando si mangia
	valore_generatrice_piu_vicina : float
	valore_occupazione_casa_base : int #valore che indica il punteggio da assegnare se si occupa la casa base altrui o la proproa
	valore_senza_generatrici : int #valore che indica se un giocatore rimane senza generatrici
	flag_pesa_punteggi_numero_pedine : bool #flag che indica se pesare il valore delle pedine con quelle che complessivamente si ha nel campo di gioco, dovrebbe permettere di avere una strategia più aggressiva se si ha tante pedine, 


	def __init__(self, 
					dim_v:int, 
					dim_o:int, 
					numero_giocatori_umani:int, 
					numero_giocatori_pc:int, 
					valore_semplice:int, 
					valore_generatrice:int, 
					id_algoritmo:int, 
					valore_eliminazione_giocatore:int, 
					valore_vincita_partita:int, 
					pedine_prima_generatrice:int, 
					mangiare_proprie_pedine: bool, 
					obbligatorio_mangiare : bool, 
					id_partita:int, 
					valore_moltiplicatore:float, 
					generatrice_piu_vicina:float, 
					valore_occupazione_casa_base:int,
					valore_senza_generatrici : int,
					flag_pesa_punteggi:bool
				) -> None:
		self.dim_o = dim_o
		self.dim_v= dim_v
		self.valore_pedina_generatrice = valore_generatrice
		self.valore_pedina_semplice = valore_semplice
		self.id_algoritmo_ricerca = id_algoritmo
		self.valore_eliminazione_giocatore = valore_eliminazione_giocatore
		self.valore_vincita_partita = valore_vincita_partita
		self.numero_pedine_prima_generatrice = pedine_prima_generatrice
		self.flag_mangiare_proprie_pedine = mangiare_proprie_pedine
		self.flag_obbligatorieta_mangiare = obbligatorio_mangiare
		self.id_partita = id_partita
		self.valore_moltiplicatore_mangiata = valore_moltiplicatore
		self.valore_generatrice_piu_vicina = generatrice_piu_vicina
		self.valore_occupazione_casa_base = valore_occupazione_casa_base
		self.valore_senza_generatrici = valore_senza_generatrici
		self.flag_pesa_punteggi_numero_pedine = flag_pesa_punteggi
		#mi creo lo schema di gioco
		self.schema_gioco = [ [ Costanti.numero_casella_vuota for c in range(self.dim_o) ] for r in range(self.dim_o) ]
		#adesso mi creo i giocatori in base a rtegole prefissate: il primo che muove sarà sempre un umano, poi un pc, poi un umano eventualmente
		#mi calcolo inizialmente la lista delle possibili case basi in relazione al numero di giocatori
		numero_totale_giocatori = numero_giocatori_umani + numero_giocatori_pc
		if numero_totale_giocatori == 2:
			lista_case_basi = [(self.dim_v-1, self.dim_o-1), (0,0)]
		else:
			#ho piu di 2 giocatori
			lista_case_basi = [ (self.dim_v-1, self.dim_o-1), (0, self.dim_o-1), (0,0), (self.dim_v-1, 0) ]
		#adesso creo la lista dei giocatori
		self.lista_giocatori = []
		while (numero_giocatori_pc + numero_giocatori_umani) >0:
			#parto a quello che ne ha di piu oppure dall'umano se hanno lo stesso numero
			id_giocatore = len(self.lista_giocatori)
			if numero_giocatori_pc > numero_giocatori_umani:
				#significa che il pc ne ha di piu o hanno lo stesso numero
				nuovo_giocatore = Giocatore.Giocatore(id_giocatore=id_giocatore, casa_base=lista_case_basi[id_giocatore], is_umano=False)
				self.lista_giocatori.append(nuovo_giocatore)
				numero_giocatori_pc -=1
			else:
				#gli umani ne hanno di piu
				nuovo_giocatore = Giocatore.Giocatore(id_giocatore=id_giocatore, casa_base=lista_case_basi[id_giocatore], is_umano=True)
				self.lista_giocatori.append(nuovo_giocatore)
				numero_giocatori_umani -=1
			
		#qui dovrei aver completato la lista dei giocatori
		#inserisco le pedine in casa base generatrici
		for i in range(len(self.lista_giocatori)):
			#i e l'id del giocatore
			(r,c) = self.lista_giocatori[i].casa_base
			#inserisco la generatrice nello schema
			self.schema_gioco[r][c] = len(self.lista_giocatori)*self.valore_pedina_generatrice + i

	############################################################################################################################################################################################
	#metodi generali
	def verifica_giocatori_eliminati(self):
		#metodo per verificare i giocatori sconfitti
		pass

	def verifica_partita_vinta(self) -> bool:
		#metodo per verificare se rimane un solo giocatore in campo
		id_giocatore_trovato = -1
		for r in range(self.dim_v):
			for c in range(self.dim_o):
				valore_casella = self.schema_gioco[r][c]
				if valore_casella != Costanti.numero_casella_vuota:
					#verifico il giocatore di appartenenza
					giocatore_appartenenza = valore_casella % len(self.lista_giocatori)
					if id_giocatore_trovato == -1:
						#allora me lo salvo in quella variabile
						id_giocatore_trovato = giocatore_appartenenza
					elif id_giocatore_trovato != giocatore_appartenenza:
						#ci sono almeno 2 giocatori
						return False
		#se arrivo qui non ho trovato altri giocatori -> dico che la partita e vinta
		return True


	################################################################################################################################################################################
	#metodo per rimuovere dallo schema tutte le caselle del giocatore
	def rimuovi_pedine_giocatore(self, id_giocatore:int):
		#metodo per rimuopvere dallo schema tutte le caselle del giocatore selezionato
		for r_eli in range(self.dim_v):
			for c_eli in range(self.dim_o):
				valore_pedina = self.schema_gioco[r_eli][c_eli]
				if (valore_pedina != Costanti.numero_casella_vuota) and ((valore_pedina%len(self.lista_giocatori) ) == id_giocatore):
					#elimino quella pedina
					self.schema_gioco[r_eli][c_eli] = Costanti.numero_casella_vuota

	##############################################################################################################################################################################################
	#metodi per eseguire le mosse e fare i controlli necessari
	def esegui_mossa(self, origine:Tuple[int, int], destinazione:Tuple[int, int]) -> float:
		#metodo per eseguire una mossa e ritornare il punteggio accumulato per eseguire quella mossa
		#questo metodo si occupa anche di inserire la pedina nuova se necessario
		#mi ricavo il valore da spostare
		(r_o, c_o) = origine
		valore_spostamento = self.schema_gioco[r_o][c_o]
		#mi ricavo il giocatore di appartenenza 
		id_giocatore_appartenenza = valore_spostamento%len(self.lista_giocatori)
		tipo_pedina = valore_spostamento // len(self.lista_giocatori)
		#valuto se in questo caso mi conviene calcolare il numero delle pedine o no
		if self.flag_pesa_punteggi_numero_pedine:
			pedine_giocatori = NumeroPedineGiocatori(schema=self.schema_gioco, numero_giocatori=len(self.lista_giocatori))
		else:
			#me ne creo uno vuoto
			pedine_giocatori = NumeroPedineGiocatori(schema=[[]], numero_giocatori=0)
		#sposto la pedina
		self.schema_gioco[r_o][c_o] = Costanti.numero_casella_vuota
		(r_d, c_d) = destinazione
		self.schema_gioco[r_d][c_d] = valore_spostamento
		punteggio = 0
		#per prima cosa verifico se ho mangiato
		if ( abs(r_d - r_o) + abs(c_d - c_o) ) > 1:
			#significa che ho mangiato
			#qui controllo anche se ha vinto o eliminato un giocatore
			#mi ricavo la pedina mangiata
			valore_pedina_mangiata = self.schema_gioco[(r_o + r_d)//2][(c_o + c_d)//2]
			#svuoto il valore
			self.schema_gioco[(r_o + r_d)//2][(c_o + c_d)//2] = Costanti.numero_casella_vuota
			#mi ricavo il giocatore a cui e stata mangiata la pedina
			giocatore_perdita_subita = valore_pedina_mangiata%len(self.lista_giocatori)
			valore_perdita = valore_pedina_mangiata//len(self.lista_giocatori)
			if giocatore_perdita_subita == id_giocatore_appartenenza:
				#ho un punteggio negativo
				if self.flag_pesa_punteggi_numero_pedine:
					punteggio -= valore_perdita*self.valore_moltiplicatore_mangiata*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=giocatore_perdita_subita)
				else:
					punteggio -= valore_perdita*self.valore_moltiplicatore_mangiata
				#se ha mangiato l'ultima generatrice tolgo il punteggio relativo
				if valore_perdita == self.valore_pedina_generatrice:
					if self.lista_giocatori[giocatore_perdita_subita].verifica_giocatore_senza_generatrici(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori), valore_generatrice=self.valore_pedina_generatrice):
						#il giocatore a cui e stata mangiata una generatrice ha perso tutte le generatrici
						punteggio -= self.valore_senza_generatrici
			else:
				#ho un punteggio positivo
				if self.flag_pesa_punteggi_numero_pedine:
					punteggio += valore_perdita*self.valore_moltiplicatore_mangiata*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=giocatore_perdita_subita)
				else:
					punteggio += valore_perdita*self.valore_moltiplicatore_mangiata
				#adesso devo valutare se ha eliminato il giocatore
				if self.lista_giocatori[giocatore_perdita_subita].verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
					#allora significa che ho eliminato il giocatore
					punteggio += self.valore_eliminazione_giocatore
					#a questo punto verifico se ho vinto la partita
					if self.verifica_partita_vinta():
						#aggiungo il punteggio di vincita poartita
						punteggio += self.valore_vincita_partita
				#adesso valuto se ha eliminato tutte le generatrici per quel giocatore
				if valore_perdita == self.valore_pedina_generatrice:
					if self.lista_giocatori[giocatore_perdita_subita].verifica_giocatore_senza_generatrici(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori), valore_generatrice=self.valore_pedina_generatrice):
						#il giocatore a cui e stata mangiata una generatrice ha perso tutte le generatrici
						punteggio += self.valore_senza_generatrici
		#verifico se devo inserire una pedina
		if (tipo_pedina == self.valore_pedina_generatrice) and (destinazione == self.lista_giocatori[id_giocatore_appartenenza].casa_base):
			#allora devo inserire una nuova pedina
			#mi calcolo la coordinata dove inserire la nuova pedina
			(r_aggiunta, c_aggiunta) = self.lista_giocatori[id_giocatore_appartenenza].calcola_cella_libera_per_pedina_aggiunta(tabella_gioco=self.schema_gioco)
			#vado ad incrementare il numero di pedine per quel giocatore
			self.lista_giocatori[id_giocatore_appartenenza].pedine_aggiunte_dopo_ultima_aggiunta += 1
			#verifico il tipo di pedina da aggiungere
			if self.lista_giocatori[id_giocatore_appartenenza].pedine_aggiunte_dopo_ultima_aggiunta == self.numero_pedine_prima_generatrice:
				#devo aggiungere una generatrice
				valore_pedina_aggiunta = self.valore_pedina_generatrice
				#resetto il valore del giocatore
				self.lista_giocatori[id_giocatore_appartenenza].pedine_aggiunte_dopo_ultima_aggiunta = 0
			else:
				#altrimenti devo aggiungere una pedina normale
				valore_pedina_aggiunta = self.valore_pedina_semplice
			#incremento il punteggio
			if self.flag_pesa_punteggi_numero_pedine:
				punteggio += valore_pedina_aggiunta*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=id_giocatore_appartenenza)
			else:
				punteggio += valore_pedina_aggiunta
			#adesso inserisco la nuova pedina
			self.schema_gioco[r_aggiunta][c_aggiunta] = valore_pedina_aggiunta*len(self.lista_giocatori) + id_giocatore_appartenenza
		#posso ritornare il punteggio raggiunto
		return punteggio

	#################################################################################################################################################################################
	#metodo per calcolare la mossa del pc
	def calcola_mossa_pc(self, giocatore_attuale:int, profondita:int) -> EsitoAlgoritmoRicerca:
		#metodo che in base all'algoritmo scelto ritorna l'esito della mossa scelta dal pc
		#mi ripristino lo schema anche qui
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		if self.id_algoritmo_ricerca == 0:
			#algoritmo min max
			#migliore_esito = self.algoritmo_min_max(profondita=profondita, id_giocatore=giocatore_attuale)
			#migliore_esito_multigiocatore = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_attuale)
			#verifico i punteggi 
			#print(f'Punteggio clas: {migliore_esito.punteggio}' )
			#print(f'punteggio mult: {migliore_esito_multigiocatore.punteggio}')
			migliore_esito = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=False)
		elif self.id_algoritmo_ricerca == 1:
			#chiamo l'altro algoritmo di ricerca  con simulazione e calcolo dalle matrici
			migliore_esito = self.algoritmo_valuta_schema_profondita(profondita=profondita, id_giocatore=giocatore_attuale)
		elif self.id_algoritmo_ricerca == 2:
			#algoritmo numero mosse per vincere
			migliore_esito = self.algoritmo_mosse_per_vincere(profondita=profondita, id_giocatore=giocatore_attuale)
		elif self.id_algoritmo_ricerca == 3:
			migliore_esito = self.algoritmo_media_punteggio(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=False)
		elif self.id_algoritmo_ricerca == 4:
			migliore_esito = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=True)
		elif self.id_algoritmo_ricerca == 5:
			migliore_esito = self.algoritmo_media_punteggio(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=True)
		else:
			raise Exception('Algoritmo non identificato')
		#ripristino lo schema
		#ripristino lo schema e i numeri
		self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		#ritorno il punteggio calcolato
		return migliore_esito

	#########################################################################################################################################################################################
	#metodi per calcolare le mosse del computer nei vari casi
	def ripristina_schema(self, schema_bk:List[List[int]], numero_pedine:List[int], lista_eliminati:List[bool]):
		#metodo per ripristinare lo schema
		self.schema_gioco = [riga[:] for riga in schema_bk]
		#ripristino i numeri delle pedine degli utenti e i flag delle eliminazioni
		for id_giocatore in range(len(self.lista_giocatori)):
			self.lista_giocatori[id_giocatore].pedine_aggiunte_dopo_ultima_aggiunta = numero_pedine[id_giocatore]
			self.lista_giocatori[id_giocatore].is_eliminato = lista_eliminati[id_giocatore]

	def calcola_differenza_valori_schemi_per_giocatore(self, schema_finale:List[List[int]], id_giocatore_analisi:int) -> float:
		#metodo per calcolare la differenza di punteggio per un giocatore tra lo schema attuale e quello successivo, da fare dopo aver ripristinato lo schema
		#calcola il valore per un singolo giocatore
		#mi creo 2 liste con la somma dei valori delle pedine per ciascun giocatore
		#sono rilevanti le pedine proprie, il fatto che si abbia vinto/perso e il fatto che qualche giocatore sia stato eliminato
		lista_punteggi_attuale = [0 for i in range(len(self.lista_giocatori))]
		lista_punteggi_futuri = [0 for i in range(len(self.lista_giocatori))]
		#adesso itero tra tutte le celle e le vado a considerare
		for r in range(self.dim_v):
			for c in range(self.dim_o):
				cella_attuale = self.schema_gioco[r][c]
				cella_futura = schema_finale[r][c]
				#valuto le varie celle
				if cella_attuale != Costanti.numero_casella_vuota:
					#allora apprtiene a un giocatore, ricavo il giocatore e aggiorno la rispettiva lista
					id_giocatore = cella_attuale%len(self.lista_giocatori)
					valore_pedina = cella_attuale // len(self.lista_giocatori)
					lista_punteggi_attuale[id_giocatore] += valore_pedina
				if cella_futura != Costanti.numero_casella_vuota:
					#allora apprtiene a un giocatore, ricavo il giocatore e aggiorno la rispettiva lista
					id_giocatore = cella_futura%len(self.lista_giocatori)
					valore_pedina = cella_futura // len(self.lista_giocatori)
					lista_punteggi_futuri[id_giocatore] += valore_pedina
		#adesso valuto i punteggi in base al giocatore in questione
		if lista_punteggi_futuri[id_giocatore_analisi] == 0:
			return -self.valore_eliminazione_giocatore - self.valore_vincita_partita
		#altrimenti valuto se ho eliminato qualche giocatore con la mia mossa
		punteggio_raggiunto = sum( [self.valore_eliminazione_giocatore for i in range(len(self.lista_giocatori)) if ( (lista_punteggi_attuale[i]!=0) and (lista_punteggi_futuri[i] == 0) ) ] )
		#a questo punteggio ci sommo il valore se ho vinto la partita
		#per aver vinto significa che i punteggi futuri diversi da 0 sono solo 1
		if lista_punteggi_futuri.count(0) == (len(self.lista_giocatori)-1 ):
			#significa che il giocatore ha vinto la partita punteggio positivo
			punteggio_raggiunto += self.valore_vincita_partita
		#adesso faccio la differenza tra il finale e l'iniziale
		punteggio_raggiunto += (lista_punteggi_futuri[id_giocatore_analisi] - lista_punteggi_attuale[id_giocatore_analisi])*self.valore_moltiplicatore_mangiata
		return punteggio_raggiunto
		
	def calcola_punteggio_generatrice_piu_vicina(self, id_giocatore:int) -> float:
		#metodo per calcolare il punteggio della generatrice piu vicina allo schema
		#vado a ricercare tutte le generatrici di quel giocatore
		#mi ricavo la casa base
		(r_base, c_base) = self.lista_giocatori[id_giocatore].casa_base
		n_giocatori = len(self.lista_giocatori)
		#se la sua casa base e occupata ritorno subito 0
		if (self.schema_gioco[r_base][c_base] != Costanti.numero_casella_vuota) and ( (self.schema_gioco[r_base][c_base]%n_giocatori) != id_giocatore ):
			return 0
		lista_distanze = [ (abs(r-r_base) + abs(c-c_base)) for r in range(self.dim_v) for c in range(self.dim_o) if ( (self.schema_gioco[r][c] != Costanti.numero_casella_vuota) and (self.schema_gioco[r][c]%n_giocatori == id_giocatore) and (self.schema_gioco[r][c]//n_giocatori == self.valore_pedina_generatrice) ) ]
		#vado a calcolarmi il secondo fattore
		if len(lista_distanze) == 0:
			#non ho generatrici, il secondo fattore è 0, potrei gia ritornare
			return 0
		secondo_fattore = self.dim_o + self.dim_v + 1 - min(lista_distanze)
		return self.valore_generatrice_piu_vicina*secondo_fattore

	
	
	'''
	def calcola_lista_mosse_sequenza_mangiare(self, id_giocatore:int) -> List[ List[ Tuple[int, int] ] ]:
		#metodo per calcolare la lisat di tutte le mosse in successioni per mangiare
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		bk_schema_gioco = [ riga[:] for riga in self.schema_gioco ]

	def calcola_lista_mosse_mangiare_da_casella(self, id_giocatore:int, r:int, c:int) -> List[List[Tuple[int, int]]]:
		#metodo per calcolare le mosse successive a per mangiare partendo da una casella prefissata
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		bk_schema_gioco = [ riga[:] for riga in self.schema_gioco ]
		#mi ricavo la lista delle mosse supponendo lo schema gia modificato
		lista_mosse_successive = []
	'''

	def esegui_successione_mosse(self, lista_mosse:List[Tuple[int, int]]) -> float:
		#metodo che esegue una serie di mosse in successione e ne ritorna il punteggio complessivo
		punteggio_attuale = 0
		for i in range(len(lista_mosse) -1):
			#eseguo la mossa ed incremento il punteggio attuale
			punteggio_attuale += self.esegui_mossa(origine=lista_mosse[i], destinazione=lista_mosse[i+1])
		return punteggio_attuale

	def calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(self, lista_mosse:List[Tuple[int, int]], id_giocatore:int) -> float:
		#metodo che esegue le mosse e calcola tutti i punteggi stabiliti
		#per prima cosa eseguo la sequenza di mosse
		punteggio = 0
		punteggio += self.esegui_successione_mosse(lista_mosse=lista_mosse)
		#adesso calcolo il punteggio per la generatrice piu vicina alla casa base
		punteggio += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore)
		#adesso vado a verificare se il giocatore ha la casa base occupata oppure ha occupato la casa base di qualcun altro
		n_giocatori = len(self.lista_giocatori)
		for giocatore in self.lista_giocatori:
			if not(giocatore.is_eliminato):
				(r_base, c_base) = giocatore.casa_base
				valore_pedina = self.schema_gioco[r_base][c_base]
				if valore_pedina != Costanti.numero_casella_vuota:
					giocatore_pedina = valore_pedina%n_giocatori
					if (giocatore_pedina == id_giocatore) and (giocatore.id_giocatore != id_giocatore):
						#allora il giocatore ha posizionato una sua pedina in una casa base di qualcun altro
						punteggio += self.valore_occupazione_casa_base
					elif (giocatore_pedina != id_giocatore) and (giocatore.id_giocatore == id_giocatore):
						#qualcun altro ha occupato la casa base del giocatore in analisi, il punteggio si sottrarrà
						punteggio -= self.valore_occupazione_casa_base
		#al termine posso ritornare il punteggio
		return punteggio





	##############################################################################################################################################################################################
	#algoritmi vari
	
	'''
	def algoritmo_min_max(self, profondita:int, id_giocatore:int) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare il miglior punteggio di quel giocatore
		#mi creo l'oggetto da ritornare in seguito
		oggetto_esito = EsitoAlgoritmoRicerca()
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			#valorizzo il punteggio
			oggetto_esito.punteggio = 0
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#allora vado a ricosrsione e ritorno lo stesso risultato aumentato della cifra della vincita della partita
			punteggio_raggiunto = -self.valore_vincita_partita*profondita 
			esito_ricorsione = self.algoritmo_min_max(profondita=profondita -1, id_giocatore=giocatore_successivo)
			punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			oggetto_esito.punteggio = punteggio_raggiunto
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore)*profondita
			esito_ricorsione = self.algoritmo_min_max(profondita=profondita-1, id_giocatore=giocatore_successivo)
			punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.punteggio = punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta():
			punteggio_raggiunto = self.valore_vincita_partita*profondita 
			esito_ricorsione = self.algoritmo_min_max(profondita=profondita-1, id_giocatore=giocatore_successivo) #il meno ci va pèerche e un punteggio riferito all'altro giocatore
			punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.punteggio = punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#mi converto la lista di mosse in lista di liste
		lista_liste_mosse = [ list(i) for i in lista_mosse_disponibili ]
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		lista_esiti:List[EsitoAlgoritmoRicerca] = []
		while len(lista_liste_mosse) >0:
			#mi prendo il primo
			lista_mosse_in_analisi = lista_liste_mosse.pop(0)
			#intanto eseguo la lista di mosse
			punteggio_attuale = self.esegui_successione_mosse(lista_mosse=lista_mosse_in_analisi)*profondita
			#adesso mi ricavo l'ultima mossa per vedere se ha mangiato e valutare se analizzarla o aspettare
			((r_pen, c_pen), (r_ult, c_ult)) = lista_mosse_in_analisi[-2:]
			flag_analizza_mossa = True
			#io non devo analizzarla solo se ho l'obbligo di mangiare, e una mossa di mangio e ho altre celle successive
			if ( abs(r_ult - r_pen) + abs(c_ult - c_pen) )>1:
				#ho mangiato, calcolo celle successive
				celle_successive_mangio = giocatore_in_turno.calcola_lista_successive_celle_per_mangiare_singola_casella(tabella_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, numero_giocatori=len(self.lista_giocatori), r=r_ult, c=c_ult)
				if len(celle_successive_mangio) >0:
					if self.flag_obbligatorieta_mangiare:
						#allora non devo analizzare la mossa perche fa parte di un percorso per mangiare obbligatorio
						flag_analizza_mossa = False
					#vado ad aggiugnere alla lista di delle mosse tutte le successive mosse
					lista_liste_mosse.extend( [ lista_mosse_in_analisi + [i] for i in celle_successive_mangio ] )
			#adesso verifico se devo analizzare la mossa
			if flag_analizza_mossa:
				#al punteggio attuale ci sommo anche il punteggio della generatrice piu vicina
				punteggio_attuale += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore)*profondita
				#mi calcolo il punteggio a ricorsione
				esito_ricorsione = self.algoritmo_min_max(profondita=profondita-1, id_giocatore=giocatore_successivo)
				punteggio_attuale -= esito_ricorsione.punteggio
				#mi creo un nuovo esito
				nuovo_esito = EsitoAlgoritmoRicerca()
				nuovo_esito.punteggio = punteggio_attuale
				nuovo_esito.lista_mosse= lista_mosse_in_analisi
				lista_esiti.append(nuovo_esito)
			#al termine del while ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		massimo_punteggio = max( [ i.punteggio for i in lista_esiti ] )
		#mi calcolo gli esiti con punteggio massimo
		esiti_punteggio_massimo = [ i for i in lista_esiti if i.punteggio==massimo_punteggio ]
		#ne ritorno uno a caso di questi
		return random.choice(esiti_punteggio_massimo)
	'''



	def algoritmo_valuta_schema_profondita(self, profondita:int, id_giocatore:int) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare il miglior punteggio di quel giocatore
		#mi creo l'oggetto da ritornare in seguito
		oggetto_esito = EsitoAlgoritmoRicerca()
		#caso base sono arrivato al limite della profondita
		if profondita <= 1:
			#valorizzo lo schema
			oggetto_esito.matrice_finale = [riga[:] for riga in self.schema_gioco]
			oggetto_esito.profondita_matrice_finale = 1
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#vado a ricorsione e ritorno lo schema che viene ritornato dalla ricorsione
			#se il giocatore e gia stato eliminato la profondita della ricorsione puo essere la stessa dell'attuale
			esito_ricorsione = self.algoritmo_valuta_schema_profondita(profondita=profondita, id_giocatore=giocatore_successivo)
			oggetto_esito.matrice_finale = esito_ricorsione.matrice_finale
			oggetto_esito.profondita_matrice_finale = esito_ricorsione.profondita_matrice_finale
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			esito_ricorsione = self.algoritmo_valuta_schema_profondita(profondita=profondita-1, id_giocatore=giocatore_successivo)
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.matrice_finale = esito_ricorsione.matrice_finale
			oggetto_esito.profondita_matrice_finale = esito_ricorsione.profondita_matrice_finale
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo o altri giocatori, comunque situazione di stallo giocatore ritorno la matrice attuale
		if self.verifica_partita_vinta(): 
			#non vado a ricorsione perche situazione di stallo e ritorno la matrice attuale
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.matrice_finale = [riga[:] for riga in self.schema_gioco]
			oggetto_esito.profondita_matrice_finale = profondita
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#mi converto la lista di mosse in lista di liste
		lista_liste_mosse = [ list(i) for i in lista_mosse_disponibili ]
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		lista_esiti:List[EsitoAlgoritmoRicerca] = []
		while len(lista_liste_mosse) >0:
			#mi prendo il primo
			lista_mosse_in_analisi = lista_liste_mosse.pop(0)
			#intanto eseguo la lista di mosse
			punteggio_attuale = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=lista_mosse_in_analisi, id_giocatore=id_giocatore)*profondita
			#adesso mi ricavo l'ultima mossa per vedere se ha mangiato e valutare se analizzarla o aspettare
			((r_pen, c_pen), (r_ult, c_ult)) = lista_mosse_in_analisi[-2:]
			flag_analizza_mossa = True
			#io non devo analizzarla solo se ho l'obbligo di mangiare, e una mossa di mangio e ho altre celle successive
			if ( abs(r_ult - r_pen) + abs(c_ult - c_pen) )>1:
				#ho mangiato, calcolo celle successive
				celle_successive_mangio = giocatore_in_turno.calcola_lista_successive_celle_per_mangiare_singola_casella(tabella_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, numero_giocatori=len(self.lista_giocatori), r=r_ult, c=c_ult)
				if len(celle_successive_mangio) >0:
					if self.flag_obbligatorieta_mangiare:
						#allora non devo analizzare la mossa perche fa parte di un percorso per mangiare obbligatorio
						flag_analizza_mossa = False
					#vado ad aggiugnere alla lista di delle mosse tutte le successive mosse
					lista_liste_mosse.extend( [ lista_mosse_in_analisi + [i] for i in celle_successive_mangio ] )
			#adesso verifico se devo analizzare la mossa
			if flag_analizza_mossa:
				#al punteggio attuale ci sommo anche il punteggio della generatrice piu vicina
				#punteggio_attuale += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore)*profondita
				#mi calcolo il punteggio a ricorsione
				esito_ricorsione = self.algoritmo_valuta_schema_profondita(profondita=profondita-1, id_giocatore=giocatore_successivo)
				#adesso devo calcolare il punteggio relativo alla ricorsione, ovvero partendo dallo schema di partenza e da quello di arrivo per il numero di giocatore calcolo la differenza delle pedine
				punteggio_ricorsione = self.calcola_differenza_valori_schemi_per_giocatore(schema_finale=esito_ricorsione.matrice_finale, id_giocatore_analisi=id_giocatore)
				punteggio_attuale += sum( [punteggio_ricorsione*i for i in range(1, esito_ricorsione.profondita_matrice_finale+1)] )
				#mi creo un nuovo esito
				nuovo_esito = EsitoAlgoritmoRicerca()
				nuovo_esito.punteggio = punteggio_attuale
				nuovo_esito.lista_mosse= lista_mosse_in_analisi
				nuovo_esito.matrice_finale = esito_ricorsione.matrice_finale
				nuovo_esito.profondita_matrice_finale = esito_ricorsione.profondita_matrice_finale
				lista_esiti.append(nuovo_esito)
			#al termine del while ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		massimo_punteggio = max( [ i.punteggio for i in lista_esiti ] )
		#mi calcolo gli esiti con punteggio massimo
		esiti_punteggio_massimo = [ i for i in lista_esiti if i.punteggio==massimo_punteggio ]
		#ne ritorno uno a caso di questi
		return random.choice(esiti_punteggio_massimo)

	'''
	INGLOBATO NELL'ALGORITMO MIN/MAX CON POSSILITÀ DI MOLTIPLICARE PER LA PROFONDITA
	#algoritmo min max per piu giocatori con le liste
	def algoritmo_min_max_multigiocatori(self, profondita:int, id_giocatore:int) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare il miglior punteggio di quel giocatore
		#mi creo l'oggetto da ritornare in seguito
		numero_giocatori = len(self.lista_giocatori)
		oggetto_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			#valorizzo il punteggio
			#oggetto_esito.punteggio = 0
			oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#allora vado a ricosrsione e ritorno lo stesso risultato aumentato della cifra della vincita della partita
			punteggio_raggiunto = -self.valore_vincita_partita*profondita 
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_successivo)
			#punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			#oggetto_esito.punteggio = punteggio_raggiunto
			#oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore)*profondita
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo)
			#punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			#oggetto_esito.punteggio = punteggio_raggiunto
			#oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta():
			punteggio_raggiunto = self.valore_vincita_partita*profondita 
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo) #il meno ci va pèerche e un punteggio riferito all'altro giocatore
			#punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			#oggetto_esito.punteggio = punteggio_raggiunto
			#oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#mi converto la lista di mosse in lista di liste
		lista_liste_mosse = [ list(i) for i in lista_mosse_disponibili ]
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		lista_esiti:List[EsitoAlgoritmoRicerca] = []
		while len(lista_liste_mosse) >0:
			#mi prendo il primo
			lista_mosse_in_analisi = lista_liste_mosse.pop(0)
			#intanto eseguo la lista di mosse
			punteggio_attuale = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=lista_mosse_in_analisi, id_giocatore=id_giocatore)*profondita
			#adesso mi ricavo l'ultima mossa per vedere se ha mangiato e valutare se analizzarla o aspettare
			((r_pen, c_pen), (r_ult, c_ult)) = lista_mosse_in_analisi[-2:]
			flag_analizza_mossa = True
			#io non devo analizzarla solo se ho l'obbligo di mangiare, e una mossa di mangio e ho altre celle successive
			if ( abs(r_ult - r_pen) + abs(c_ult - c_pen) )>1:
				#ho mangiato, calcolo celle successive
				celle_successive_mangio = giocatore_in_turno.calcola_lista_successive_celle_per_mangiare_singola_casella(tabella_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, numero_giocatori=len(self.lista_giocatori), r=r_ult, c=c_ult)
				if len(celle_successive_mangio) >0:
					if self.flag_obbligatorieta_mangiare:
						#allora non devo analizzare la mossa perche fa parte di un percorso per mangiare obbligatorio
						flag_analizza_mossa = False
					#vado ad aggiugnere alla lista di delle mosse tutte le successive mosse
					lista_liste_mosse.extend( [ lista_mosse_in_analisi + [i] for i in celle_successive_mangio ] )
			#adesso verifico se devo analizzare la mossa
			if flag_analizza_mossa:
				#al punteggio attuale ci sommo anche il punteggio della generatrice piu vicina
				#punteggio_attuale += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore)*profondita
				#mi calcolo il punteggio a ricorsione
				esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo)
				#punteggio_attuale -= esito_ricorsione.punteggio
				#mi creo un nuovo esito
				nuovo_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
				#nuovo_esito.punteggio = punteggio_attuale
				nuovo_esito.lista_mosse= lista_mosse_in_analisi
				nuovo_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
				nuovo_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_attuale
				nuovo_esito.calcola_punteggio(id_giocatore=id_giocatore)
				lista_esiti.append(nuovo_esito)
			#al termine del while ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		massimo_punteggio = max( [ i.punteggio for i in lista_esiti ] )
		#per test per vedere se lacolano le stesse mosse provo a calcolarmi la lista delle mosse con gli esitiRicercaListaMosse
		#sembra che il numero sia lo stesso
		#lista_mosse_calcolata_esiti = EsitoRicercaListaMosse.EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco, giocatore_in_questione=giocatore_in_turno, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		#print(f'\nMosse attuali: {len(lista_esiti)}')
		#print(f'Mosse nuove  : {len(lista_mosse_calcolata_esiti)}')
		#############################################################
		#mi calcolo gli esiti con punteggio massimo
		esiti_punteggio_massimo = [ i for i in lista_esiti if i.punteggio==massimo_punteggio ]
		#ne ritorno uno a caso di questi
		return random.choice(esiti_punteggio_massimo)
	'''

	def calcola_numero_mosse_per_vincere(self, giocatore_in_analisi:Giocatore.Giocatore, massima_profondita:int) -> float:
		#metodo per calcolare con esplorazione in ampiezza il numero di mosse che permetterebbe di vimcere, se non risulta vincente nella profondita stabilita ritorna la profondita, se vede che entro n mosse rischia di perdere ritorna + infinito
		n_giocatori = len(self.lista_giocatori)
		#per prima cosa verifico se ha gia vinto o perso (in teroia basta solo che ha vinto perche e lui che ha eseguito l'ultima mossa)
		if giocatore_in_analisi.verifica_giocatore_vincente(tabella_gioco=self.schema_gioco, numero_giocatori=n_giocatori):
			return 0
		elif giocatore_in_analisi.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=n_giocatori):
			return math.inf
		#altrimenti posso provare ad analizzare il giocatore successivo
		#mi calcolo il giocatore successivo
		giocatore_successivo = self.lista_giocatori[ (giocatore_in_analisi.id_giocatore+1)%n_giocatori ]
		#mi creo le liste di analisi
		lista_analisi_attuale : List[EsitoRicercaListaMosse.EsitoRicercaListaMosse] = []
		lista_analisi_successiva : List[ EsitoRicercaListaMosse.EsitoRicercaListaMosse ] = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(
												matrice_inziale=self.schema_gioco,
												flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
												flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
												giocatore_in_questione=giocatore_successivo,
												numero_giocatori=n_giocatori,
												flag_salva_lista_mosse=False) #non mi serve la lista dello storico delle mosse per risparmiare memoria perche gia di suo sara pesante come memoria
		for profondita_attuale in range(1, massima_profondita+1):
			#cambio le liste
			lista_analisi_attuale = lista_analisi_successiva[:]
			lista_analisi_successiva.clear()
			#mi calcolo il giocatore successivo
			giocatore_successivo = self.lista_giocatori[ (giocatore_successivo.id_giocatore+1)%n_giocatori ]
			#while len(lista_analisi_attuale) >0:
			for esito_lista_mosse in lista_analisi_attuale:
				#forse posso provare con un for per migliorare performance
				#esito_lista_mosse = lista_analisi_attuale.pop()
				#vado a verificare se il giocatore in analisi ha vinto o no
				if giocatore_in_analisi.verifica_giocatore_vincente(tabella_gioco=esito_lista_mosse.matrice_finale, numero_giocatori=n_giocatori):
					return profondita_attuale
				elif giocatore_in_analisi.verifica_giocatore_eliminato(tabella_gioco=esito_lista_mosse.matrice_finale, numero_giocatori=n_giocatori):
					return math.inf
				#se non ha vinto provo ad eseguire le mosse del giocatore successivo ed ad aggiungerle 
				lista_configurazioni_da_aggiungere = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=esito_lista_mosse.matrice_finale,
						giocatore_in_questione=giocatore_successivo,
						flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
						flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
						flag_salva_lista_mosse=False,
						numero_giocatori=n_giocatori)
				#se la lista delle possibili configurazioni e vuota significa che devo aggiungere la configurazione stessa con togliendo tutti gli elementi del giocatore in questione
				if len(lista_configurazioni_da_aggiungere) == 0:
					nuovo_esito = EsitoRicercaListaMosse.EsitoRicercaListaMosse(matrice_finale=esito_lista_mosse.elimina_giocatore_schema(
										id_giocatore=giocatore_successivo.id_giocatore,
										numero_giocatori=n_giocatori
									), 
									flag_salva_mosse=False,
									lista_mosse=[]
					)
					lista_analisi_successiva.append(nuovo_esito)
				else:
					#ho trovato delle mosse per il giocatore
					lista_analisi_successiva.extend(lista_configurazioni_da_aggiungere)
		#al termine del for se non ha trovato niente significa che devo ritornare la massima profondita raggiunta aumentata di 1
		return massima_profondita +1

	def algoritmo_mosse_per_vincere(self, profondita:int, id_giocatore:int) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare la migliore mossa utilizzando l'algoritmo che va a ricercare entro quante mosse si può vincere o perdere
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		#suppongo che il giocatore non sia gia stato eliminato
		#mi calcolo il giocatore in turno
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		n_giocatori = len(self.lista_giocatori)
		#mi calcolo tutti le mosse che potrebbe fare il giocatore
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco,
																							giocatore_in_questione=giocatore_in_turno,
																							flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																							flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																							flag_salva_lista_mosse=True,
																							numero_giocatori=n_giocatori)
		lista_esiti : List[EsitoAlgoritmoRicerca] = []
		#adesso provo ad eseguire tutte le mosse e calcolarmi i nuovi esiti
		for esito_lista_mosse in lista_mosse_disponibili:
			nuovo_esito = EsitoAlgoritmoRicerca(numero_giocatori=n_giocatori)
			#eseguo la lista delle mosse calcolate
			punteggio_lista_mnosse = self.esegui_successione_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate)
			nuovo_esito.punteggio = punteggio_lista_mnosse
			nuovo_esito.lista_mosse = esito_lista_mosse.lista_mosse_calcolate[:]
			#adesso vado a calcolare quante mosse sarebbero necessarie per vincere
			nuovo_esito.numero_mosse_per_vincere = self.calcola_numero_mosse_per_vincere(giocatore_in_analisi=giocatore_in_turno, massima_profondita=profondita)
			#mi vado ad aggiugnere l'esito alla lista
			lista_esiti.append(nuovo_esito)
			#mi ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		#mi calcolo il minimo tra numero mosse e punteggio
		min_mosse = min( [ i.numero_mosse_per_vincere for i in lista_esiti ] )
		#mi calcolo la lisat degli esiti minimi
		lista_esiti_minimo_mosse = [ i for i in lista_esiti if (i.numero_mosse_per_vincere==min_mosse) ]
		#se ha lunghezza >1 ritorno quella con il massimo punteggio
		if len(lista_esiti_minimo_mosse) == 0:
			return lista_esiti_minimo_mosse[0]
		#altrimenti ha piu di un elemento
		massimo_punteggio = max( [i.punteggio for i in lista_esiti_minimo_mosse] )
		lista_esiti_massimo_punteggio = [ i for i in lista_esiti_minimo_mosse if (i.punteggio==massimo_punteggio) ]
		esito_scelto = random.choice(lista_esiti_massimo_punteggio)
		return esito_scelto

	def algoritmo_media_punteggio(self, profondita:int, id_giocatore:int, flag_moltiplica_profondita:bool) -> EsitoAlgoritmoRicerca:
		#algoritmo che ritorna la mossa che ha un punteggio medio maggiore
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		#suppongo che il giocatore non sia gia stato eliminato
		#mi calcolo il giocatore in turno
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		n_giocatori = len(self.lista_giocatori)
		#mi calcolo tutti le mosse che potrebbe fare il giocatore
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco,
																							giocatore_in_questione=giocatore_in_turno,
																							flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																							flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																							flag_salva_lista_mosse=True,
																							numero_giocatori=n_giocatori)
		if len(lista_mosse_disponibili) == 1:
			#mi faccio un nuovo esito e ritorno quello
			esito_ritorno = EsitoAlgoritmoRicerca(numero_giocatori=0)
			esito_ritorno.lista_mosse = lista_mosse_disponibili[0].lista_mosse_calcolate
			return esito_ritorno
		lista_esiti : List[EsitoAlgoritmoRicerca] = []
		#adesso provo ad eseguire tutte le mosse e calcolarmi i nuovi esiti
		for esito_lista_mosse in lista_mosse_disponibili:
			#eseguo la lista delle mosse calcolate
			punteggio_lista_mosse = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate, id_giocatore=id_giocatore)
			#mi devo calcolare anche il ppunteggio di distanza dalla casa base
			#punteggio_lista_mosse += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore)
			#vado a calcolarmi l'esito della somma dei punteggi dei percorsi
			esito_somma_punteggi_percorsi = self.calcola_punteggi_percorsi(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ci vado a sommare il punteggio di questa mossa
			esito_somma_punteggi_percorsi.lista_punteggi_giocatori[id_giocatore] += punteggio_lista_mosse*profondita*esito_somma_punteggi_percorsi.numero_percorsi
			#vado a popolarmi la lista delle mosse di questo esito
			esito_somma_punteggi_percorsi.lista_mosse = esito_lista_mosse.lista_mosse_calcolate[:]
			#vado a calcolare il punteggio di questo esito che poi ancdor a dividere per il numero di percorsi trovati
			esito_somma_punteggi_percorsi.calcola_punteggio(id_giocatore=id_giocatore)
			esito_somma_punteggi_percorsi.punteggio = esito_somma_punteggi_percorsi.punteggio/esito_somma_punteggi_percorsi.numero_percorsi
			#mi vado ad aggiugnere l'esito alla lista
			lista_esiti.append(esito_somma_punteggi_percorsi)
			#mi ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		#mi calcolo il massimo punteggio
		massimo_punteggio = max( [i.punteggio for i in lista_esiti] )
		lista_esiti_massimo_punteggio = [ i for i in lista_esiti if (i.punteggio==massimo_punteggio) ]
		esito_scelto = random.choice(lista_esiti_massimo_punteggio)
		#prima di ritornarlo voglio stamparmeli tutti
		print('esiti con somma punteggio')
		for i in sorted(lista_esiti, key= lambda x: x.punteggio):
			print(f'mosse: {i.lista_mosse}, punteggio: {i.punteggio}, numero_percorsi: {i.numero_percorsi}, lista_punteggi: {i.lista_punteggi_giocatori}')
		return esito_scelto

	def calcola_punteggi_percorsi(self, profondita:int, id_giocatore:int, flag_moltiplica_profondita:bool) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare la somma dei punteggi dei percorsi generati 
		#mi creo l'oggetto da ritornare in seguito
		numero_giocatori = len(self.lista_giocatori)
		oggetto_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			oggetto_esito.numero_percorsi = 1
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#vado a ricorsione e mi calcolo i percorsi a ricorsione
			punteggio_raggiunto = -self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.calcola_punteggi_percorsi(profondita=profondita, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += (punteggio_raggiunto*esito_ricorsione.numero_percorsi) #metto la mostriplicazione perche se l'esito successivo si bifornca in n percorsi, il punteggio raggiunto da questo livello va moltiplicato per il numero di percorsi perche potrei avere n percorsi differenti
			oggetto_esito.numero_percorsi = esito_ricorsione.numero_percorsi #il numero di percorsi e lo stesso calcolato al livello successivo
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		#lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco,
																							giocatore_in_questione=giocatore_in_turno,
																							flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																							flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																							flag_salva_lista_mosse=True,
																							numero_giocatori=numero_giocatori)
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.calcola_punteggi_percorsi(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += (punteggio_raggiunto*esito_ricorsione.numero_percorsi) #metto la mostriplicazione perche se l'esito successivo si bifornca in n percorsi, il punteggio raggiunto da questo livello va moltiplicato per il numero di percorsi perche potrei avere n percorsi differenti
			oggetto_esito.numero_percorsi = esito_ricorsione.numero_percorsi #il numero di percorsi e lo stesso calcolato al livello successivo
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta():
			punteggio_raggiunto = self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.calcola_punteggi_percorsi(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita) #il meno ci va pèerche e un punteggio riferito all'altro giocatore
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += (punteggio_raggiunto*esito_ricorsione.numero_percorsi) #metto la mostriplicazione perche se l'esito successivo si bifornca in n percorsi, il punteggio raggiunto da questo livello va moltiplicato per il numero di percorsi perche potrei avere n percorsi differenti
			oggetto_esito.numero_percorsi = esito_ricorsione.numero_percorsi #il numero di percorsi e lo stesso calcolato al livello successivo
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#mi converto la lista di mosse in lista di liste
		#lista_liste_mosse = [ list(i) for i in lista_mosse_disponibili ]
		esito_da_ritornare = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		esito_da_ritornare.numero_percorsi = 0
		'''
		while len(lista_liste_mosse) >0:
			#mi prendo il primo
			lista_mosse_in_analisi = lista_liste_mosse.pop(0)
			#intanto eseguo la lista di mosse
			punteggio_attuale = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=lista_mosse_in_analisi, id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			#adesso mi ricavo l'ultima mossa per vedere se ha mangiato e valutare se analizzarla o aspettare
			((r_pen, c_pen), (r_ult, c_ult)) = lista_mosse_in_analisi[-2:]
			flag_analizza_mossa = True
			#io non devo analizzarla solo se ho l'obbligo di mangiare, e una mossa di mangio e ho altre celle successive
			if ( abs(r_ult - r_pen) + abs(c_ult - c_pen) )>1:
				#ho mangiato, calcolo celle successive
				celle_successive_mangio = giocatore_in_turno.calcola_lista_successive_celle_per_mangiare_singola_casella(tabella_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, numero_giocatori=len(self.lista_giocatori), r=r_ult, c=c_ult)
				if len(celle_successive_mangio) >0:
					if self.flag_obbligatorieta_mangiare:
						#allora non devo analizzare la mossa perche fa parte di un percorso per mangiare obbligatorio
						flag_analizza_mossa = False
					#vado ad aggiugnere alla lista di delle mosse tutte le successive mosse
					lista_liste_mosse.extend( [ lista_mosse_in_analisi + [i] for i in celle_successive_mangio ] )
			#adesso verifico se devo analizzare la mossa
			if flag_analizza_mossa:
				#al punteggio attuale ci sommo anche il punteggio della generatrice piu vicina
				#punteggio_attuale += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
				#mi calcolo il punteggio a ricorsione
				esito_ricorsione = self.calcola_punteggi_percorsi(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
				#vado a sommare i valori trovati nell'esito da ritornare
				for id_gioc in range(numero_giocatori):
					esito_da_ritornare.lista_punteggi_giocatori[id_gioc] += esito_ricorsione.lista_punteggi_giocatori[id_gioc]
				#ci vado a sommare anche il numero di percorsi
				esito_da_ritornare.numero_percorsi += esito_ricorsione.numero_percorsi
				#ci vado a sommare il punteggio per il giocatore attuale
				esito_da_ritornare.lista_punteggi_giocatori[id_giocatore] += (punteggio_attuale*esito_ricorsione.numero_percorsi)
			#al termine del while ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		'''
		for esito_lista_mosse in lista_mosse_disponibili:
			#mi calcolo il punteggio
			punteggio_attuale = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate, id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			#mi calcolo il punteggio a ricorsione
			esito_ricorsione = self.calcola_punteggi_percorsi(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#vado a sommare i valori trovati nell'esito da ritornare
			for id_gioc in range(numero_giocatori):
				esito_da_ritornare.lista_punteggi_giocatori[id_gioc] += esito_ricorsione.lista_punteggi_giocatori[id_gioc]
			#ci vado a sommare anche il numero di percorsi
			esito_da_ritornare.numero_percorsi += esito_ricorsione.numero_percorsi
			#ci vado a sommare il punteggio per il giocatore attuale
			esito_da_ritornare.lista_punteggi_giocatori[id_giocatore] += (punteggio_attuale*esito_ricorsione.numero_percorsi)
			#al termine del for ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		#se arrivo qui significa che ho popolato l'esito da ritornare con almeno una mossa
		return esito_da_ritornare

	#algoritmo min max per piu giocatori con le liste disinteressandosi della profodnita
	def algoritmo_min_max_multigiocatori(self, profondita:int, id_giocatore:int, flag_moltiplica_profondita:bool) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare il miglior punteggio di quel giocatore
		#mi creo l'oggetto da ritornare in seguito
		numero_giocatori = len(self.lista_giocatori)
		oggetto_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			#valorizzo il punteggio
			#oggetto_esito.punteggio = 0
			#oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#allora vado a ricosrsione e ritorno lo stesso risultato aumentato della cifra della vincita della partita
			punteggio_raggiunto = -self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		#lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco,
																							giocatore_in_questione=giocatore_in_turno,
																							flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																							flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																							flag_salva_lista_mosse=True,
																							numero_giocatori=numero_giocatori)
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta():
			punteggio_raggiunto = self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita) #il meno ci va pèerche e un punteggio riferito all'altro giocatore
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		lista_esiti:List[EsitoAlgoritmoRicerca] = []
		for esito_lista_mosse in lista_mosse_disponibili:
			#eseguo la lista delle mosse calcolate
			punteggio_lista_mosse = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate, id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			#mi calcolo il punteggio a ricorsione
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#punteggio_attuale -= esito_ricorsione.punteggio
			#mi creo un nuovo esito
			nuovo_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
			#nuovo_esito.punteggio = punteggio_attuale
			nuovo_esito.lista_mosse= esito_lista_mosse.lista_mosse_calcolate
			nuovo_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			nuovo_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_lista_mosse
			nuovo_esito.calcola_punteggio(id_giocatore=id_giocatore)
			lista_esiti.append(nuovo_esito)
			#al termine del for ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		if len(lista_esiti) == 1:
			#se ha solo 1 elemento lo ritorno
			return lista_esiti[0]
		massimo_punteggio = max( [ i.punteggio for i in lista_esiti ] )
		#mi calcolo gli esiti con punteggio massimo
		esiti_punteggio_massimo = [ i for i in lista_esiti if i.punteggio==massimo_punteggio ]
		#ne ritorno uno a caso di questi
		return random.choice(esiti_punteggio_massimo)




		


		
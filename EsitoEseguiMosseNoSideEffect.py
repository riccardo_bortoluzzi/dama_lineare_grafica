#script che contiene l'esito per eseguire le mosse ma non modificare lo schema di partenza

from __future__ import annotations
from typing import List, Tuple

class EsitoEseguiMosseNoSideEffect:
	matrice: List[List[int]]
	numero_pedine_aggiunte_giocatori : List[int]
	lista_case_basi : List[Tuple[int, int]]
	punteggio:float

	def __init__(self, schema:List[List[int]], numero_pedine_giocatori: List[int], case_basi:List[Tuple[int, int]], punteggio:float) -> None:
		self.matrice = schema#[riga[:] for riga in schema]
		self.numero_pedine_aggiunte_giocatori = numero_pedine_giocatori#numero_pedine_giocatori[:]
		self.lista_case_basi = case_basi#[:]
		self.punteggio = punteggio
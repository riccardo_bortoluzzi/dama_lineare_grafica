#script che gestisce il gioco della dama lineare

from __future__ import annotations
from typing import List, Tuple, Union
import random, threading

import math
from EsitoEseguiMosseNoSideEffect import EsitoEseguiMosseNoSideEffect

import Giocatore, Costanti, EsitoRicercaListaMosse
from EsitoAlgoritmoRicerca import EsitoAlgoritmoRicerca
from NumeroPedineGiocatori import NumeroPedineGiocatori

class GiocoBase:
	dim_v: int
	dim_o : int
	lista_giocatori : List[Giocatore.Giocatore] #ogni giocatore e identificato da un numero e le pedine sono nel modo: len(giocatori)*valore + id_giocatore
	valore_pedina_generatrice : int
	valore_pedina_semplice : int
	schema_gioco : List[List[int]]
	algoritmo_ricerca : str
	valore_eliminazione_giocatore : int
	valore_vincita_partita : int
	numero_pedine_prima_generatrice : int
	flag_mangiare_proprie_pedine : bool
	flag_obbligatorieta_mangiare : bool
	id_partita : int #id che indica quale partita si sta giocando
	valore_moltiplicatore_mangiata : float #valore per cui moltiplicare il valore della pedian quando si mangia
	valore_generatrice_piu_vicina : float
	valore_occupazione_casa_base : int #valore che indica il punteggio da assegnare se si occupa la casa base altrui o la proproa
	valore_senza_generatrici : int #valore che indica se un giocatore rimane senza generatrici
	flag_pesa_punteggi_numero_pedine : bool #flag che indica se pesare il valore delle pedine con quelle che complessivamente si ha nel campo di gioco, dovrebbe permettere di avere una strategia più aggressiva se si ha tante pedine, 
	flag_sposta_casa_base : bool #flag che indica se durante il gioco la casa base si sposta nella casella della pedina generata
	flag_crea_damoni : bool #flag che indica se si è scelto di giocare con i damoni o no
	valore_pedina_damone : int #valore associato al damone 
	flag_damone_solo_angolo : bool #flag che indica se fare il damnoe solo nell'angolo opposto alla casa base di partenza
	flag_damone_mossa_diagonale : bool #flag che indioca se i damoni possono muovere in diagonale per una mossa semplice, senza mangiare
	flag_damone_funzione_generatrice:bool #indica se i damoni hanno la funzione di generatrici e quindi se vanno nella casa base generano una nuova pedina
	flag_damone_mangia_diagonale : bool #indica se i damoni possono mangiare in diagonale indipendentemente se si possano o meno muovere in diagonale
	flag_damone_mangiato_solo_da_damoni : bool #flag che indica se un damone puo essere mangiato solo da altri damoni
	lista_thread_totali : List[threading.Thread] #lista che contiene i thread per contarli


	def __init__(self, 
					dim_v:int, 
					dim_o:int, 
					numero_giocatori_umani:int, 
					numero_giocatori_pc:int, 
					valore_semplice:int, 
					valore_generatrice:int, 
					algoritmo:str, 
					valore_eliminazione_giocatore:int, 
					valore_vincita_partita:int, 
					pedine_prima_generatrice:int, 
					mangiare_proprie_pedine: bool, 
					obbligatorio_mangiare : bool, 
					id_partita:int, 
					valore_moltiplicatore:float, 
					generatrice_piu_vicina:float, 
					valore_occupazione_casa_base:int,
					valore_senza_generatrici : int,
					flag_pesa_punteggi:bool,
					flag_sposta_casa_base : bool,
					flag_crea_damoni : bool,
					valore_damoni:int,
					flag_damone_solo_angolo:bool,
					flag_damoni_mossa_diagonale : bool,
					flag_damoni_generatrice:bool,
					flag_damone_mangia_diagonale : bool,
					flag_damone_mangiato_solo_damoni:bool
				) -> None:
		self.dim_o = dim_o
		self.dim_v= dim_v
		self.valore_pedina_generatrice = valore_generatrice
		self.valore_pedina_semplice = valore_semplice
		self.algoritmo_ricerca = algoritmo
		self.valore_eliminazione_giocatore = valore_eliminazione_giocatore
		self.valore_vincita_partita = valore_vincita_partita
		self.numero_pedine_prima_generatrice = pedine_prima_generatrice
		self.flag_mangiare_proprie_pedine = mangiare_proprie_pedine
		self.flag_obbligatorieta_mangiare = obbligatorio_mangiare
		self.id_partita = id_partita
		self.valore_moltiplicatore_mangiata = valore_moltiplicatore
		self.valore_generatrice_piu_vicina = generatrice_piu_vicina
		self.valore_occupazione_casa_base = valore_occupazione_casa_base
		self.valore_senza_generatrici = valore_senza_generatrici
		self.flag_pesa_punteggi_numero_pedine = flag_pesa_punteggi
		self.flag_sposta_casa_base = flag_sposta_casa_base
		self.flag_crea_damoni = flag_crea_damoni
		self.valore_pedina_damone = valore_damoni
		self.flag_damone_solo_angolo = flag_damone_solo_angolo
		self.flag_damone_mossa_diagonale = flag_damoni_mossa_diagonale
		self.flag_damone_funzione_generatrice = flag_damoni_generatrice
		self.flag_damone_mangia_diagonale = flag_damone_mangia_diagonale
		self.flag_damone_mangiato_solo_da_damoni = flag_damone_mangiato_solo_damoni
		self.lista_thread_totali = []
		#mi creo lo schema di gioco
		self.schema_gioco = [ [ Costanti.numero_casella_vuota for c in range(self.dim_o) ] for r in range(self.dim_o) ]
		#adesso mi creo i giocatori in base a rtegole prefissate: il primo che muove sarà sempre un umano, poi un pc, poi un umano eventualmente
		#mi calcolo inizialmente la lista delle possibili case basi in relazione al numero di giocatori
		numero_totale_giocatori = numero_giocatori_umani + numero_giocatori_pc
		if numero_totale_giocatori == 2:
			lista_case_basi = [(self.dim_v-1, self.dim_o-1), (0,0)]
		else:
			#ho piu di 2 giocatori
			lista_case_basi = [ (self.dim_v-1, self.dim_o-1), (0, self.dim_o-1), (0,0), (self.dim_v-1, 0) ]
		#adesso creo la lista dei giocatori
		self.lista_giocatori = []
		while (numero_giocatori_pc + numero_giocatori_umani) >0:
			#parto a quello che ne ha di piu oppure dall'umano se hanno lo stesso numero
			id_giocatore = len(self.lista_giocatori)
			if numero_giocatori_pc > numero_giocatori_umani:
				#significa che il pc ne ha di piu o hanno lo stesso numero
				nuovo_giocatore = Giocatore.Giocatore(id_giocatore=id_giocatore, casa_base=lista_case_basi[id_giocatore], is_umano=False, dim_v=self.dim_v, dim_o=self.dim_o)
				self.lista_giocatori.append(nuovo_giocatore)
				numero_giocatori_pc -=1
			else:
				#gli umani ne hanno di piu
				nuovo_giocatore = Giocatore.Giocatore(id_giocatore=id_giocatore, casa_base=lista_case_basi[id_giocatore], is_umano=True, dim_o=self.dim_o, dim_v=self.dim_v)
				self.lista_giocatori.append(nuovo_giocatore)
				numero_giocatori_umani -=1
			
		#qui dovrei aver completato la lista dei giocatori
		#inserisco le pedine in casa base generatrici
		for i in range(len(self.lista_giocatori)):
			#i e l'id del giocatore
			(r,c) = self.lista_giocatori[i].casa_base
			#inserisco la generatrice nello schema
			self.schema_gioco[r][c] = len(self.lista_giocatori)*self.valore_pedina_generatrice + i

	############################################################################################################################################################################################
	#metodi generali
	def verifica_giocatori_eliminati(self):
		#metodo per verificare i giocatori sconfitti
		pass

	def verifica_partita_vinta(self, matrice_attuale:Union[List[List[int]], None]=None) -> bool:
		#metodo per verificare se rimane un solo giocatore in campo
		id_giocatore_trovato = -1
		if matrice_attuale is not None:
			schema_gioco = matrice_attuale
		else:
			schema_gioco = self.schema_gioco
		for r in range(self.dim_v):
			for c in range(self.dim_o):
				valore_casella = schema_gioco[r][c]
				if valore_casella != Costanti.numero_casella_vuota:
					#verifico il giocatore di appartenenza
					giocatore_appartenenza = valore_casella % len(self.lista_giocatori)
					if id_giocatore_trovato == -1:
						#allora me lo salvo in quella variabile
						id_giocatore_trovato = giocatore_appartenenza
					elif id_giocatore_trovato != giocatore_appartenenza:
						#ci sono almeno 2 giocatori
						return False
		#se arrivo qui non ho trovato altri giocatori -> dico che la partita e vinta
		return True


	################################################################################################################################################################################
	#metodo per rimuovere dallo schema tutte le caselle del giocatore
	def rimuovi_pedine_giocatore(self, id_giocatore:int, schema_gioco_input:Union[None, List[List[int]]] = None):
		#metodo per rimuopvere dallo schema tutte le caselle del giocatore selezionato
		if schema_gioco_input is None:
			schema_gioco = self.schema_gioco
		else:
			schema_gioco = schema_gioco_input
		for r_eli in range(self.dim_v):
			for c_eli in range(self.dim_o):
				valore_pedina = self.schema_gioco[r_eli][c_eli]
				if (valore_pedina != Costanti.numero_casella_vuota) and ((valore_pedina%len(self.lista_giocatori) ) == id_giocatore):
					#elimino quella pedina
					schema_gioco[r_eli][c_eli] = Costanti.numero_casella_vuota

	##############################################################################################################################################################################################
	#metodi per eseguire le mosse e fare i controlli necessari
	def esegui_mossa(self, origine:Tuple[int, int], destinazione:Tuple[int, int]) -> float:
		#metodo per eseguire una mossa e ritornare il punteggio accumulato per eseguire quella mossa
		#questo metodo si occupa anche di inserire la pedina nuova se necessario
		#mi ricavo il valore da spostare
		(r_o, c_o) = origine
		valore_spostamento = self.schema_gioco[r_o][c_o]
		#mi ricavo il giocatore di appartenenza 
		id_giocatore_appartenenza = valore_spostamento%len(self.lista_giocatori)
		tipo_pedina = valore_spostamento // len(self.lista_giocatori)
		#valuto se in questo caso mi conviene calcolare il numero delle pedine o no
		if self.flag_pesa_punteggi_numero_pedine:
			pedine_giocatori = NumeroPedineGiocatori(schema=self.schema_gioco, numero_giocatori=len(self.lista_giocatori))
		else:
			#me ne creo uno vuoto
			pedine_giocatori = NumeroPedineGiocatori(schema=[[]], numero_giocatori=0)
		#sposto la pedina
		self.schema_gioco[r_o][c_o] = Costanti.numero_casella_vuota
		(r_d, c_d) = destinazione
		self.schema_gioco[r_d][c_d] = valore_spostamento
		punteggio = 0
		#verifico subito se ho generato un damone
		if (self.flag_crea_damoni) and (tipo_pedina != self.valore_pedina_damone): #la pedina in questione non e gia un damone
			#devo inserire un damone se o riga o colonna sono quelli del giocatore associato
			valore_damone_creato = 0
			if self.flag_damone_solo_angolo:
				#devono corrispondere tutti e due
				if (r_d == self.lista_giocatori[id_giocatore_appartenenza].riga_damoni) and ( c_d == self.lista_giocatori[id_giocatore_appartenenza].colonna_damoni ):
					valore_damone_creato = self.valore_pedina_damone
			else:
				#non devo generarlo solo nell'angolo, posso generarlo se arrivo in riga o colonna opposta alla casa base madre
				if (r_d == self.lista_giocatori[id_giocatore_appartenenza].riga_damoni) or ( c_d == self.lista_giocatori[id_giocatore_appartenenza].colonna_damoni ):
					valore_damone_creato = self.valore_pedina_damone
			#aggiungo al punteggio il fatto di aver creato il damone
			if self.flag_pesa_punteggi_numero_pedine:
				punteggio += valore_damone_creato*self.valore_moltiplicatore_mangiata*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=id_giocatore_appartenenza)
			else:
				punteggio += valore_damone_creato*self.valore_moltiplicatore_mangiata
			#vado a m,etterlo nello schema
			if valore_damone_creato > 0:
				self.schema_gioco[r_d][c_d] = valore_damone_creato*len(self.lista_giocatori) + id_giocatore_appartenenza
		#per prima cosa verifico se ho mangiato
		if ( abs(r_d - r_o) == 2) or (abs(c_d - c_o) == 2 ):
			#significa che ho mangiato
			#qui controllo anche se ha vinto o eliminato un giocatore
			#mi ricavo la pedina mangiata
			valore_pedina_mangiata = self.schema_gioco[(r_o + r_d)//2][(c_o + c_d)//2]
			#svuoto il valore
			self.schema_gioco[(r_o + r_d)//2][(c_o + c_d)//2] = Costanti.numero_casella_vuota
			#mi ricavo il giocatore a cui e stata mangiata la pedina
			giocatore_perdita_subita = valore_pedina_mangiata%len(self.lista_giocatori)
			valore_perdita = valore_pedina_mangiata//len(self.lista_giocatori)
			if giocatore_perdita_subita == id_giocatore_appartenenza:
				#ho un punteggio negativo
				if self.flag_pesa_punteggi_numero_pedine:
					punteggio -= valore_perdita*self.valore_moltiplicatore_mangiata*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=giocatore_perdita_subita)
				else:
					punteggio -= valore_perdita*self.valore_moltiplicatore_mangiata
				#se ha mangiato l'ultima generatrice tolgo il punteggio relativo
				if valore_perdita == self.valore_pedina_generatrice:
					if self.lista_giocatori[giocatore_perdita_subita].verifica_giocatore_senza_generatrici(tabella_gioco=self.schema_gioco, 
																								numero_giocatori=len(self.lista_giocatori), 
																								valore_generatrice=self.valore_pedina_generatrice,
																								valore_damone=self.valore_pedina_damone, 
																								is_damone_genetratrice=self.flag_damone_funzione_generatrice):
						#il giocatore a cui e stata mangiata una generatrice ha perso tutte le generatrici
						punteggio -= self.valore_senza_generatrici
			else:
				#ho un punteggio positivo
				if self.flag_pesa_punteggi_numero_pedine:
					punteggio += valore_perdita*self.valore_moltiplicatore_mangiata*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=giocatore_perdita_subita)
				else:
					punteggio += valore_perdita*self.valore_moltiplicatore_mangiata
				#adesso devo valutare se ha eliminato il giocatore
				if self.lista_giocatori[giocatore_perdita_subita].verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
					#allora significa che ho eliminato il giocatore
					punteggio += self.valore_eliminazione_giocatore
					#a questo punto verifico se ho vinto la partita
					if self.verifica_partita_vinta():
						#aggiungo il punteggio di vincita poartita
						punteggio += self.valore_vincita_partita
				#adesso valuto se ha eliminato tutte le generatrici per quel giocatore
				if valore_perdita == self.valore_pedina_generatrice:
					if self.lista_giocatori[giocatore_perdita_subita].verifica_giocatore_senza_generatrici(tabella_gioco=self.schema_gioco, 
																			numero_giocatori=len(self.lista_giocatori), 
																			valore_generatrice=self.valore_pedina_generatrice,
																			valore_damone=self.valore_pedina_damone, 
																			is_damone_genetratrice=self.flag_damone_funzione_generatrice):
						#il giocatore a cui e stata mangiata una generatrice ha perso tutte le generatrici
						punteggio += self.valore_senza_generatrici
		#verifico se devo inserire una pedina
		pedina_generatrice_in_casa_base = (tipo_pedina == self.valore_pedina_generatrice) and (destinazione == self.lista_giocatori[id_giocatore_appartenenza].casa_base)
		damone_casa_base = (tipo_pedina == self.valore_pedina_damone) and ( self.flag_damone_funzione_generatrice) and (destinazione == self.lista_giocatori[id_giocatore_appartenenza].casa_base)
		if pedina_generatrice_in_casa_base or damone_casa_base:
			#allora devo inserire una nuova pedina
			#mi calcolo la coordinata dove inserire la nuova pedina
			(r_aggiunta, c_aggiunta) = self.lista_giocatori[id_giocatore_appartenenza].calcola_cella_libera_per_pedina_aggiunta(tabella_gioco=self.schema_gioco)
			#vado ad incrementare il numero di pedine per quel giocatore
			self.lista_giocatori[id_giocatore_appartenenza].pedine_aggiunte_dopo_ultima_aggiunta += 1
			#verifico il tipo di pedina da aggiungere
			if self.lista_giocatori[id_giocatore_appartenenza].pedine_aggiunte_dopo_ultima_aggiunta == self.numero_pedine_prima_generatrice:
				#devo aggiungere una generatrice
				valore_pedina_aggiunta = self.valore_pedina_generatrice
				#resetto il valore del giocatore
				self.lista_giocatori[id_giocatore_appartenenza].pedine_aggiunte_dopo_ultima_aggiunta = 0
			else:
				#altrimenti devo aggiungere una pedina normale
				valore_pedina_aggiunta = self.valore_pedina_semplice
			#adesso verifico se devo inserire un damone
			if self.flag_crea_damoni:
				#devo inserire un damone se o riga o colonna sono quelli del giocatore associato
				if self.flag_damone_solo_angolo:
					#devono corrispondere tutti e due
					if (r_aggiunta == self.lista_giocatori[id_giocatore_appartenenza].riga_damoni) and ( c_aggiunta == self.lista_giocatori[id_giocatore_appartenenza].colonna_damoni ):
						valore_pedina_aggiunta = self.valore_pedina_damone
				else:
					#non devo generarlo solo nell'angolo, posso generarlo se arrivo in riga o colonna opposta alla casa base madre
					if (r_aggiunta == self.lista_giocatori[id_giocatore_appartenenza].riga_damoni) or ( c_aggiunta == self.lista_giocatori[id_giocatore_appartenenza].colonna_damoni ):
						valore_pedina_aggiunta = self.valore_pedina_damone
			#incremento il punteggio
			if self.flag_pesa_punteggi_numero_pedine:
				punteggio += valore_pedina_aggiunta*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=id_giocatore_appartenenza)
			else:
				punteggio += valore_pedina_aggiunta
			#adesso inserisco la nuova pedina
			self.schema_gioco[r_aggiunta][c_aggiunta] = valore_pedina_aggiunta*len(self.lista_giocatori) + id_giocatore_appartenenza
			#adesso valuto se spostare la casa base del giocatore nella nuova casella
			if self.flag_sposta_casa_base:
				#verifico che non sia gia la casa base di qualcuno
				if (r_aggiunta, c_aggiunta) not in [ gg.casa_base for gg in self.lista_giocatori if not (gg.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori))) ]: #cerco solo tra quelli non eliminati
					#aggiorno la csa base del giocatore
					self.lista_giocatori[id_giocatore_appartenenza].casa_base = (r_aggiunta, c_aggiunta)
		#posso ritornare il punteggio raggiunto
		return punteggio

	def esegui_mossa_no_side_effect(self, origine:Tuple[int, int], destinazione:Tuple[int, int], matrice_iniziale:List[List[int]], lista_numero_pedine_aggiunte:List[int], lista_case_base:List[Tuple[int, int]]) -> EsitoEseguiMosseNoSideEffect:
		#metodo per eseguire una mossa e ritornare il punteggio accumulato per eseguire quella mossa insieme allo schema, non fa side effect
		#questo metodo si occupa anche di inserire la pedina nuova se necessario
		#mi copio i valori
		schema_gioco = [riga[:] for riga in matrice_iniziale]
		lista_numero_pedine_aggiunte_copia = lista_numero_pedine_aggiunte[:]
		lista_casa_base_copia = lista_case_base[:]
		#mi ricavo il valore da spostare
		(r_o, c_o) = origine
		valore_spostamento = schema_gioco[r_o][c_o]
		#mi ricavo il giocatore di appartenenza 
		id_giocatore_appartenenza = valore_spostamento%len(self.lista_giocatori)
		tipo_pedina = valore_spostamento // len(self.lista_giocatori)
		#valuto se in questo caso mi conviene calcolare il numero delle pedine o no
		if self.flag_pesa_punteggi_numero_pedine:
			pedine_giocatori = NumeroPedineGiocatori(schema=schema_gioco, numero_giocatori=len(self.lista_giocatori))
		else:
			#me ne creo uno vuoto
			pedine_giocatori = NumeroPedineGiocatori(schema=[[]], numero_giocatori=0)
		#sposto la pedina
		schema_gioco[r_o][c_o] = Costanti.numero_casella_vuota
		(r_d, c_d) = destinazione
		schema_gioco[r_d][c_d] = valore_spostamento
		punteggio = 0
		#verifico subito se ho generato un damone
		if (self.flag_crea_damoni) and (tipo_pedina != self.valore_pedina_damone): #la pedina in questione non e gia un damone
			#devo inserire un damone se o riga o colonna sono quelli del giocatore associato
			valore_damone_creato = 0
			if self.flag_damone_solo_angolo:
				#devono corrispondere tutti e due
				if (r_d == self.lista_giocatori[id_giocatore_appartenenza].riga_damoni) and ( c_d == self.lista_giocatori[id_giocatore_appartenenza].colonna_damoni ):
					valore_damone_creato = self.valore_pedina_damone
			else:
				#non devo generarlo solo nell'angolo, posso generarlo se arrivo in riga o colonna opposta alla casa base madre
				if (r_d == self.lista_giocatori[id_giocatore_appartenenza].riga_damoni) or ( c_d == self.lista_giocatori[id_giocatore_appartenenza].colonna_damoni ):
					valore_damone_creato = self.valore_pedina_damone
			#aggiungo al punteggio il fatto di aver creato il damone
			if self.flag_pesa_punteggi_numero_pedine:
				punteggio += valore_damone_creato*self.valore_moltiplicatore_mangiata*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=id_giocatore_appartenenza)
			else:
				punteggio += valore_damone_creato*self.valore_moltiplicatore_mangiata
			#vado a m,etterlo nello schema
			if valore_damone_creato > 0:
				schema_gioco[r_d][c_d] = valore_damone_creato*len(self.lista_giocatori) + id_giocatore_appartenenza
		#per prima cosa verifico se ho mangiato
		if ( abs(r_d - r_o) == 2) or (abs(c_d - c_o) == 2 ):
			#significa che ho mangiato
			#qui controllo anche se ha vinto o eliminato un giocatore
			#mi ricavo la pedina mangiata
			valore_pedina_mangiata = schema_gioco[(r_o + r_d)//2][(c_o + c_d)//2]
			#svuoto il valore
			schema_gioco[(r_o + r_d)//2][(c_o + c_d)//2] = Costanti.numero_casella_vuota
			#mi ricavo il giocatore a cui e stata mangiata la pedina
			giocatore_perdita_subita = valore_pedina_mangiata%len(self.lista_giocatori)
			valore_perdita = valore_pedina_mangiata//len(self.lista_giocatori)
			if giocatore_perdita_subita == id_giocatore_appartenenza:
				#ho un punteggio negativo
				if self.flag_pesa_punteggi_numero_pedine:
					punteggio -= valore_perdita*self.valore_moltiplicatore_mangiata*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=giocatore_perdita_subita)
				else:
					punteggio -= valore_perdita*self.valore_moltiplicatore_mangiata
				#se ha mangiato l'ultima generatrice tolgo il punteggio relativo
				if valore_perdita == self.valore_pedina_generatrice:
					if self.lista_giocatori[giocatore_perdita_subita].verifica_giocatore_senza_generatrici(tabella_gioco=schema_gioco, 
																								numero_giocatori=len(self.lista_giocatori), 
																								valore_generatrice=self.valore_pedina_generatrice,
																								valore_damone=self.valore_pedina_damone, 
																								is_damone_genetratrice=self.flag_damone_funzione_generatrice):
						#il giocatore a cui e stata mangiata una generatrice ha perso tutte le generatrici
						punteggio -= self.valore_senza_generatrici
			else:
				#ho un punteggio positivo
				if self.flag_pesa_punteggi_numero_pedine:
					punteggio += valore_perdita*self.valore_moltiplicatore_mangiata*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=giocatore_perdita_subita)
				else:
					punteggio += valore_perdita*self.valore_moltiplicatore_mangiata
				#adesso devo valutare se ha eliminato il giocatore
				if self.lista_giocatori[giocatore_perdita_subita].verifica_giocatore_eliminato(tabella_gioco=schema_gioco, numero_giocatori=len(self.lista_giocatori)):
					#allora significa che ho eliminato il giocatore
					punteggio += self.valore_eliminazione_giocatore
					#a questo punto verifico se ho vinto la partita
					if self.verifica_partita_vinta(matrice_attuale=schema_gioco):
						#aggiungo il punteggio di vincita poartita
						punteggio += self.valore_vincita_partita
				#adesso valuto se ha eliminato tutte le generatrici per quel giocatore
				if valore_perdita == self.valore_pedina_generatrice:
					if self.lista_giocatori[giocatore_perdita_subita].verifica_giocatore_senza_generatrici(tabella_gioco=schema_gioco, 
																			numero_giocatori=len(self.lista_giocatori), 
																			valore_generatrice=self.valore_pedina_generatrice,
																			valore_damone=self.valore_pedina_damone, 
																			is_damone_genetratrice=self.flag_damone_funzione_generatrice):
						#il giocatore a cui e stata mangiata una generatrice ha perso tutte le generatrici
						punteggio += self.valore_senza_generatrici
		#verifico se devo inserire una pedina
		pedina_generatrice_in_casa_base = (tipo_pedina == self.valore_pedina_generatrice) and (destinazione == lista_casa_base_copia[id_giocatore_appartenenza])
		damone_casa_base = (tipo_pedina == self.valore_pedina_damone) and ( self.flag_damone_funzione_generatrice) and (destinazione == lista_casa_base_copia[id_giocatore_appartenenza])
		if pedina_generatrice_in_casa_base or damone_casa_base:
			#allora devo inserire una nuova pedina
			#mi calcolo la coordinata dove inserire la nuova pedina
			(r_aggiunta, c_aggiunta) = self.lista_giocatori[id_giocatore_appartenenza].calcola_cella_libera_per_pedina_aggiunta(tabella_gioco=schema_gioco)
			#vado ad incrementare il numero di pedine per quel giocatore
			lista_numero_pedine_aggiunte_copia[id_giocatore_appartenenza] += 1
			#verifico il tipo di pedina da aggiungere
			if lista_numero_pedine_aggiunte_copia[id_giocatore_appartenenza] == self.numero_pedine_prima_generatrice:
				#devo aggiungere una generatrice
				valore_pedina_aggiunta = self.valore_pedina_generatrice
				#resetto il valore del giocatore
				lista_numero_pedine_aggiunte_copia[id_giocatore_appartenenza] = 0
			else:
				#altrimenti devo aggiungere una pedina normale
				valore_pedina_aggiunta = self.valore_pedina_semplice
			#adesso verifico se devo inserire un damone
			if self.flag_crea_damoni:
				#devo inserire un damone se o riga o colonna sono quelli del giocatore associato
				if self.flag_damone_solo_angolo:
					#devono corrispondere tutti e due
					if (r_aggiunta == self.lista_giocatori[id_giocatore_appartenenza].riga_damoni) and ( c_aggiunta == self.lista_giocatori[id_giocatore_appartenenza].colonna_damoni ):
						valore_pedina_aggiunta = self.valore_pedina_damone
				else:
					#non devo generarlo solo nell'angolo, posso generarlo se arrivo in riga o colonna opposta alla casa base madre
					if (r_aggiunta == self.lista_giocatori[id_giocatore_appartenenza].riga_damoni) or ( c_aggiunta == self.lista_giocatori[id_giocatore_appartenenza].colonna_damoni ):
						valore_pedina_aggiunta = self.valore_pedina_damone
			#incremento il punteggio
			if self.flag_pesa_punteggi_numero_pedine:
				punteggio += valore_pedina_aggiunta*pedine_giocatori.calcola_fattore_moltiplicatore_giocatore(id_giocatore=id_giocatore_appartenenza)
			else:
				punteggio += valore_pedina_aggiunta
			#adesso inserisco la nuova pedina
			schema_gioco[r_aggiunta][c_aggiunta] = valore_pedina_aggiunta*len(self.lista_giocatori) + id_giocatore_appartenenza
			#adesso valuto se spostare la casa base del giocatore nella nuova casella
			if self.flag_sposta_casa_base:
				#verifico che non sia gia la casa base di qualcuno
				if (r_aggiunta, c_aggiunta) not in [ lista_casa_base_copia[i] for i in range(len(self.lista_giocatori)) if not (self.lista_giocatori[i].verifica_giocatore_eliminato(tabella_gioco=schema_gioco, numero_giocatori=len(self.lista_giocatori))) ]: #cerco solo tra quelli non eliminati
					#aggiorno la csa base del giocatore
					lista_casa_base_copia[id_giocatore_appartenenza] = (r_aggiunta, c_aggiunta)
		#posso ritornare il punteggio raggiunto
		#mi creo l'esito
		esito_ritorno = EsitoEseguiMosseNoSideEffect(schema=schema_gioco, numero_pedine_giocatori=lista_numero_pedine_aggiunte_copia, case_basi=lista_casa_base_copia, punteggio=punteggio)
		return esito_ritorno

	#################################################################################################################################################################################

	#########################################################################################################################################################################################
	#metodi per calcolare le mosse del computer nei vari casi
	def ripristina_schema(self, schema_bk:List[List[int]], numero_pedine:List[int], lista_eliminati:List[bool], lista_case_basi:List[Tuple[int, int]]):
		#metodo per ripristinare lo schema
		self.schema_gioco = [riga[:] for riga in schema_bk]
		#ripristino i numeri delle pedine degli utenti e i flag delle eliminazioni
		for id_giocatore in range(len(self.lista_giocatori)):
			self.lista_giocatori[id_giocatore].pedine_aggiunte_dopo_ultima_aggiunta = numero_pedine[id_giocatore]
			self.lista_giocatori[id_giocatore].is_eliminato = lista_eliminati[id_giocatore]
			self.lista_giocatori[id_giocatore].casa_base = lista_case_basi[id_giocatore]

	def calcola_differenza_valori_schemi_per_giocatore(self, schema_finale:List[List[int]], id_giocatore_analisi:int) -> float:
		#metodo per calcolare la differenza di punteggio per un giocatore tra lo schema attuale e quello successivo, da fare dopo aver ripristinato lo schema
		#calcola il valore per un singolo giocatore
		#mi creo 2 liste con la somma dei valori delle pedine per ciascun giocatore
		#sono rilevanti le pedine proprie, il fatto che si abbia vinto/perso e il fatto che qualche giocatore sia stato eliminato
		lista_punteggi_attuale = [0 for i in range(len(self.lista_giocatori))]
		lista_punteggi_futuri = [0 for i in range(len(self.lista_giocatori))]
		#adesso itero tra tutte le celle e le vado a considerare
		for r in range(self.dim_v):
			for c in range(self.dim_o):
				cella_attuale = self.schema_gioco[r][c]
				cella_futura = schema_finale[r][c]
				#valuto le varie celle
				if cella_attuale != Costanti.numero_casella_vuota:
					#allora apprtiene a un giocatore, ricavo il giocatore e aggiorno la rispettiva lista
					id_giocatore = cella_attuale%len(self.lista_giocatori)
					valore_pedina = cella_attuale // len(self.lista_giocatori)
					lista_punteggi_attuale[id_giocatore] += valore_pedina
				if cella_futura != Costanti.numero_casella_vuota:
					#allora apprtiene a un giocatore, ricavo il giocatore e aggiorno la rispettiva lista
					id_giocatore = cella_futura%len(self.lista_giocatori)
					valore_pedina = cella_futura // len(self.lista_giocatori)
					lista_punteggi_futuri[id_giocatore] += valore_pedina
		#adesso valuto i punteggi in base al giocatore in questione
		if lista_punteggi_futuri[id_giocatore_analisi] == 0:
			return -self.valore_eliminazione_giocatore - self.valore_vincita_partita
		#altrimenti valuto se ho eliminato qualche giocatore con la mia mossa
		punteggio_raggiunto = sum( [self.valore_eliminazione_giocatore for i in range(len(self.lista_giocatori)) if ( (lista_punteggi_attuale[i]!=0) and (lista_punteggi_futuri[i] == 0) ) ] )
		#a questo punteggio ci sommo il valore se ho vinto la partita
		#per aver vinto significa che i punteggi futuri diversi da 0 sono solo 1
		if lista_punteggi_futuri.count(0) == (len(self.lista_giocatori)-1 ):
			#significa che il giocatore ha vinto la partita punteggio positivo
			punteggio_raggiunto += self.valore_vincita_partita
		#adesso faccio la differenza tra il finale e l'iniziale
		punteggio_raggiunto += (lista_punteggi_futuri[id_giocatore_analisi] - lista_punteggi_attuale[id_giocatore_analisi])*self.valore_moltiplicatore_mangiata
		return punteggio_raggiunto
		
	def calcola_punteggio_generatrice_piu_vicina(self, id_giocatore:int, schema_gioco_input:Union[None, List[List[int]]] = None, lista_case_basi:Union[None, List[Tuple[int, int]]] = None) -> float:
		#metodo per calcolare il punteggio della generatrice piu vicina allo schema
		#vado a ricercare tutte le generatrici di quel giocatore
		if schema_gioco_input is None:
			schema_gioco = self.schema_gioco
		else:
			schema_gioco = schema_gioco_input
		#mi ricavo la casa base
		if lista_case_basi is None:
			(r_base, c_base) = self.lista_giocatori[id_giocatore].casa_base
		else:
			(r_base, c_base) = lista_case_basi[id_giocatore]
		n_giocatori = len(self.lista_giocatori)
		#se la sua casa base e occupata ritorno subito 0
		if (schema_gioco[r_base][c_base] != Costanti.numero_casella_vuota) and ( (schema_gioco[r_base][c_base]%n_giocatori) != id_giocatore ):
			return 0
		lista_distanze = [ (abs(r-r_base) + abs(c-c_base)) for r in range(self.dim_v) for c in range(self.dim_o) if ( (schema_gioco[r][c] != Costanti.numero_casella_vuota) and (schema_gioco[r][c]%n_giocatori == id_giocatore) and (schema_gioco[r][c]//n_giocatori == self.valore_pedina_generatrice) ) ]
		#se i damoni sono generatrici cerco anche i damoni
		if self.flag_damone_funzione_generatrice:
			#devo distinguere se i damoni si possono muovere in diagonale o meno
			if self.flag_damone_mossa_diagonale:
				#altrimenti basta che prendo il massimo tra le 2 distanze
				lista_distanze_damoni = [ max(abs(r-r_base), abs(c-c_base)) for r in range(self.dim_v) for c in range(self.dim_o) if ( (schema_gioco[r][c] != Costanti.numero_casella_vuota) and (schema_gioco[r][c]%n_giocatori == id_giocatore) and (schema_gioco[r][c]//n_giocatori == self.valore_pedina_damone) ) ]
			else:
				#i damoni possono muoversi in diagonale quindi li equiparo alle pedine generatrici
				lista_distanze_damoni = [ (abs(r-r_base) + abs(c-c_base)) for r in range(self.dim_v) for c in range(self.dim_o) if ( (schema_gioco[r][c] != Costanti.numero_casella_vuota) and (schema_gioco[r][c]%n_giocatori == id_giocatore) and (schema_gioco[r][c]//n_giocatori == self.valore_pedina_damone) ) ]
		else:
			lista_distanze_damoni = []
		#vado a calcolarmi il secondo fattore
		if len(lista_distanze) == 0:
			#non ho generatrici, il secondo fattore è 0, potrei gia ritornare
			return 0
		secondo_fattore = self.dim_o + self.dim_v + 1 - min(lista_distanze + lista_distanze_damoni)
		return self.valore_generatrice_piu_vicina*secondo_fattore

	
	
	'''
	def calcola_lista_mosse_sequenza_mangiare(self, id_giocatore:int) -> List[ List[ Tuple[int, int] ] ]:
		#metodo per calcolare la lisat di tutte le mosse in successioni per mangiare
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		bk_schema_gioco = [ riga[:] for riga in self.schema_gioco ]

	def calcola_lista_mosse_mangiare_da_casella(self, id_giocatore:int, r:int, c:int) -> List[List[Tuple[int, int]]]:
		#metodo per calcolare le mosse successive a per mangiare partendo da una casella prefissata
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		bk_schema_gioco = [ riga[:] for riga in self.schema_gioco ]
		#mi ricavo la lista delle mosse supponendo lo schema gia modificato
		lista_mosse_successive = []
	'''

	def esegui_successione_mosse(self, lista_mosse:List[Tuple[int, int]]) -> float:
		#metodo che esegue una serie di mosse in successione e ne ritorna il punteggio complessivo
		punteggio_attuale = 0
		for i in range(len(lista_mosse) -1):
			#eseguo la mossa ed incremento il punteggio attuale
			punteggio_attuale += self.esegui_mossa(origine=lista_mosse[i], destinazione=lista_mosse[i+1])
		return punteggio_attuale

	def esegui_successione_mosse_no_side_effect(self, lista_mosse:List[Tuple[int, int]], matrice_attuale:List[List[int]], lista_numeri_pedine_aggiunte:List[int], lista_case_basi:List[Tuple[int, int]]) -> EsitoEseguiMosseNoSideEffect:
		#metodo che esegue una serie di mosse in successione e ne ritorna il punteggio complessivo
		punteggio_attuale = 0
		esito_mosse = EsitoEseguiMosseNoSideEffect(schema=matrice_attuale, numero_pedine_giocatori=lista_numeri_pedine_aggiunte, case_basi=lista_case_basi, punteggio=0)
		for i in range(len(lista_mosse) -1):
			#eseguo la mossa ed incremento il punteggio attuale
			esito_mosse = self.esegui_mossa_no_side_effect(origine=lista_mosse[i], destinazione=lista_mosse[i+1], matrice_iniziale=esito_mosse.matrice, lista_numero_pedine_aggiunte=esito_mosse.numero_pedine_aggiunte_giocatori, lista_case_base=esito_mosse.lista_case_basi,)
			punteggio_attuale += esito_mosse.punteggio
		#al termine metto il punteggio raggiunto nell'ultimo esito
		esito_mosse.punteggio = punteggio_attuale
		return esito_mosse

	def calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(self, lista_mosse:List[Tuple[int, int]], id_giocatore:int) -> float:
		#metodo che esegue le mosse e calcola tutti i punteggi stabiliti
		#per prima cosa eseguo la sequenza di mosse
		punteggio = 0
		punteggio += self.esegui_successione_mosse(lista_mosse=lista_mosse)
		#adesso calcolo il punteggio per la generatrice piu vicina alla casa base
		punteggio += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore)
		#adesso vado a verificare se il giocatore ha la casa base occupata oppure ha occupato la casa base di qualcun altro
		n_giocatori = len(self.lista_giocatori)
		for giocatore in self.lista_giocatori:
			if not(giocatore.is_eliminato):
				(r_base, c_base) = giocatore.casa_base
				valore_pedina = self.schema_gioco[r_base][c_base]
				if valore_pedina != Costanti.numero_casella_vuota:
					giocatore_pedina = valore_pedina%n_giocatori
					if (giocatore_pedina == id_giocatore) and (giocatore.id_giocatore != id_giocatore):
						#allora il giocatore ha posizionato una sua pedina in una casa base di qualcun altro
						punteggio += self.valore_occupazione_casa_base
					elif (giocatore_pedina != id_giocatore) and (giocatore.id_giocatore == id_giocatore):
						#qualcun altro ha occupato la casa base del giocatore in analisi, il punteggio si sottrarrà
						punteggio -= self.valore_occupazione_casa_base
		#al termine posso ritornare il punteggio
		return punteggio

	def calcola_punteggio_complessivo_nuovo_schema_esegui_mosse_no_side_effect(self, lista_mosse:List[Tuple[int, int]], id_giocatore:int,  matrice_attuale:List[List[int]], lista_numeri_pedine_aggiunte:List[int], lista_case_basi:List[Tuple[int, int]]) -> EsitoEseguiMosseNoSideEffect:
		#metodo che esegue le mosse e calcola tutti i punteggi stabiliti
		#per prima cosa eseguo la sequenza di mosse
		punteggio = 0
		esito_mosse_no_side_effect = self.esegui_successione_mosse_no_side_effect(lista_mosse=lista_mosse,
																					lista_case_basi=lista_case_basi,
																					lista_numeri_pedine_aggiunte=lista_numeri_pedine_aggiunte,
																					matrice_attuale=matrice_attuale)
		punteggio += esito_mosse_no_side_effect.punteggio
		#adesso calcolo il punteggio per la generatrice piu vicina alla casa base
		punteggio += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore, 
																	lista_case_basi=esito_mosse_no_side_effect.lista_case_basi,
																	schema_gioco_input=esito_mosse_no_side_effect.matrice)
		#adesso vado a verificare se il giocatore ha la casa base occupata oppure ha occupato la casa base di qualcun altro
		n_giocatori = len(self.lista_giocatori)
		for giocatore in self.lista_giocatori:
			if not(giocatore.is_eliminato):
				(r_base, c_base) = esito_mosse_no_side_effect.lista_case_basi[giocatore.id_giocatore]
				valore_pedina = esito_mosse_no_side_effect.matrice[r_base][c_base]
				if valore_pedina != Costanti.numero_casella_vuota:
					giocatore_pedina = valore_pedina%n_giocatori
					if (giocatore_pedina == id_giocatore) and (giocatore.id_giocatore != id_giocatore):
						#allora il giocatore ha posizionato una sua pedina in una casa base di qualcun altro
						punteggio += self.valore_occupazione_casa_base
					elif (giocatore_pedina != id_giocatore) and (giocatore.id_giocatore == id_giocatore):
						#qualcun altro ha occupato la casa base del giocatore in analisi, il punteggio si sottrarrà
						punteggio -= self.valore_occupazione_casa_base
		#al termine posso ritornare il punteggio
		esito_mosse_no_side_effect.punteggio = punteggio
		return esito_mosse_no_side_effect

	
	




		


		
#script che contiene l'esito per la ricerca della lisatd elle mosse

from __future__ import annotations
from typing import List, Tuple

import Costanti, Giocatore

class EsitoRicercaListaMosse:
	matrice_finale : List[List[int]]
	lista_mosse_calcolate : List[Tuple[int, int]]

	def __init__(self, matrice_finale:List[List[int]], lista_mosse:List[Tuple[int, int]], flag_salva_mosse:bool) -> None:
		self.matrice_finale = matrice_finale
		if flag_salva_mosse:
			#me lo salvo solo se la lunghezza e positiva
			self.lista_mosse_calcolate = lista_mosse

	def elimina_giocatore_schema(self, id_giocatore: int, numero_giocatori:int) -> List[List[int]]:
		#metodo per eliminare dallo schema attuale il giocatore e ritornare lo schema aggiornato
		schema_ritorno = [riga[:] for riga in self.matrice_finale]
		dim_v = len(schema_ritorno)
		dim_o = len(schema_ritorno[0])
		for r in range(dim_v):
			for c in range(dim_o):
				if ( (schema_ritorno[r][c] != Costanti.numero_casella_vuota) and ( (schema_ritorno[r][c]%numero_giocatori) == id_giocatore ) ):
					#lo tolgo dallo schema
					schema_ritorno[r][c] = Costanti.numero_casella_vuota
		return schema_ritorno


def esegui_mossa_no_punteggio(matrice_iniziale:List[List[int]], partenza:Tuple[int, int], destinazione:Tuple[int, int])-> List[List[int]]:
	#metodo per eseguire una mossa semplice senza calcolarne il punteggio disinterressandosi anche del fatto che le 2 celle non siano piene
	schema_ritorno = [riga[:] for riga in matrice_iniziale]
	(r_p, c_p) = partenza
	(r_d, c_d) = destinazione
	schema_ritorno[r_d][c_d] = schema_ritorno[r_p][c_p]
	schema_ritorno[r_p][c_p] = Costanti.numero_casella_vuota
	#vedo se ho mangiato
	if ( abs(r_d - r_p) == 2) or  (abs(c_d - c_p) == 2 ) :
		schema_ritorno[ (r_p + r_d)//2 ][(c_p+c_d)//2] = Costanti.numero_casella_vuota
	return schema_ritorno

def calcola_lista_mosse_complessive(matrice_inziale:List[List[int]], 
									giocatore_in_questione:Giocatore.Giocatore, 
									flag_mangiare_proprie_pedine:bool, 
									flag_obbligo_mangiare:bool, 
									numero_giocatori:int, 
									flag_salva_lista_mosse:bool,
									flag_damone_mossa_diagonale : bool,
									valore_casella_damone : int,
									flag_damoni_mangiano_diagonale:bool,
									flag_damone_mangiato_solo_da_damoni:bool
									) -> List[ EsitoRicercaListaMosse ]:
	#metodo per calcolare la lista degli esiti delle mosse con i relativi schemi
	lista_esiti_tmp :List[EsitoRicercaListaMosse]
	lista_esiti_definitivi : List[EsitoRicercaListaMosse] = []
	#mi calcolo la lista degli esiti tmp e dopo eventualmente sposto da una all'altra
	lista_esiti_tmp = [ EsitoRicercaListaMosse(matrice_finale=esegui_mossa_no_punteggio(matrice_iniziale=matrice_inziale,
																							partenza=i[0],
																							destinazione=i[1]), 
												lista_mosse=list(i),
												flag_salva_mosse=True
											) 
						for i in giocatore_in_questione.calcola_lista_mosse_possibili_totale(schema_gioco=matrice_inziale, 
																						flag_mangiare_proprie_pedine=flag_mangiare_proprie_pedine, 
																						numero_giocatori=numero_giocatori, 
																						flag_obbligatorio_mangiare=flag_obbligo_mangiare,
																						flag_damone_mossa_diagonale=flag_damone_mossa_diagonale, 
																						valore_casella_damone= valore_casella_damone,
																						flag_damoni_mangiano_diagonale=flag_damoni_mangiano_diagonale,
																						flag_damone_mangiato_solo_da_damoni=flag_damone_mangiato_solo_da_damoni) 
					]
	#adesso fino a quando la lista tmp e vuota vadfo ag aggiungere elementi a quella definitiva
	while len(lista_esiti_tmp) >0:
		#prendo anche l'ultimo elemento 
		esito_in_analisi = lista_esiti_tmp.pop(0)
		#mi calcolo l'ultima mossa
		[ (r_pen, c_pen), (r_ult, c_ult) ] = esito_in_analisi.lista_mosse_calcolate[-2:]
		#se ho mangiato vado a calcolare tutte le possibili mosse
		if ( abs(r_ult - r_pen) ==1) or ( abs( c_ult - c_pen ) ==1 ):
			#e una mossa semplice la inserisco subito
			nuovo_esito = EsitoRicercaListaMosse(matrice_finale=esito_in_analisi.matrice_finale, lista_mosse=esito_in_analisi.lista_mosse_calcolate, flag_salva_mosse=flag_salva_lista_mosse)
			lista_esiti_definitivi.append(nuovo_esito)
		else:
			#ho mangiato, valuto se ho l'obbligo di mangiare e quindi di continuare
			#mi calcolo le mosse successive
			lista_mosse_successive = giocatore_in_questione.calcola_lista_successive_celle_per_mangiare_singola_casella(tabella_gioco=esito_in_analisi.matrice_finale,
																						flag_mangiare_proprie_pedine=flag_mangiare_proprie_pedine,
																						numero_giocatori=numero_giocatori,
																						r=r_ult,
																						c=c_ult,
																						flag_damoni_mangiano_diagonale=flag_damoni_mangiano_diagonale, 
																						valore_damone=valore_casella_damone,
																						flag_damone_mangiato_solo_da_damoni=flag_damone_mangiato_solo_da_damoni
																													)
			if len(lista_mosse_successive) == 0:
				#la aggiungo senza dubbi 
				#distinguo se mi serve o meno la lista delle mosse
				#e una mossa semplice la inserisco subito
				nuovo_esito = EsitoRicercaListaMosse(matrice_finale=esito_in_analisi.matrice_finale, lista_mosse=esito_in_analisi.lista_mosse_calcolate, flag_salva_mosse=flag_salva_lista_mosse)
				lista_esiti_definitivi.append(nuovo_esito)
			else:
				#ho mosse per cui posso continuare, le aggiungo di sicuro alla tmp
				for next_cella in lista_mosse_successive:
					nuovo_esito_tmp = EsitoRicercaListaMosse(matrice_finale=esegui_mossa_no_punteggio(matrice_iniziale=esito_in_analisi.matrice_finale,
																										partenza=esito_in_analisi.lista_mosse_calcolate[-1],
																										destinazione=next_cella), 
															lista_mosse=esito_in_analisi.lista_mosse_calcolate + [next_cella],
															flag_salva_mosse=True)
					lista_esiti_tmp.append(nuovo_esito_tmp)
					#adesso se non ho l'obbligo di mangiare aggiungo quello attuale alla definitiva
					if not(flag_obbligo_mangiare):
						#distinguo se mi serve o meno la lista delle mosse
						#e una mossa semplice la inserisco subito
						nuovo_esito = EsitoRicercaListaMosse(matrice_finale=esito_in_analisi.matrice_finale, lista_mosse=esito_in_analisi.lista_mosse_calcolate, flag_salva_mosse=flag_salva_lista_mosse)
						lista_esiti_definitivi.append(nuovo_esito)
	#al termine posso ritornare la lista degli esiti definitivi
	return lista_esiti_definitivi
		
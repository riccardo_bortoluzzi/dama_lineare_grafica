#script che contiene la classe per calcolare il numero di pedine di ogni giocatore

from typing import List

import Costanti


class NumeroPedineGiocatori:
	lista_numero_pedine : List[int]

	def __init__(self, schema:List[List[int]], numero_giocatori:int) -> None:
		#metodo che si calcola le pedine di ciascun giocatore e le assegna ai metodi
		#mi calcolo le dimensioni dello schmea
		dim_v = len(schema)
		dim_o = len(schema[0])
		#mi calcolo la lista delle pedine
		self.lista_numero_pedine = [ 0 for i in range(numero_giocatori)]
		for c in range(dim_o):
			for r in range(dim_v):
				#verifico che non sia una casella vuota
				if schema[r][c] != Costanti.numero_casella_vuota:
					#mi calcolo il giocatore di appartenenza
					giocatore_appartenenza = schema[r][c] % numero_giocatori
					self.lista_numero_pedine[giocatore_appartenenza] += 1

	def calcola_fattore_moltiplicatore_giocatore(self, id_giocatore:int) -> float:
		#metodo che calcola il fattore di moltiplicatore per un particolare giocatore
		numero_pedine_giocatore = self.lista_numero_pedine[id_giocatore]
		numero_pedine_totali = sum(self.lista_numero_pedine) 
		return 2**((numero_pedine_totali -2*numero_pedine_giocatore) / numero_pedine_totali) 
		#return numero_pedine_altri_giocatori/numero_pedine_giocatore

	'''
	tra 1/2 e 2 
si poswsono esprimere come potenze di 2
2^-1:1

2^-1 se io ho tutte le pedine
2^1 se non ho pedine

2^0 se le pedine sono uguali per entrambi

deve essere 2^qualcosa con qualcosa comrpeso tra -1 e 1

potrebbe essere del tipo:
le mie pedine - quelle di tutti gli altri?

es con 63 rosse e 1 verde
punteggio verde = 1-63 / 64 = -0.98 -> il -1 dovrebbe essere quello carattestico del rosso -> pedine di altri - le mie / pedine totali
	'''
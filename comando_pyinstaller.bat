python -m pip install --upgrade pip
python -m pip install --upgrade pyinstaller 
python -m pip install --upgrade pyqt5
python -m pip install --upgrade -r requirements.txt

pyinstaller --distpath .\compilato_windows ^
            --noconfirm ^
			--clean ^
			--onefile ^
			--name dama_lineare ^
			--add-data=".\immagini_pedine\generatrice_black.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\generatrice_blue.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\generatrice_green.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\generatrice_red.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\pedina_black.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\pedina_blue.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\pedina_green.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\pedina_red.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\damone_black.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\damone_blue.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\damone_green.png;immagini_pedine" ^
			--add-data=".\immagini_pedine\damone_red.png;immagini_pedine" ^
			--hiddenimport os ^
			--hiddenimport typing ^
			--hiddenimport random ^
			--hiddenimport math ^
			--hiddenimport PyQt5 ^
			--hiddenimport sys ^
			--hiddenimport traceback ^
			--hiddenimport time ^
			--hiddenimport Costanti ^
			--hiddenimport EsitoAlgoritmoRicerca ^
			--hiddenimport EsitoRicercaListaMosse ^
			--hiddenimport Giocatore ^
			--hiddenimport Gioco ^
			--hiddenimport GraficaDamaLineare ^
			--hiddenimport ThreadGrafica ^
			--hiddenimport Ui_DamaLineare ^
			--console ^
			--icon .\immagini_pedine\icona_windows.ico ^
			--version-file .\version_file.txt ^
			--win-private-assemblies ^
			--win-no-prefer-redirects ^
			main_dama_lineare.py

pause
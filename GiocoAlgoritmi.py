#script che contiene la classe dell'algoritmo che contiene tutti gli algoritmi
from __future__ import annotations
import math

import random
import threading
from typing import List, Tuple
from EsitoAlgoritmoRicerca import EsitoAlgoritmoRicerca
import EsitoRicercaListaMosse
from Giocatore import Giocatore

from GiocoBase import GiocoBase


class GiocoAlgoritmi(GiocoBase):

	def __init__(self, dim_v: int, dim_o: int, numero_giocatori_umani: int, numero_giocatori_pc: int, valore_semplice: int, valore_generatrice: int, algoritmo: str, valore_eliminazione_giocatore: int, valore_vincita_partita: int, pedine_prima_generatrice: int, mangiare_proprie_pedine: bool, obbligatorio_mangiare: bool, id_partita: int, valore_moltiplicatore: float, generatrice_piu_vicina: float, valore_occupazione_casa_base: int, valore_senza_generatrici: int, flag_pesa_punteggi: bool, flag_sposta_casa_base: bool, flag_crea_damoni: bool, valore_damoni: int, flag_damone_solo_angolo: bool, flag_damoni_mossa_diagonale: bool, flag_damoni_generatrice: bool, flag_damone_mangia_diagonale: bool, flag_damone_mangiato_solo_damoni: bool) -> None:
		super().__init__(dim_v, dim_o, numero_giocatori_umani, numero_giocatori_pc, valore_semplice, valore_generatrice, algoritmo, valore_eliminazione_giocatore, valore_vincita_partita, pedine_prima_generatrice, mangiare_proprie_pedine, obbligatorio_mangiare, id_partita, valore_moltiplicatore, generatrice_piu_vicina, valore_occupazione_casa_base, valore_senza_generatrici, flag_pesa_punteggi, flag_sposta_casa_base, flag_crea_damoni, valore_damoni, flag_damone_solo_angolo, flag_damoni_mossa_diagonale, flag_damoni_generatrice, flag_damone_mangia_diagonale, flag_damone_mangiato_solo_damoni)

	############################################################################################################################################################################
	#per decidere l'algoritmo da fare
	#metodo per calcolare la mossa del pc
	def calcola_mossa_pc(self, giocatore_attuale:int, profondita:int, numero_massimo_thread:int) -> EsitoAlgoritmoRicerca:
		#metodo che in base all'algoritmo scelto ritorna l'esito della mossa scelta dal pc
		#mi ripristino lo schema anche qui
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		case_basi_iniziali = [ i.casa_base for i in self.lista_giocatori ]
		#mi ricavo la descrizione dell'algoritmo
		if self.algoritmo_ricerca == "Algoritmo min/max":
			#algoritmo min max
			#migliore_esito = self.algoritmo_min_max(profondita=profondita, id_giocatore=giocatore_attuale)
			#migliore_esito_multigiocatore = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_attuale)
			#verifico i punteggi 
			#print(f'Punteggio clas: {migliore_esito.punteggio}' )
			#print(f'punteggio mult: {migliore_esito_multigiocatore.punteggio}')
			migliore_esito = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=False)
		elif self.algoritmo_ricerca == "Simulazione mosse altri giocatori":
			#chiamo l'altro algoritmo di ricerca  con simulazione e calcolo dalle matrici
			migliore_esito = self.algoritmo_valuta_schema_profondita(profondita=profondita, id_giocatore=giocatore_attuale)
		elif self.algoritmo_ricerca == "Algoritmo mosse per vincere":
			#algoritmo numero mosse per vincere
			migliore_esito = self.algoritmo_mosse_per_vincere(profondita=profondita, id_giocatore=giocatore_attuale)
		elif self.algoritmo_ricerca == "Algoritmo punteggio medio":
			migliore_esito = self.algoritmo_media_punteggio(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=False)
		elif self.algoritmo_ricerca == "Algoritmo min/max * profondità":
			migliore_esito = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=True)
		elif self.algoritmo_ricerca == "Algoritmo punteggio medio *  profondità":
			migliore_esito = self.algoritmo_media_punteggio(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=True)
		elif self.algoritmo_ricerca == "Algoritmo alfa beta pruning":
			migliore_esito = self.algoritmo_alpha_beta_pruning_multigiocatori(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=False)
		elif self.algoritmo_ricerca == "Algoritmo alfa beta pruning * profondità":
			migliore_esito = self.algoritmo_alpha_beta_pruning_multigiocatori(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=True)
		elif self.algoritmo_ricerca == "Algoritmo min/max con parallelizzazione":
			#parallel
			#prima di iniziare mi stampo il numero di thread
			print(f'Thread prima di iniziare: {threading.active_count()}')
			self.lista_thread_totali.clear()
			lista_dove_salvare : List[EsitoAlgoritmoRicerca] = []
			migliore_esito = self.algoritmo_min_max_multigiocatori_parallelo(profondita=profondita, 
													id_giocatore=giocatore_attuale, 
													flag_moltiplica_profondita=False,
													lista_case_basi= [i.casa_base for i in self.lista_giocatori],
													lista_numeri_pedine_aggiunte=[i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori],
													massimo_numero_trhread=numero_massimo_thread,
													punteggio_da_aggiugnere=0,
													schema_gioco=[riga[:] for riga in self.schema_gioco],
													lista_dove_salvare_esito=lista_dove_salvare,
													lista_mosse_da_aggiungere=[],
													flag_primo=True)
			migliore_esito =lista_dove_salvare.pop()
			print(f'parallelo: {migliore_esito.lista_mosse}, {migliore_esito.punteggio}')
			#provo a calcolare anche con quello normale, i risultati dovrebbero essere simili
			#migliore_esito_normale = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=False)
			#print(f'normale: {migliore_esito_normale.lista_mosse}, {migliore_esito_normale.punteggio}')
		elif self.algoritmo_ricerca == "Algoritmo min/max con parallelizzazione * profondità":
			#parallel
			#prima di iniziare mi stampo il numero di thread
			print(f'Thread prima di iniziare: {threading.active_count()}')
			self.lista_thread_totali.clear()
			lista_dove_salvare : List[EsitoAlgoritmoRicerca] = []
			migliore_esito = self.algoritmo_min_max_multigiocatori_parallelo(profondita=profondita, 
													id_giocatore=giocatore_attuale, 
													flag_moltiplica_profondita=True,
													lista_case_basi= [i.casa_base for i in self.lista_giocatori],
													lista_numeri_pedine_aggiunte=[i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori],
													massimo_numero_trhread=numero_massimo_thread,
													punteggio_da_aggiugnere=0,
													schema_gioco=[riga[:] for riga in self.schema_gioco],
													lista_dove_salvare_esito=lista_dove_salvare,
													lista_mosse_da_aggiungere=[],
													flag_primo=True)
			migliore_esito =lista_dove_salvare.pop()
			print(f'parallelo: {migliore_esito.lista_mosse}, {migliore_esito.punteggio}')
		elif self.algoritmo_ricerca == "Algoritmo mossa imemdiata miglior punteggio":
			migliore_esito = self.algoritmo_mossa_immediata_miglior_punteggio(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=False)
		elif self.algoritmo_ricerca == "Algoritmo mossa imemdiata miglior punteggio * profondità":
			migliore_esito = self.algoritmo_mossa_immediata_miglior_punteggio(profondita=profondita, id_giocatore=giocatore_attuale, flag_moltiplica_profondita=True)
		else:
			raise Exception('Algoritmo non identificato')
		#ripristino lo schema
		#ripristino lo schema e i numeri
		self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
		#ritorno il punteggio calcolato
		return migliore_esito

	#######################################################################################################################################################################################
	#qui inserisco i vari algoritmi
	'''
	def algoritmo_min_max(self, profondita:int, id_giocatore:int) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare il miglior punteggio di quel giocatore
		#mi creo l'oggetto da ritornare in seguito
		oggetto_esito = EsitoAlgoritmoRicerca()
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			#valorizzo il punteggio
			oggetto_esito.punteggio = 0
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#allora vado a ricosrsione e ritorno lo stesso risultato aumentato della cifra della vincita della partita
			punteggio_raggiunto = -self.valore_vincita_partita*profondita 
			esito_ricorsione = self.algoritmo_min_max(profondita=profondita -1, id_giocatore=giocatore_successivo)
			punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			oggetto_esito.punteggio = punteggio_raggiunto
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore)*profondita
			esito_ricorsione = self.algoritmo_min_max(profondita=profondita-1, id_giocatore=giocatore_successivo)
			punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.punteggio = punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta():
			punteggio_raggiunto = self.valore_vincita_partita*profondita 
			esito_ricorsione = self.algoritmo_min_max(profondita=profondita-1, id_giocatore=giocatore_successivo) #il meno ci va pèerche e un punteggio riferito all'altro giocatore
			punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.punteggio = punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#mi converto la lista di mosse in lista di liste
		lista_liste_mosse = [ list(i) for i in lista_mosse_disponibili ]
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		lista_esiti:List[EsitoAlgoritmoRicerca] = []
		while len(lista_liste_mosse) >0:
			#mi prendo il primo
			lista_mosse_in_analisi = lista_liste_mosse.pop(0)
			#intanto eseguo la lista di mosse
			punteggio_attuale = self.esegui_successione_mosse(lista_mosse=lista_mosse_in_analisi)*profondita
			#adesso mi ricavo l'ultima mossa per vedere se ha mangiato e valutare se analizzarla o aspettare
			((r_pen, c_pen), (r_ult, c_ult)) = lista_mosse_in_analisi[-2:]
			flag_analizza_mossa = True
			#io non devo analizzarla solo se ho l'obbligo di mangiare, e una mossa di mangio e ho altre celle successive
			if ( abs(r_ult - r_pen) + abs(c_ult - c_pen) )>1:
				#ho mangiato, calcolo celle successive
				celle_successive_mangio = giocatore_in_turno.calcola_lista_successive_celle_per_mangiare_singola_casella(tabella_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, numero_giocatori=len(self.lista_giocatori), r=r_ult, c=c_ult)
				if len(celle_successive_mangio) >0:
					if self.flag_obbligatorieta_mangiare:
						#allora non devo analizzare la mossa perche fa parte di un percorso per mangiare obbligatorio
						flag_analizza_mossa = False
					#vado ad aggiugnere alla lista di delle mosse tutte le successive mosse
					lista_liste_mosse.extend( [ lista_mosse_in_analisi + [i] for i in celle_successive_mangio ] )
			#adesso verifico se devo analizzare la mossa
			if flag_analizza_mossa:
				#al punteggio attuale ci sommo anche il punteggio della generatrice piu vicina
				punteggio_attuale += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore)*profondita
				#mi calcolo il punteggio a ricorsione
				esito_ricorsione = self.algoritmo_min_max(profondita=profondita-1, id_giocatore=giocatore_successivo)
				punteggio_attuale -= esito_ricorsione.punteggio
				#mi creo un nuovo esito
				nuovo_esito = EsitoAlgoritmoRicerca()
				nuovo_esito.punteggio = punteggio_attuale
				nuovo_esito.lista_mosse= lista_mosse_in_analisi
				lista_esiti.append(nuovo_esito)
			#al termine del while ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		massimo_punteggio = max( [ i.punteggio for i in lista_esiti ] )
		#mi calcolo gli esiti con punteggio massimo
		esiti_punteggio_massimo = [ i for i in lista_esiti if i.punteggio==massimo_punteggio ]
		#ne ritorno uno a caso di questi
		return random.choice(esiti_punteggio_massimo)
	'''



	def algoritmo_valuta_schema_profondita(self, profondita:int, id_giocatore:int) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare il miglior punteggio di quel giocatore
		#mi creo l'oggetto da ritornare in seguito
		oggetto_esito = EsitoAlgoritmoRicerca()
		#caso base sono arrivato al limite della profondita
		if profondita <= 1:
			#valorizzo lo schema
			oggetto_esito.matrice_finale = [riga[:] for riga in self.schema_gioco]
			oggetto_esito.profondita_matrice_finale = 1
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		case_basi_iniziali = [ i.casa_base for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#vado a ricorsione e ritorno lo schema che viene ritornato dalla ricorsione
			#se il giocatore e gia stato eliminato la profondita della ricorsione puo essere la stessa dell'attuale
			esito_ricorsione = self.algoritmo_valuta_schema_profondita(profondita=profondita, id_giocatore=giocatore_successivo)
			oggetto_esito.matrice_finale = esito_ricorsione.matrice_finale
			oggetto_esito.profondita_matrice_finale = esito_ricorsione.profondita_matrice_finale
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, 
																				flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, 
																				flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, 
																				numero_giocatori=len(self.lista_giocatori),
																				flag_damone_mossa_diagonale = self.flag_damone_mossa_diagonale,
																				valore_casella_damone = self.valore_pedina_damone,
																				flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
																				flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni
																						   )
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			esito_ricorsione = self.algoritmo_valuta_schema_profondita(profondita=profondita-1, id_giocatore=giocatore_successivo)
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.matrice_finale = esito_ricorsione.matrice_finale
			oggetto_esito.profondita_matrice_finale = esito_ricorsione.profondita_matrice_finale
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo o altri giocatori, comunque situazione di stallo giocatore ritorno la matrice attuale
		if self.verifica_partita_vinta(): 
			#non vado a ricorsione perche situazione di stallo e ritorno la matrice attuale
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.matrice_finale = [riga[:] for riga in self.schema_gioco]
			oggetto_esito.profondita_matrice_finale = profondita
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#mi converto la lista di mosse in lista di liste
		lista_liste_mosse = [ list(i) for i in lista_mosse_disponibili ]
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		lista_esiti:List[EsitoAlgoritmoRicerca] = []
		while len(lista_liste_mosse) >0:
			#mi prendo il primo
			lista_mosse_in_analisi = lista_liste_mosse.pop(0)
			#intanto eseguo la lista di mosse
			punteggio_attuale = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=lista_mosse_in_analisi, id_giocatore=id_giocatore)*profondita
			#adesso mi ricavo l'ultima mossa per vedere se ha mangiato e valutare se analizzarla o aspettare
			((r_pen, c_pen), (r_ult, c_ult)) = lista_mosse_in_analisi[-2:]
			flag_analizza_mossa = True
			#io non devo analizzarla solo se ho l'obbligo di mangiare, e una mossa di mangio e ho altre celle successive
			if ( abs(r_ult - r_pen) == 2) or (abs(c_ult - c_pen) == 2 ):
				#ho mangiato, calcolo celle successive
				celle_successive_mangio = giocatore_in_turno.calcola_lista_successive_celle_per_mangiare_singola_casella(tabella_gioco=self.schema_gioco, 
																				flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, 
																				numero_giocatori=len(self.lista_giocatori), 
																				r=r_ult, 
																				c=c_ult,
																				flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
																				valore_damone=self.valore_pedina_damone,
																				flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni)
				if len(celle_successive_mangio) >0:
					if self.flag_obbligatorieta_mangiare:
						#allora non devo analizzare la mossa perche fa parte di un percorso per mangiare obbligatorio
						flag_analizza_mossa = False
					#vado ad aggiugnere alla lista di delle mosse tutte le successive mosse
					lista_liste_mosse.extend( [ lista_mosse_in_analisi + [i] for i in celle_successive_mangio ] )
			#adesso verifico se devo analizzare la mossa
			if flag_analizza_mossa:
				#al punteggio attuale ci sommo anche il punteggio della generatrice piu vicina
				#punteggio_attuale += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore)*profondita
				#mi calcolo il punteggio a ricorsione
				esito_ricorsione = self.algoritmo_valuta_schema_profondita(profondita=profondita-1, id_giocatore=giocatore_successivo)
				#adesso devo calcolare il punteggio relativo alla ricorsione, ovvero partendo dallo schema di partenza e da quello di arrivo per il numero di giocatore calcolo la differenza delle pedine
				punteggio_ricorsione = self.calcola_differenza_valori_schemi_per_giocatore(schema_finale=esito_ricorsione.matrice_finale, id_giocatore_analisi=id_giocatore)
				punteggio_attuale += sum( [punteggio_ricorsione*i for i in range(1, esito_ricorsione.profondita_matrice_finale+1)] )
				#mi creo un nuovo esito
				nuovo_esito = EsitoAlgoritmoRicerca()
				nuovo_esito.punteggio = punteggio_attuale
				nuovo_esito.lista_mosse= lista_mosse_in_analisi
				nuovo_esito.matrice_finale = esito_ricorsione.matrice_finale
				nuovo_esito.profondita_matrice_finale = esito_ricorsione.profondita_matrice_finale
				lista_esiti.append(nuovo_esito)
			#al termine del while ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		massimo_punteggio = max( [ i.punteggio for i in lista_esiti ] )
		#mi calcolo gli esiti con punteggio massimo
		esiti_punteggio_massimo = [ i for i in lista_esiti if i.punteggio==massimo_punteggio ]
		#ne ritorno uno a caso di questi
		return random.choice(esiti_punteggio_massimo)

	'''
	INGLOBATO NELL'ALGORITMO MIN/MAX CON POSSILITÀ DI MOLTIPLICARE PER LA PROFONDITA
	#algoritmo min max per piu giocatori con le liste
	def algoritmo_min_max_multigiocatori(self, profondita:int, id_giocatore:int) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare il miglior punteggio di quel giocatore
		#mi creo l'oggetto da ritornare in seguito
		numero_giocatori = len(self.lista_giocatori)
		oggetto_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			#valorizzo il punteggio
			#oggetto_esito.punteggio = 0
			oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#allora vado a ricosrsione e ritorno lo stesso risultato aumentato della cifra della vincita della partita
			punteggio_raggiunto = -self.valore_vincita_partita*profondita 
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_successivo)
			#punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			#oggetto_esito.punteggio = punteggio_raggiunto
			#oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore)*profondita
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo)
			#punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			#oggetto_esito.punteggio = punteggio_raggiunto
			#oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta():
			punteggio_raggiunto = self.valore_vincita_partita*profondita 
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo) #il meno ci va pèerche e un punteggio riferito all'altro giocatore
			#punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			#oggetto_esito.punteggio = punteggio_raggiunto
			#oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#mi converto la lista di mosse in lista di liste
		lista_liste_mosse = [ list(i) for i in lista_mosse_disponibili ]
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		lista_esiti:List[EsitoAlgoritmoRicerca] = []
		while len(lista_liste_mosse) >0:
			#mi prendo il primo
			lista_mosse_in_analisi = lista_liste_mosse.pop(0)
			#intanto eseguo la lista di mosse
			punteggio_attuale = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=lista_mosse_in_analisi, id_giocatore=id_giocatore)*profondita
			#adesso mi ricavo l'ultima mossa per vedere se ha mangiato e valutare se analizzarla o aspettare
			((r_pen, c_pen), (r_ult, c_ult)) = lista_mosse_in_analisi[-2:]
			flag_analizza_mossa = True
			#io non devo analizzarla solo se ho l'obbligo di mangiare, e una mossa di mangio e ho altre celle successive
			if ( abs(r_ult - r_pen) + abs(c_ult - c_pen) )>1:
				#ho mangiato, calcolo celle successive
				celle_successive_mangio = giocatore_in_turno.calcola_lista_successive_celle_per_mangiare_singola_casella(tabella_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, numero_giocatori=len(self.lista_giocatori), r=r_ult, c=c_ult)
				if len(celle_successive_mangio) >0:
					if self.flag_obbligatorieta_mangiare:
						#allora non devo analizzare la mossa perche fa parte di un percorso per mangiare obbligatorio
						flag_analizza_mossa = False
					#vado ad aggiugnere alla lista di delle mosse tutte le successive mosse
					lista_liste_mosse.extend( [ lista_mosse_in_analisi + [i] for i in celle_successive_mangio ] )
			#adesso verifico se devo analizzare la mossa
			if flag_analizza_mossa:
				#al punteggio attuale ci sommo anche il punteggio della generatrice piu vicina
				#punteggio_attuale += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore)*profondita
				#mi calcolo il punteggio a ricorsione
				esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo)
				#punteggio_attuale -= esito_ricorsione.punteggio
				#mi creo un nuovo esito
				nuovo_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
				#nuovo_esito.punteggio = punteggio_attuale
				nuovo_esito.lista_mosse= lista_mosse_in_analisi
				nuovo_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
				nuovo_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_attuale
				nuovo_esito.calcola_punteggio(id_giocatore=id_giocatore)
				lista_esiti.append(nuovo_esito)
			#al termine del while ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		massimo_punteggio = max( [ i.punteggio for i in lista_esiti ] )
		#per test per vedere se lacolano le stesse mosse provo a calcolarmi la lista delle mosse con gli esitiRicercaListaMosse
		#sembra che il numero sia lo stesso
		#lista_mosse_calcolata_esiti = EsitoRicercaListaMosse.EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco, giocatore_in_questione=giocatore_in_turno, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		#print(f'\nMosse attuali: {len(lista_esiti)}')
		#print(f'Mosse nuove  : {len(lista_mosse_calcolata_esiti)}')
		#############################################################
		#mi calcolo gli esiti con punteggio massimo
		esiti_punteggio_massimo = [ i for i in lista_esiti if i.punteggio==massimo_punteggio ]
		#ne ritorno uno a caso di questi
		return random.choice(esiti_punteggio_massimo)
	'''

	def calcola_numero_mosse_per_vincere(self, giocatore_in_analisi:Giocatore, massima_profondita:int) -> float:
		#metodo per calcolare con esplorazione in ampiezza il numero di mosse che permetterebbe di vimcere, se non risulta vincente nella profondita stabilita ritorna la profondita, se vede che entro n mosse rischia di perdere ritorna + infinito
		n_giocatori = len(self.lista_giocatori)
		#per prima cosa verifico se ha gia vinto o perso (in teroia basta solo che ha vinto perche e lui che ha eseguito l'ultima mossa)
		if giocatore_in_analisi.verifica_giocatore_vincente(tabella_gioco=self.schema_gioco, numero_giocatori=n_giocatori):
			return 0
		elif giocatore_in_analisi.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=n_giocatori):
			return math.inf
		#altrimenti posso provare ad analizzare il giocatore successivo
		#mi calcolo il giocatore successivo
		giocatore_successivo = self.lista_giocatori[ (giocatore_in_analisi.id_giocatore+1)%n_giocatori ]
		#mi creo le liste di analisi
		lista_analisi_attuale : List[EsitoRicercaListaMosse.EsitoRicercaListaMosse] = []
		lista_analisi_successiva : List[ EsitoRicercaListaMosse.EsitoRicercaListaMosse ] = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(
												matrice_inziale=self.schema_gioco,
												flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
												flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
												giocatore_in_questione=giocatore_successivo,
												numero_giocatori=n_giocatori,
												flag_salva_lista_mosse=False,
												flag_damone_mossa_diagonale = self.flag_damone_mossa_diagonale,
											 	valore_casella_damone = self.valore_pedina_damone,
												flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
												flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni) #non mi serve la lista dello storico delle mosse per risparmiare memoria perche gia di suo sara pesante come memoria
		for profondita_attuale in range(1, massima_profondita+1):
			#cambio le liste
			lista_analisi_attuale = lista_analisi_successiva[:]
			lista_analisi_successiva.clear()
			#mi calcolo il giocatore successivo
			giocatore_successivo = self.lista_giocatori[ (giocatore_successivo.id_giocatore+1)%n_giocatori ]
			#while len(lista_analisi_attuale) >0:
			for esito_lista_mosse in lista_analisi_attuale:
				#forse posso provare con un for per migliorare performance
				#esito_lista_mosse = lista_analisi_attuale.pop()
				#vado a verificare se il giocatore in analisi ha vinto o no
				if giocatore_in_analisi.verifica_giocatore_vincente(tabella_gioco=esito_lista_mosse.matrice_finale, numero_giocatori=n_giocatori):
					return profondita_attuale
				elif giocatore_in_analisi.verifica_giocatore_eliminato(tabella_gioco=esito_lista_mosse.matrice_finale, numero_giocatori=n_giocatori):
					return math.inf
				#se non ha vinto provo ad eseguire le mosse del giocatore successivo ed ad aggiungerle 
				lista_configurazioni_da_aggiungere = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=esito_lista_mosse.matrice_finale,
																				giocatore_in_questione=giocatore_successivo,
																				flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																				flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																				flag_salva_lista_mosse=False,
																				numero_giocatori=n_giocatori,
																				flag_damone_mossa_diagonale = self.flag_damone_mossa_diagonale,
																					valore_casella_damone = self.valore_pedina_damone,
																				flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
																				flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni)
				#se la lista delle possibili configurazioni e vuota significa che devo aggiungere la configurazione stessa con togliendo tutti gli elementi del giocatore in questione
				if len(lista_configurazioni_da_aggiungere) == 0:
					nuovo_esito = EsitoRicercaListaMosse.EsitoRicercaListaMosse(matrice_finale=esito_lista_mosse.elimina_giocatore_schema(
										id_giocatore=giocatore_successivo.id_giocatore,
										numero_giocatori=n_giocatori
									), 
									flag_salva_mosse=False,
									lista_mosse=[]
					)
					lista_analisi_successiva.append(nuovo_esito)
				else:
					#ho trovato delle mosse per il giocatore
					lista_analisi_successiva.extend(lista_configurazioni_da_aggiungere)
		#al termine del for se non ha trovato niente significa che devo ritornare la massima profondita raggiunta aumentata di 1
		return massima_profondita +1

	def algoritmo_mosse_per_vincere(self, profondita:int, id_giocatore:int) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare la migliore mossa utilizzando l'algoritmo che va a ricercare entro quante mosse si può vincere o perdere
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		case_basi_iniziali = [ i.casa_base for i in self.lista_giocatori ]
		#suppongo che il giocatore non sia gia stato eliminato
		#mi calcolo il giocatore in turno
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		n_giocatori = len(self.lista_giocatori)
		#mi calcolo tutti le mosse che potrebbe fare il giocatore
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco,
																				giocatore_in_questione=giocatore_in_turno,
																				flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																				flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																				flag_salva_lista_mosse=True,
																				numero_giocatori=n_giocatori,
																				flag_damone_mossa_diagonale = self.flag_damone_mossa_diagonale,
																				valore_casella_damone = self.valore_pedina_damone,
																				flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
																				flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni)
		lista_esiti : List[EsitoAlgoritmoRicerca] = []
		#adesso provo ad eseguire tutte le mosse e calcolarmi i nuovi esiti
		for esito_lista_mosse in lista_mosse_disponibili:
			nuovo_esito = EsitoAlgoritmoRicerca(numero_giocatori=n_giocatori)
			#eseguo la lista delle mosse calcolate
			punteggio_lista_mnosse = self.esegui_successione_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate)
			nuovo_esito.punteggio = punteggio_lista_mnosse
			nuovo_esito.lista_mosse = esito_lista_mosse.lista_mosse_calcolate[:]
			#adesso vado a calcolare quante mosse sarebbero necessarie per vincere
			nuovo_esito.numero_mosse_per_vincere = self.calcola_numero_mosse_per_vincere(giocatore_in_analisi=giocatore_in_turno, massima_profondita=profondita)
			#mi vado ad aggiugnere l'esito alla lista
			lista_esiti.append(nuovo_esito)
			#mi ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
		#mi calcolo il minimo tra numero mosse e punteggio
		min_mosse = min( [ i.numero_mosse_per_vincere for i in lista_esiti ] )
		#mi calcolo la lisat degli esiti minimi
		lista_esiti_minimo_mosse = [ i for i in lista_esiti if (i.numero_mosse_per_vincere==min_mosse) ]
		#se ha lunghezza >1 ritorno quella con il massimo punteggio
		if len(lista_esiti_minimo_mosse) == 0:
			return lista_esiti_minimo_mosse[0]
		#altrimenti ha piu di un elemento
		massimo_punteggio = max( [i.punteggio for i in lista_esiti_minimo_mosse] )
		lista_esiti_massimo_punteggio = [ i for i in lista_esiti_minimo_mosse if (i.punteggio==massimo_punteggio) ]
		esito_scelto = random.choice(lista_esiti_massimo_punteggio)
		return esito_scelto

	def algoritmo_media_punteggio(self, profondita:int, id_giocatore:int, flag_moltiplica_profondita:bool) -> EsitoAlgoritmoRicerca:
		#algoritmo che ritorna la mossa che ha un punteggio medio maggiore
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		case_basi_iniziali = [ i.casa_base for i in self.lista_giocatori ]
		#suppongo che il giocatore non sia gia stato eliminato
		#mi calcolo il giocatore in turno
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		n_giocatori = len(self.lista_giocatori)
		#mi calcolo tutti le mosse che potrebbe fare il giocatore
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco,
																				giocatore_in_questione=giocatore_in_turno,
																				flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																				flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																				flag_salva_lista_mosse=True,
																				numero_giocatori=n_giocatori,
																				flag_damone_mossa_diagonale = self.flag_damone_mossa_diagonale,
																				valore_casella_damone = self.valore_pedina_damone,
																				flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
																				flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni)
		if len(lista_mosse_disponibili) == 1:
			#mi faccio un nuovo esito e ritorno quello
			esito_ritorno = EsitoAlgoritmoRicerca(numero_giocatori=0)
			esito_ritorno.lista_mosse = lista_mosse_disponibili[0].lista_mosse_calcolate
			return esito_ritorno
		lista_esiti : List[EsitoAlgoritmoRicerca] = []
		#adesso provo ad eseguire tutte le mosse e calcolarmi i nuovi esiti
		for esito_lista_mosse in lista_mosse_disponibili:
			#eseguo la lista delle mosse calcolate
			punteggio_lista_mosse = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate, id_giocatore=id_giocatore)
			#mi devo calcolare anche il ppunteggio di distanza dalla casa base
			#punteggio_lista_mosse += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore)
			#vado a calcolarmi l'esito della somma dei punteggi dei percorsi
			esito_somma_punteggi_percorsi = self.calcola_punteggi_percorsi(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ci vado a sommare il punteggio di questa mossa
			esito_somma_punteggi_percorsi.lista_punteggi_giocatori[id_giocatore] += punteggio_lista_mosse*profondita*esito_somma_punteggi_percorsi.numero_percorsi
			#vado a popolarmi la lista delle mosse di questo esito
			esito_somma_punteggi_percorsi.lista_mosse = esito_lista_mosse.lista_mosse_calcolate[:]
			#vado a calcolare il punteggio di questo esito che poi ancdor a dividere per il numero di percorsi trovati
			esito_somma_punteggi_percorsi.calcola_punteggio(id_giocatore=id_giocatore)
			esito_somma_punteggi_percorsi.punteggio = esito_somma_punteggi_percorsi.punteggio/esito_somma_punteggi_percorsi.numero_percorsi
			#mi vado ad aggiugnere l'esito alla lista
			lista_esiti.append(esito_somma_punteggi_percorsi)
			#mi ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
		#mi calcolo il massimo punteggio
		massimo_punteggio = max( [i.punteggio for i in lista_esiti] )
		lista_esiti_massimo_punteggio = [ i for i in lista_esiti if (i.punteggio==massimo_punteggio) ]
		esito_scelto = random.choice(lista_esiti_massimo_punteggio)
		#prima di ritornarlo voglio stamparmeli tutti
		print('esiti con somma punteggio')
		for i in sorted(lista_esiti, key= lambda x: x.punteggio):
			print(f'mosse: {i.lista_mosse}, punteggio: {i.punteggio}, numero_percorsi: {i.numero_percorsi}, lista_punteggi: {i.lista_punteggi_giocatori}')
		return esito_scelto

	def calcola_punteggi_percorsi(self, profondita:int, id_giocatore:int, flag_moltiplica_profondita:bool) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare la somma dei punteggi dei percorsi generati 
		#mi creo l'oggetto da ritornare in seguito
		numero_giocatori = len(self.lista_giocatori)
		oggetto_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			oggetto_esito.numero_percorsi = 1
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		case_basi_iniziali = [ i.casa_base for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#vado a ricorsione e mi calcolo i percorsi a ricorsione
			punteggio_raggiunto = -self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.calcola_punteggi_percorsi(profondita=profondita, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += (punteggio_raggiunto*esito_ricorsione.numero_percorsi) #metto la mostriplicazione perche se l'esito successivo si bifornca in n percorsi, il punteggio raggiunto da questo livello va moltiplicato per il numero di percorsi perche potrei avere n percorsi differenti
			oggetto_esito.numero_percorsi = esito_ricorsione.numero_percorsi #il numero di percorsi e lo stesso calcolato al livello successivo
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		#lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco,
																				giocatore_in_questione=giocatore_in_turno,
																				flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																				flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																				flag_salva_lista_mosse=True,
																				numero_giocatori=numero_giocatori,
																				flag_damone_mossa_diagonale = self.flag_damone_mossa_diagonale,
																				valore_casella_damone = self.valore_pedina_damone,
																				flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
																				flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni)
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.calcola_punteggi_percorsi(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += (punteggio_raggiunto*esito_ricorsione.numero_percorsi) #metto la mostriplicazione perche se l'esito successivo si bifornca in n percorsi, il punteggio raggiunto da questo livello va moltiplicato per il numero di percorsi perche potrei avere n percorsi differenti
			oggetto_esito.numero_percorsi = esito_ricorsione.numero_percorsi #il numero di percorsi e lo stesso calcolato al livello successivo
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta():
			punteggio_raggiunto = self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.calcola_punteggi_percorsi(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita) #il meno ci va pèerche e un punteggio riferito all'altro giocatore
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += (punteggio_raggiunto*esito_ricorsione.numero_percorsi) #metto la mostriplicazione perche se l'esito successivo si bifornca in n percorsi, il punteggio raggiunto da questo livello va moltiplicato per il numero di percorsi perche potrei avere n percorsi differenti
			oggetto_esito.numero_percorsi = esito_ricorsione.numero_percorsi #il numero di percorsi e lo stesso calcolato al livello successivo
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#mi converto la lista di mosse in lista di liste
		#lista_liste_mosse = [ list(i) for i in lista_mosse_disponibili ]
		esito_da_ritornare = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		esito_da_ritornare.numero_percorsi = 0
		'''
		while len(lista_liste_mosse) >0:
			#mi prendo il primo
			lista_mosse_in_analisi = lista_liste_mosse.pop(0)
			#intanto eseguo la lista di mosse
			punteggio_attuale = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=lista_mosse_in_analisi, id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			#adesso mi ricavo l'ultima mossa per vedere se ha mangiato e valutare se analizzarla o aspettare
			((r_pen, c_pen), (r_ult, c_ult)) = lista_mosse_in_analisi[-2:]
			flag_analizza_mossa = True
			#io non devo analizzarla solo se ho l'obbligo di mangiare, e una mossa di mangio e ho altre celle successive
			if ( abs(r_ult - r_pen) + abs(c_ult - c_pen) )>1:
				#ho mangiato, calcolo celle successive
				celle_successive_mangio = giocatore_in_turno.calcola_lista_successive_celle_per_mangiare_singola_casella(tabella_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, numero_giocatori=len(self.lista_giocatori), r=r_ult, c=c_ult)
				if len(celle_successive_mangio) >0:
					if self.flag_obbligatorieta_mangiare:
						#allora non devo analizzare la mossa perche fa parte di un percorso per mangiare obbligatorio
						flag_analizza_mossa = False
					#vado ad aggiugnere alla lista di delle mosse tutte le successive mosse
					lista_liste_mosse.extend( [ lista_mosse_in_analisi + [i] for i in celle_successive_mangio ] )
			#adesso verifico se devo analizzare la mossa
			if flag_analizza_mossa:
				#al punteggio attuale ci sommo anche il punteggio della generatrice piu vicina
				#punteggio_attuale += self.calcola_punteggio_generatrice_piu_vicina(id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
				#mi calcolo il punteggio a ricorsione
				esito_ricorsione = self.calcola_punteggi_percorsi(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
				#vado a sommare i valori trovati nell'esito da ritornare
				for id_gioc in range(numero_giocatori):
					esito_da_ritornare.lista_punteggi_giocatori[id_gioc] += esito_ricorsione.lista_punteggi_giocatori[id_gioc]
				#ci vado a sommare anche il numero di percorsi
				esito_da_ritornare.numero_percorsi += esito_ricorsione.numero_percorsi
				#ci vado a sommare il punteggio per il giocatore attuale
				esito_da_ritornare.lista_punteggi_giocatori[id_giocatore] += (punteggio_attuale*esito_ricorsione.numero_percorsi)
			#al termine del while ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori)
		'''
		for esito_lista_mosse in lista_mosse_disponibili:
			#mi calcolo il punteggio
			punteggio_attuale = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate, id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			#mi calcolo il punteggio a ricorsione
			esito_ricorsione = self.calcola_punteggi_percorsi(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#vado a sommare i valori trovati nell'esito da ritornare
			for id_gioc in range(numero_giocatori):
				esito_da_ritornare.lista_punteggi_giocatori[id_gioc] += esito_ricorsione.lista_punteggi_giocatori[id_gioc]
			#ci vado a sommare anche il numero di percorsi
			esito_da_ritornare.numero_percorsi += esito_ricorsione.numero_percorsi
			#ci vado a sommare il punteggio per il giocatore attuale
			esito_da_ritornare.lista_punteggi_giocatori[id_giocatore] += (punteggio_attuale*esito_ricorsione.numero_percorsi)
			#al termine del for ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
		#se arrivo qui significa che ho popolato l'esito da ritornare con almeno una mossa
		return esito_da_ritornare

	#algoritmo min max per piu giocatori con le liste disinteressandosi della profodnita
	def algoritmo_min_max_multigiocatori(self, profondita:int, id_giocatore:int, flag_moltiplica_profondita:bool) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare il miglior punteggio di quel giocatore
		#mi creo l'oggetto da ritornare in seguito
		numero_giocatori = len(self.lista_giocatori)
		oggetto_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			#valorizzo il punteggio
			#oggetto_esito.punteggio = 0
			#oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		case_basi_iniziali = [ i.casa_base for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#allora vado a ricosrsione e ritorno lo stesso risultato aumentato della cifra della vincita della partita
			punteggio_raggiunto = -self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		#lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco,
																				giocatore_in_questione=giocatore_in_turno,
																				flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																				flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																				flag_salva_lista_mosse=True,
																				numero_giocatori=numero_giocatori,
																				flag_damone_mossa_diagonale = self.flag_damone_mossa_diagonale,
																				valore_casella_damone = self.valore_pedina_damone,
																				flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
																				flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni)
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta():
			punteggio_raggiunto = self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita) #il meno ci va pèerche e un punteggio riferito all'altro giocatore
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		lista_esiti:List[EsitoAlgoritmoRicerca] = []
		for esito_lista_mosse in lista_mosse_disponibili:
			#eseguo la lista delle mosse calcolate
			punteggio_lista_mosse = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate, id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			#mi calcolo il punteggio a ricorsione
			esito_ricorsione = self.algoritmo_min_max_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#punteggio_attuale -= esito_ricorsione.punteggio
			#mi creo un nuovo esito
			nuovo_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
			#nuovo_esito.punteggio = punteggio_attuale
			nuovo_esito.lista_mosse= esito_lista_mosse.lista_mosse_calcolate
			nuovo_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			nuovo_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_lista_mosse
			nuovo_esito.calcola_punteggio(id_giocatore=id_giocatore)
			lista_esiti.append(nuovo_esito)
			#al termine del for ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		if len(lista_esiti) == 1:
			#se ha solo 1 elemento lo ritorno
			return lista_esiti[0]
		massimo_punteggio = max( [ i.punteggio for i in lista_esiti ] )
		#mi calcolo gli esiti con punteggio massimo
		esiti_punteggio_massimo = [ i for i in lista_esiti if i.punteggio==massimo_punteggio ]
		#mi stampo tutta la lista
		#print('lista punteggi')
		#for esito in sorted(lista_esiti, key=lambda x: x.punteggio):
		#	print(esito.lista_mosse, esito.punteggio)
		#ne ritorno uno a caso di questi
		return random.choice(esiti_punteggio_massimo)

	#algoritmo alfa beta pruning per piu giocatori con le liste disinteressandosi della profodnita
	def algoritmo_alpha_beta_pruning_multigiocatori(self, profondita:int, id_giocatore:int, flag_moltiplica_profondita:bool) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare il miglior punteggio di quel giocatore
		#mi creo l'oggetto da ritornare in seguito
		numero_giocatori = len(self.lista_giocatori)
		oggetto_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			#valorizzo il punteggio
			#oggetto_esito.punteggio = 0
			#oggetto_esito.calcola_punteggio(id_giocatore=id_giocatore)
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		case_basi_iniziali = [ i.casa_base for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % len(self.lista_giocatori)
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=len(self.lista_giocatori)):
			#allora vado a ricosrsione e ritorno lo stesso risultato aumentato della cifra della vincita della partita
			punteggio_raggiunto = -self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_alpha_beta_pruning_multigiocatori(profondita=profondita, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#punteggio_raggiunto -= esito_ricorsione.punteggio
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		#lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco,
																				giocatore_in_questione=giocatore_in_turno,
																				flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																				flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																				flag_salva_lista_mosse=True,
																				numero_giocatori=numero_giocatori,
																				flag_damone_mossa_diagonale = self.flag_damone_mossa_diagonale,
																				valore_casella_damone = self.valore_pedina_damone,
																				flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
																				flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni)
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_alpha_beta_pruning_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta():
			punteggio_raggiunto = self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_alpha_beta_pruning_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita) #il meno ci va pèerche e un punteggio riferito all'altro giocatore
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		#lista_esiti:List[EsitoAlgoritmoRicerca] = []
		lista_esiti_migliori : List[EsitoAlgoritmoRicerca] = []
		massimo_punteggio_attuale = -math.inf
		for esito_lista_mosse in lista_mosse_disponibili:
			#eseguo la lista delle mosse calcolate
			punteggio_lista_mosse = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate, id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			#valuto se mi conviene fare quella strada oppre per alfa beta pruning
			punteggio_da_aggiungere = 0
			for nuova_profondita in range(profondita):
				punteggio_da_aggiungere += self.valore_vincita_partita * ( nuova_profondita if flag_moltiplica_profondita else 1 )
			#verifico se posso aver eliminato qualche giocatore
			profondita_eliminazione_giocatore = 1
			for giocatore_prova_alfa_beta in ( self.lista_giocatori[id_giocatore+1:] + self.lista_giocatori[:id_giocatore] ):
				#se il giocatore non e eliminato posso averlo eliminato in questo turno
				if not(giocatore_prova_alfa_beta.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=numero_giocatori)):
					punteggio_da_aggiungere += self.valore_eliminazione_giocatore * ( profondita_eliminazione_giocatore if flag_moltiplica_profondita else 1 )
				profondita_eliminazione_giocatore += 1
			if (punteggio_lista_mosse + punteggio_da_aggiungere) < massimo_punteggio_attuale:
				#dico che ho eliminato una strada 
				print(f'Eliminata chiamata a profondita {profondita-1}')
			else:
				#devo eseguire la chiamata
				#mi calcolo il punteggio a ricorsione
				esito_ricorsione = self.algoritmo_alpha_beta_pruning_multigiocatori(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
				#punteggio_attuale -= esito_ricorsione.punteggio
				#mi creo un nuovo esito
				nuovo_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
				#nuovo_esito.punteggio = punteggio_attuale
				nuovo_esito.lista_mosse= esito_lista_mosse.lista_mosse_calcolate
				nuovo_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
				nuovo_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_lista_mosse
				nuovo_esito.calcola_punteggio(id_giocatore=id_giocatore)
				#lista_esiti.append(nuovo_esito)
				if nuovo_esito.punteggio == massimo_punteggio_attuale:
					lista_esiti_migliori.append(nuovo_esito)
				elif nuovo_esito.punteggio > massimo_punteggio_attuale:
					lista_esiti_migliori = [nuovo_esito]
					massimo_punteggio_attuale = nuovo_esito.punteggio
				#al termine del for ripristino lo schema
				self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		#ne ritorno uno a caso di questi
		return random.choice(lista_esiti_migliori)

	#algoritmo min max per piu giocatori con le liste disinteressandosi della profodnita
	def algoritmo_min_max_multigiocatori_parallelo(self, 
									profondita:int, 
									id_giocatore:int, 
									flag_moltiplica_profondita:bool, #se devo o meno moltiplicare i punteggi per la profondita
									schema_gioco:List[List[int]], #schema del gioco non fisso
									lista_numeri_pedine_aggiunte:List[int], #lista dei numeri delle pedine aggiunte per gli altri giocatori
									lista_case_basi:List[Tuple[int, int]],  #lista casa base dei giocatori
									lista_dove_salvare_esito:List[EsitoAlgoritmoRicerca], #lista dove andare a salvare l'esito 
									massimo_numero_trhread:int, #numero massimo di thread impostato da grafica
									punteggio_da_aggiugnere:float, #punteggio da aggiungere al giocatore precedente prima di salvare l'esito nella lista
									lista_mosse_da_aggiungere:List[Tuple[int, int]], #lista delle mosse con cui popolare l'esito che si aggiunge alla lista di ritorno
									flag_primo:bool #flag che indica se e la prima chiamata, se e la prima chiamata non aggiorna l'elenco delle mosse
									) -> None:
		#metodo per calcolare il miglior punteggio di quel giocatore utilizzando parallelismo
		#mi creo l'oggetto da ritornare in seguito
		numero_giocatori = len(self.lista_giocatori)
		oggetto_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		giocatore_successivo = (id_giocatore+1) % numero_giocatori
		giocatore_precedente = (id_giocatore-1) % numero_giocatori
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			#metto l'esito nella lista
			oggetto_esito.lista_punteggi_giocatori[giocatore_precedente] += punteggio_da_aggiugnere
			#aggiorno l'elenco delle mosse solo se non e il primo
			if not(flag_primo):
				oggetto_esito.lista_mosse = lista_mosse_da_aggiungere[:]
			lista_dove_salvare_esito.append(oggetto_esito)
			return
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=schema_gioco, numero_giocatori=numero_giocatori):
			#allora vado a ricosrsione e ritorno lo stesso risultato aumentato della cifra della vincita della partita
			punteggio_raggiunto = -self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			#per calcolarmi l'esito a ricorsione non serve entri in un thread perche tanto comunque sara sequenziale
			lista_per_esiti_ricorsione:List[EsitoAlgoritmoRicerca] = []
			self.algoritmo_min_max_multigiocatori_parallelo(profondita=profondita, 
															id_giocatore=giocatore_successivo, 
															flag_moltiplica_profondita=flag_moltiplica_profondita,
															lista_numeri_pedine_aggiunte=lista_numeri_pedine_aggiunte,
															lista_case_basi=lista_case_basi,
															lista_dove_salvare_esito=lista_per_esiti_ricorsione,
															massimo_numero_trhread=massimo_numero_trhread,
															schema_gioco=schema_gioco,
															punteggio_da_aggiugnere=punteggio_raggiunto,
															lista_mosse_da_aggiungere=[], #metto la lista vuota perche il giocatore precedente non puo fare mosse
															flag_primo=False
															)
			esito_ricorsione = lista_per_esiti_ricorsione.pop()
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			#oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			oggetto_esito.lista_punteggi_giocatori[giocatore_precedente] += punteggio_da_aggiugnere
			#aggiorno l'elenco delle mosse solo se non e il primo
			if not(flag_primo):
				oggetto_esito.lista_mosse = lista_mosse_da_aggiungere[:]
			lista_dove_salvare_esito.append(oggetto_esito)
			return
		#altrimenti verifico il numero delle mosse
		
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=schema_gioco,
																				giocatore_in_questione=giocatore_in_turno,
																				flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																				flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																				flag_salva_lista_mosse=True,
																				numero_giocatori=numero_giocatori,
																				flag_damone_mossa_diagonale = self.flag_damone_mossa_diagonale,
																				valore_casella_damone = self.valore_pedina_damone,
																				flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
																				flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni)
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore, schema_gioco_input=schema_gioco)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			lista_per_esiti_ricorsione:List[EsitoAlgoritmoRicerca] = []
			esito_ricorsione = self.algoritmo_min_max_multigiocatori_parallelo(profondita=profondita-1, 
																				id_giocatore=giocatore_successivo, 
																				flag_moltiplica_profondita=flag_moltiplica_profondita,
																				lista_numeri_pedine_aggiunte=lista_numeri_pedine_aggiunte,
																				lista_case_basi=lista_case_basi,
																				lista_dove_salvare_esito=lista_per_esiti_ricorsione,
																				massimo_numero_trhread=massimo_numero_trhread,
																				schema_gioco=schema_gioco,
																				punteggio_da_aggiugnere=punteggio_raggiunto,
																				lista_mosse_da_aggiungere=[], #metto la lista vuota perche il giocatore precedente non puo fare mosse
																				flag_primo=False
																				)
			esito_ricorsione = lista_per_esiti_ricorsione.pop()
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			#oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			oggetto_esito.lista_punteggi_giocatori[giocatore_precedente] += punteggio_da_aggiugnere
			#aggiorno l'elenco delle mosse solo se non e il primo
			if not(flag_primo):
				oggetto_esito.lista_mosse = lista_mosse_da_aggiungere[:]
			lista_dove_salvare_esito.append(oggetto_esito)
			return
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta(matrice_attuale=schema_gioco):
			punteggio_raggiunto = self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			lista_per_esiti_ricorsione:List[EsitoAlgoritmoRicerca] = []
			esito_ricorsione = self.algoritmo_min_max_multigiocatori_parallelo(profondita=profondita-1, 
																				id_giocatore=giocatore_successivo, 
																				flag_moltiplica_profondita=flag_moltiplica_profondita,
																				lista_numeri_pedine_aggiunte=lista_numeri_pedine_aggiunte,
																				lista_case_basi=lista_case_basi,
																				lista_dove_salvare_esito=lista_per_esiti_ricorsione,
																				massimo_numero_trhread=massimo_numero_trhread,
																				schema_gioco=schema_gioco,
																				punteggio_da_aggiugnere=punteggio_raggiunto,
																				lista_mosse_da_aggiungere=[], #metto la lista vuota perche il giocatore precedente non puo fare mosse
																				flag_primo=False
																				)
			esito_ricorsione = lista_per_esiti_ricorsione.pop()
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			#oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			oggetto_esito.lista_punteggi_giocatori[giocatore_precedente] += punteggio_da_aggiugnere
			#aggiorno l'elenco delle mosse solo se non e il primo
			if not(flag_primo):
				oggetto_esito.lista_mosse = lista_mosse_da_aggiungere[:]
			lista_dove_salvare_esito.append(oggetto_esito)
			return
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#potrei provare a farmi una lisat di esiti 
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		lista_esiti_dove_salvare:List[EsitoAlgoritmoRicerca] = []
		lista_thread_startati:List[threading.Thread] = []
		for esito_lista_mosse in lista_mosse_disponibili:
			#eseguo la lista delle mosse calcolate
			esito_esegui_mosse_no_side_effect = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse_no_side_effect(
																		lista_mosse=esito_lista_mosse.lista_mosse_calcolate, 
																		id_giocatore=id_giocatore,
																		lista_case_basi=lista_case_basi,
																		lista_numeri_pedine_aggiunte=lista_numeri_pedine_aggiunte,
																		matrice_attuale=schema_gioco)
			punteggio_lista_mosse = esito_esegui_mosse_no_side_effect.punteggio * ( profondita if flag_moltiplica_profondita else 1 ) 
			#mi calcolo il punteggio a ricorsione e valuto se avviare il thread o semplicemente runnuarlo se ce ne sono gia troppi
			
			#valuto se devo fare run o start in base al numero di thread gia attivi
			#if threading.active_count() > massimo_numero_trhread:
			if len(self.lista_thread_totali) > massimo_numero_trhread:
				#chiamo semplicemente a ricorsione
				#dico che ho raggiunto il numero massimo di thread
				print(f'raggiunto massimo numero di thread ({massimo_numero_trhread}): {len(self.lista_thread_totali)}')
				self.algoritmo_min_max_multigiocatori_parallelo(profondita -1, 
																giocatore_successivo, 
																flag_moltiplica_profondita, 
																esito_esegui_mosse_no_side_effect.matrice, 
																esito_esegui_mosse_no_side_effect.numero_pedine_aggiunte_giocatori, 
																esito_esegui_mosse_no_side_effect.lista_case_basi, 
																lista_esiti_dove_salvare, 
																massimo_numero_trhread,
																punteggio_lista_mosse,
																esito_lista_mosse.lista_mosse_calcolate,
																False)
			else:
				#posso aumentare il numero di thread
				#nuovo_thread.run()
				#print('nuovo thread')
				nuovo_thread = threading.Thread(target=self.algoritmo_min_max_multigiocatori_parallelo, args=(
																		profondita -1, 
																		giocatore_successivo, 
																		flag_moltiplica_profondita, 
																		esito_esegui_mosse_no_side_effect.matrice, 
																		esito_esegui_mosse_no_side_effect.numero_pedine_aggiunte_giocatori, 
																		esito_esegui_mosse_no_side_effect.lista_case_basi, 
																		lista_esiti_dove_salvare, 
																		massimo_numero_trhread,
																		punteggio_lista_mosse,
																		esito_lista_mosse.lista_mosse_calcolate,
																		False), daemon=True)
				nuovo_thread.start()
				#lo aggiungo a quelli startati
				lista_thread_startati.append(nuovo_thread)
				self.lista_thread_totali.append(nuovo_thread)
		#adesso devo attendere che la lista degli esiti sia di lunghezza uguale a quella delle mosse -> significa che tutti i thread hanno terminato
		#numero_mosse = len(lista_mosse_disponibili)
		#while len(lista_esiti_dove_salvare) != numero_mosse:
		#	pass
		for t in lista_thread_startati:
		#while len(lista_thread_startati) >0:
			#t = lista_thread_startati.pop()
			t.join()
			#del t
			self.lista_thread_totali.remove(t)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		if len(lista_esiti_dove_salvare) == 1:
			#se ha solo 1 elemento lo ritorno
			#vado a salvarmi quell'elemento nella lista padre
			esito_ricorsione = lista_esiti_dove_salvare[0]
			esito_ricorsione.lista_punteggi_giocatori[giocatore_precedente] += punteggio_da_aggiugnere
			#aggiorno l'elenco delle mosse solo se non e il primo
			if not(flag_primo):
				esito_ricorsione.lista_mosse = lista_mosse_da_aggiungere[:] #qui non dovrebbe servire perche l'esito a ricorsione ha gia le mosse salvate
			lista_dove_salvare_esito.append(esito_ricorsione)
			return 
		#devo prima calcolarmi i punteggi di tutti gli esiti
		for esito in lista_esiti_dove_salvare:
			esito.calcola_punteggio(id_giocatore=id_giocatore)
		massimo_punteggio = max( [ i.punteggio for i in lista_esiti_dove_salvare ] )
		#mi calcolo gli esiti con punteggio massimo
		esiti_punteggio_massimo = [ i for i in lista_esiti_dove_salvare if i.punteggio==massimo_punteggio ]
		#mi stampo tutta la lista
		#print('lista punteggi')
		#for esito in sorted(lista_esiti, key=lambda x: x.punteggio):
		#	print(esito.lista_mosse, esito.punteggio)
		#ne calcolo uno a caso
		esito_ricorsione_da_aggiungere = random.choice(esiti_punteggio_massimo)
		esito_ricorsione_da_aggiungere.lista_punteggi_giocatori[giocatore_precedente] += punteggio_da_aggiugnere
		#aggiorno l'elenco delle mosse solo se non e il primo
		if not(flag_primo):
			esito_ricorsione_da_aggiungere.lista_mosse = lista_mosse_da_aggiungere[:] #qui non dovrebbe servire perche l'esito a ricorsione ha gia le mosse salvate
		lista_dove_salvare_esito.append(esito_ricorsione_da_aggiungere)
		return 


	#algoritmo min max per piu giocatori con le liste disinteressandosi della profodnita, prende la mossa che ha il miglior punteggio, in caso di parita le espande entrambe, dovrebbe permettere di andare piu in profondita
	def algoritmo_mossa_immediata_miglior_punteggio(self, profondita:int, id_giocatore:int, flag_moltiplica_profondita:bool) -> EsitoAlgoritmoRicerca:
		#metodo per calcolare il miglior punteggio di quel giocatore
		#mi creo l'oggetto da ritornare in seguito
		numero_giocatori = len(self.lista_giocatori)
		oggetto_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
		#caso base sono arrivato al limite della profondita
		if profondita <= 0:
			return oggetto_esito
		#altrimenti verifico se il giocatore sia elimitato, in tal caso vado direttamente al giocatore successivo
		#mi faccio un backup dello schema attuale
		schema_bk = [riga[:] for riga in self.schema_gioco]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in self.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in self.lista_giocatori ]
		case_basi_iniziali = [ i.casa_base for i in self.lista_giocatori ]

		giocatore_successivo = (id_giocatore+1) % numero_giocatori
		giocatore_in_turno = self.lista_giocatori[id_giocatore]
		if giocatore_in_turno.verifica_giocatore_eliminato(tabella_gioco=self.schema_gioco, numero_giocatori=numero_giocatori):
			#allora vado a ricosrsione e ritorno lo stesso risultato aumentato della cifra della vincita della partita
			punteggio_raggiunto = -self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_mossa_immediata_miglior_punteggio(profondita=profondita, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ripristino lo schema per sicurezza
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti verifico il numero delle mosse
		#lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.schema_gioco, flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine, flag_obbligatorio_mangiare=self.flag_obbligatorieta_mangiare, numero_giocatori=len(self.lista_giocatori))
		
		lista_mosse_disponibili = EsitoRicercaListaMosse.calcola_lista_mosse_complessive(matrice_inziale=self.schema_gioco,
																				giocatore_in_questione=giocatore_in_turno,
																				flag_mangiare_proprie_pedine=self.flag_mangiare_proprie_pedine,
																				flag_obbligo_mangiare=self.flag_obbligatorieta_mangiare,
																				flag_salva_lista_mosse=True,
																				numero_giocatori=numero_giocatori,
																				flag_damone_mossa_diagonale = self.flag_damone_mossa_diagonale,
																				valore_casella_damone = self.valore_pedina_damone,
																				flag_damoni_mangiano_diagonale=self.flag_damone_mangia_diagonale,
																				flag_damone_mangiato_solo_da_damoni=self.flag_damone_mangiato_solo_da_damoni)
		if len(lista_mosse_disponibili) == 0:
			#il giocatore e di fatto eliminato -> rimuovo tutte le sue caselle dallo schema
			self.rimuovi_pedine_giocatore(id_giocatore=id_giocatore)
			#mi calcolo il valore a ricorsrione
			#ho il punteggio di eliminazione del giocatore e quello di vincita della partita 
			punteggio_raggiunto = (-self.valore_vincita_partita - self.valore_eliminazione_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_mossa_immediata_miglior_punteggio(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a verificare se la partita e gia vinta da questo giocatore
		if self.verifica_partita_vinta():
			punteggio_raggiunto = self.valore_vincita_partita * ( profondita if flag_moltiplica_profondita else 1 )
			esito_ricorsione = self.algoritmo_mossa_immediata_miglior_punteggio(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita) #il meno ci va pèerche e un punteggio riferito all'altro giocatore
			#ripristino il backup
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			#ritorno il valore calcolato a ricsorione
			oggetto_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			oggetto_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_raggiunto
			return oggetto_esito
		#altrimenti vado a considerare tutte le mosse, e se mangio vado ad aggiornare l'elenco in base al fatto che mangio o meno
		#mi creo una lista con le mosse che hanno un miglior punteggio
		lista_mosse_miglior_punteggio : List[EsitoRicercaListaMosse.EsitoRicercaListaMosse]=[]
		miglior_punteggio :float = -math.inf
		for esito_lista_mosse in lista_mosse_disponibili:
			#eseguo la lista delle mosse calcolate
			punteggio_lista_mosse = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate, id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			#in base al punteggio valuto se aggiornare la lista
			if punteggio_lista_mosse > miglior_punteggio:
				#aggiorno il punteggio e svuoto la lista
				miglior_punteggio = punteggio_lista_mosse
				#lista_mosse_miglior_punteggio.clear()
				lista_mosse_miglior_punteggio = [esito_lista_mosse]
			elif punteggio_lista_mosse == miglior_punteggio:
				#la aggiungo alla lista
				lista_mosse_miglior_punteggio.append(esito_lista_mosse)
			#al termine del for ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
		#potrei provare a farmi una lisat di esiti 
		#adesso fino a quando questa lista non e vuota io itero e mi creo la lista di esiti
		lista_esiti_migliori:List[EsitoAlgoritmoRicerca] = []
		for esito_lista_mosse in lista_mosse_miglior_punteggio:
			#eseguo la lista delle mosse calcolate
			punteggio_lista_mosse = self.calcola_punteggio_complessivo_nuovo_schema_esegui_mosse(lista_mosse=esito_lista_mosse.lista_mosse_calcolate, id_giocatore=id_giocatore) * ( profondita if flag_moltiplica_profondita else 1 )
			#mi calcolo il punteggio a ricorsione
			esito_ricorsione = self.algoritmo_mossa_immediata_miglior_punteggio(profondita=profondita-1, id_giocatore=giocatore_successivo, flag_moltiplica_profondita=flag_moltiplica_profondita)
			#punteggio_attuale -= esito_ricorsione.punteggio
			#mi creo un nuovo esito
			nuovo_esito = EsitoAlgoritmoRicerca(numero_giocatori=numero_giocatori)
			#nuovo_esito.punteggio = punteggio_attuale
			nuovo_esito.lista_mosse= esito_lista_mosse.lista_mosse_calcolate
			nuovo_esito.lista_punteggi_giocatori = esito_ricorsione.lista_punteggi_giocatori[:]
			nuovo_esito.lista_punteggi_giocatori[id_giocatore] += punteggio_lista_mosse
			nuovo_esito.calcola_punteggio(id_giocatore=id_giocatore)
			lista_esiti_migliori.append(nuovo_esito)
			#al termine del for ripristino lo schema
			self.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
		#quando arrivo qui significa che ho terminato le mosse, mi calcolo il massimo punteggio
		if len(lista_esiti_migliori) == 1:
			#se ha solo 1 elemento lo ritorno
			return lista_esiti_migliori[0]
		massimo_punteggio = max( [ i.punteggio for i in lista_esiti_migliori ] )
		#mi calcolo gli esiti con punteggio massimo
		esiti_punteggio_massimo = [ i for i in lista_esiti_migliori if i.punteggio==massimo_punteggio ]
		#mi stampo tutta la lista
		#print('lista punteggi')
		#for esito in sorted(lista_esiti, key=lambda x: x.punteggio):
		#	print(esito.lista_mosse, esito.punteggio)
		#ne ritorno uno a caso di questi
		return random.choice(esiti_punteggio_massimo)

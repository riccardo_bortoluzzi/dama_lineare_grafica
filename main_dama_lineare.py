#!.venv/bin/python3

#script per gestire il main della dama lineare con grafica preso spunto da dama lineare

import sys, traceback

try:

	import GraficaDamaLineare
	from PyQt5 import QtCore, QtGui, QtWidgets

	if __name__ == '__main__':
		app = QtWidgets.QApplication(sys.argv)

		grafica = GraficaDamaLineare.GraficaDamaLineare()
		grafica.show()

		sys.exit(app.exec_())
except Exception as e:
	etype, value, tb = sys.exc_info()
	stringa_errore = ''.join(traceback.format_exception(etype, value, tb))
	print(stringa_errore)
	input('Si è verificato un errore. inviare le scritte qui sopra a riccardo.bortoluzzi96+damalineare@gmail.com')

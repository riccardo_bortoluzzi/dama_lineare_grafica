#script che contiene la classe per gestire un giocatorie

from __future__ import annotations
from typing import List, Tuple

import Costanti

class Giocatore:
	colore : str #colore del giocatore
	casa_base : Tuple[int, int] #tupla che indica la casella in cui portare la generatrice per generare altre pedine
	casa_base_originale : Tuple[int, int] #casa base originale utilizzata per stabilire la casella da cui spostarsi per inserire le nuove pedine
	pedine_aggiunte_dopo_ultima_aggiunta : int #numero di pedine aggiunte dalla ultima aggiunta di una generatrice
	is_umano:bool #flag che indica se il giocatore e umano o un pc
	is_eliminato : bool #flag che indica se il giocatore e eliminato
	id_giocatore : int
	colore_selezione : str #colore da utilizzare nella grafica per indicare le celle selezionate
	colore_traduzione : str #traduzione del colore 
	lista_celle_selezionate : List[Tuple[int, int]] #lista delle celle selezionate per la grafica
	riga_damoni : int #riga dove il giocatore guadagna un damone
	colonna_damoni : int #colonna dove il giocatore guadagna un damone

	def __init__(self, id_giocatore:int, casa_base:Tuple[int, int], is_umano:bool, dim_v:int, dim_o:int) -> None:
		self.id_giocatore = id_giocatore
		self.colore = Costanti.lista_colori[self.id_giocatore]
		self.colore_selezione = Costanti.lista_colori_selezione[self.id_giocatore]
		self.colore_traduzione = Costanti.lista_traduzione_colori[self.id_giocatore]
		self.casa_base = casa_base
		self.casa_base_originale = casa_base
		self.pedine_aggiunte_dopo_ultima_aggiunta = 0
		self.is_umano = is_umano
		self.is_eliminato = False
		self.lista_celle_selezionate = []
		#mi calcolo riga e colonna damoni
		(r_base, c_base) = casa_base
		self.riga_damoni = dim_v -1 -r_base
		self.colonna_damoni = dim_o -1 -c_base

	def __eq__(self, g2:Giocatore) -> bool:
		#poiche i giocatori sono distinti dallo stesso colore
		return self.colore == g2.colore

	def verifica_giocatore_eliminato(self, tabella_gioco:List[List[int]], numero_giocatori:int) -> bool:
		#metodo per verificare se un giocatore risulta eliminato
		dim_v = len(tabella_gioco)
		dim_o = len(tabella_gioco[0])
		for r in range(dim_v):
			for c in range(dim_o):
				valore_casella = tabella_gioco[r][c]
				if valore_casella != Costanti.numero_casella_vuota:
					#verifico se e di questo giocatore
					if (valore_casella % numero_giocatori) == self.id_giocatore:
						#la pedina e di questo giocatore, non e eliminato
						return False
		#altrimenti dico che e eliminato
		return True

	def verifica_giocatore_vincente(self, tabella_gioco:List[List[int]], numero_giocatori:int) -> bool:
		#metodo per verificare se un giocatore risulta vincente in uno schema
		dim_v = len(tabella_gioco)
		dim_o = len(tabella_gioco[0])
		for r in range(dim_v):
			for c in range(dim_o):
				valore_casella = tabella_gioco[r][c]
				if valore_casella != Costanti.numero_casella_vuota:
					#verifico se non e di questo giocatore
					if (valore_casella % numero_giocatori) != self.id_giocatore:
						#la pedina e di questo giocatore, non e eliminato
						return False
		#altrimenti dico che e vincente
		return True

	def verifica_giocatore_senza_generatrici(self, 
											 tabella_gioco:List[List[int]], 
											 numero_giocatori:int, 
											 valore_generatrice:int, 
											 valore_damone:int,
											 is_damone_genetratrice:bool) -> bool:
		#metodo per verificare se un giocatore ha perso tutte le generatrici
		dim_v = len(tabella_gioco)
		dim_o = len(tabella_gioco[0])
		for r in range(dim_v):
			for c in range(dim_o):
				valore_casella = tabella_gioco[r][c]
				if valore_casella != Costanti.numero_casella_vuota:
					#verifico se e di questo giocatore
					if (valore_casella % numero_giocatori) == self.id_giocatore: 
						tipologia_casella = (valore_casella // numero_giocatori)
						if (tipologia_casella == valore_generatrice):
							#la generatrice e di questo giocatore, non le ha perse tutte
							return False
						#controllo per i damoni
						if is_damone_genetratrice and (tipologia_casella == valore_damone):
							#e un damone genetrice dico che non gha terminato le generatrici
							return False
		#altrimenti dico che ha perso tutte le generatrici
		return True

	##################################################################################################################################################################################################################
	#metodi per calcolare la lisat delle mosse disponibili
	def calcola_lista_successive_celle_per_mangiare_singola_casella(self, 
																	tabella_gioco:List[List[int]], 
																	flag_mangiare_proprie_pedine:bool, 
																	numero_giocatori:int, 
																	r:int, 
																	c:int,
																	flag_damoni_mangiano_diagonale : bool,
																	valore_damone:int,
																    flag_damone_mangiato_solo_da_damoni : bool
																   ) -> List[Tuple[int, int]]:
		#metodo per calcolare la lista delle celle successive per mangiare da una cella data
		lista_celle = []
		#mi calcolo le dimensioni dello schema
		dim_v = len(tabella_gioco)
		dim_o = len(tabella_gioco[0])
		valore_casella = tabella_gioco[r][c]
		tipologia_pedina = valore_casella//numero_giocatori
		if (valore_casella != Costanti.numero_casella_vuota) and ((valore_casella%numero_giocatori) == self.id_giocatore):
			#la casella contiene una cella del giocatore in questione
			#mi calcolo le possibili direzioni
			direzioni = (-2, 2)
			for dr in direzioni:
				#verifico se posso muovermi in verticale
				if (0 <= r+dr < dim_v) and (tabella_gioco[r+dr][c] == Costanti.numero_casella_vuota) and (tabella_gioco[r+(dr//2)][c] != Costanti.numero_casella_vuota):
					#se arrivo qui significa che la casella puo saltare perche ha una cella libera e in mezzo c'e un altra cella, valuto se posso mangiarla perche e di questo giocatore o no
					if flag_mangiare_proprie_pedine or ( (tabella_gioco[r+(dr//2)][c]%numero_giocatori) != self.id_giocatore ):
						#verifico se posso mangiare in base al flag del damone
						if flag_damone_mangiato_solo_da_damoni:
							#se quella mangiata non e un damone posso mangiare sempre, altrimenti devo controllare anche quella che mangia sia un damone
							if ((tabella_gioco[r+(dr//2)][c]//numero_giocatori) != valore_damone) or (tipologia_pedina == valore_damone):
								lista_celle.append(  (r+dr, c)  )
						else:
							#posso mangiare sempre
							lista_celle.append(  (r+dr, c)  )

			for dc in direzioni:
				#verifico se posso muovermi in orizzontale
				if (0 <= c+dc < dim_o) and (tabella_gioco[r][c+dc] == Costanti.numero_casella_vuota) and (tabella_gioco[r][c + (dc//2)] != Costanti.numero_casella_vuota):
					#se arrivo qui significa che la casella puo saltare perche ha una cella libera e in mezzo c'e un altra cella, valuto se posso mangiarla perche e di questo giocatore o no
					if flag_mangiare_proprie_pedine or ( (tabella_gioco[r][c+(dc//2)]%numero_giocatori) != self.id_giocatore ):
						#verifico se posso mangiare in base al flag del damone
						if flag_damone_mangiato_solo_da_damoni:
							#se quella mangiata non e un damone posso mangiare sempre, altrimenti devo controllare anche quella che mangia sia un damone
							if ((tabella_gioco[r][c+(dc//2)]//numero_giocatori) != valore_damone) or (tipologia_pedina == valore_damone):
								lista_celle.append( (r, c+dc) )
						else:
							#posso mangiare sempre
							lista_celle.append( (r, c+dc) )
			#valuto se e un damone e se puo mangiare
			if flag_damoni_mangiano_diagonale:
				if tipologia_pedina == valore_damone:
					#allora aggiungo anche la diagonale se serve
					for dr in (-2, 2):
						for dc in (-2, 2):
							if (0 <= r+dr < dim_v) and (0 <= c+dc < dim_o):
								if (tabella_gioco[r+dr][c +dc] == Costanti.numero_casella_vuota) and (tabella_gioco[r+(dr//2)][c+(dc//2)] != Costanti.numero_casella_vuota):
									#se arrivo qui significa che la casella puo saltare perche ha una cella libera e in mezzo c'e un altra cella, valuto se posso mangiarla perche e di questo giocatore o no
									if flag_mangiare_proprie_pedine or ( (tabella_gioco[r+(dr//2)][c+(dc//2)]%numero_giocatori) != self.id_giocatore ):
										#allora posso mangiare, indipendentemente dal flag del damone mangia solo damone
										lista_celle.append(  (r+dr, c+dc)  )
		return lista_celle

	def calcola_lista_mosse_per_mangiare(self, 
										tabella_gioco:List[List[int]], 
										flag_mangiare_proprie_pedine:bool, 
										numero_giocatori:int,
										flag_damoni_mangiano_diagonale:bool,
										valore_damone:int,
										flag_damone_mangiato_solo_da_damoni:bool) -> List[Tuple[Tuple[int, int], Tuple[int, int]]]:
		#metodo per calcolare la lista delle mosse se si puo mangiare
		#non mi interessa delle pedine generate perche questo metodo viene eseguito di continuo
		lista_percorsi_da_seguire = []
		#mi calcolo le dimensioni dello schema
		dim_v = len(tabella_gioco)
		dim_o = len(tabella_gioco[0])
		#vado a considerare tutte le caselle dello schema e verifico se le pedine appartengono a questo giocatore
		for r in range(dim_v):
			for c in range(dim_o):
				for next_cella in self.calcola_lista_successive_celle_per_mangiare_singola_casella(tabella_gioco=tabella_gioco, 
																								flag_mangiare_proprie_pedine=flag_mangiare_proprie_pedine, 
																								numero_giocatori=numero_giocatori, 
																								r=r, 
																								c=c,
																								flag_damoni_mangiano_diagonale=flag_damoni_mangiano_diagonale,
																								valore_damone=valore_damone, 
																								flag_damone_mangiato_solo_da_damoni=flag_damone_mangiato_solo_da_damoni
																								):
					lista_percorsi_da_seguire.append( ( (r,c), next_cella ) )
		#alla fine posso ritornare la lista delle mosse possibili
		return lista_percorsi_da_seguire

	def calcola_lista_mosse_normali(self, 
									tabella_gioco:List[List[int]], 
									numero_giocatori:int,
									flag_damone_mossa_diagonale:bool,
									valore_casella_damone : int) -> List[Tuple[Tuple[int, int], Tuple[int, int]]]:
		#metodo per calcolare la lista delle mosse normali
		#non mi interessa delle pedine generate perche questo metodo viene eseguito di continuo
		lista_percorsi_da_seguire = []
		#mi calcolo le dimensioni dello schema
		dim_v = len(tabella_gioco)
		dim_o = len(tabella_gioco[0])
		#vado a considerare tutte le caselle dello schema e verifico se le pedine appartengono a questo giocatore
		for r in range(dim_v):
			for c in range(dim_o):
				valore_casella = tabella_gioco[r][c]
				if (valore_casella != Costanti.numero_casella_vuota) and ((valore_casella%numero_giocatori) == self.id_giocatore):
					#la casella contiene una cella del giocatore in questione
					#mi calcolo le possibili direzioni
					direzioni = (-1, 1)
					for dr in direzioni:
						#verifico se posso muovermi in verticale
						if (0 <= r+dr < dim_v) and (tabella_gioco[r+dr][c] == Costanti.numero_casella_vuota):
							#se arrivo qui significa che posso muovermi in quella direzione
							lista_percorsi_da_seguire.append( ((r,c), (r+dr, c)) )
					for dc in direzioni:
						#verifico se posso muovermi in orizzontale
						if (0 <= c+dc < dim_o) and (tabella_gioco[r][c+dc] == Costanti.numero_casella_vuota):
							#se arrivo qui significa che la si muo muovere
							lista_percorsi_da_seguire.append( ((r,c), (r, c+dc)) )
					#adesso verifico se e un damno ed aggiungo anche le diagonali se servisse
					if flag_damone_mossa_diagonale:
						#mi calolo la tipologia di pedina in questione
						tipologia_pedina = valore_casella // numero_giocatori
						if tipologia_pedina == valore_casella_damone:
							#e un damone ci aggiungo le diagonali
							for dr in (-1, 1):
								for dc in (-1, 1):
									if (0 <= r+dr < dim_v) and (0 <= c+dc < dim_o): 
										#verifico se la caselle sono libere
										if tabella_gioco[r+dr][c+dc] == Costanti.numero_casella_vuota:
											lista_percorsi_da_seguire.append( ((r,c), (r+dr, c+dc)) )
		#alla fine posso ritornare la lista delle mosse possibili
		return lista_percorsi_da_seguire

	def calcola_lista_mosse_possibili_totale(self, 
											 schema_gioco:List[List[int]], 
											 flag_mangiare_proprie_pedine:bool, 
											 numero_giocatori:int, 
											 flag_obbligatorio_mangiare:int,
											 flag_damone_mossa_diagonale:bool,
											 valore_casella_damone:int,
											 flag_damoni_mangiano_diagonale:bool,
											 flag_damone_mangiato_solo_da_damoni:bool
											) -> List[Tuple[Tuple[int, int], Tuple[int, int]]]:
		#metodo per calcolare tutte le mosse possibili del giocatore
		#per prima cosa mi calcolo le mosse possibili da mangiare
		mosse_calcolate = self.calcola_lista_mosse_per_mangiare(tabella_gioco=schema_gioco, 
																flag_mangiare_proprie_pedine=flag_mangiare_proprie_pedine, 
																numero_giocatori=numero_giocatori,
																flag_damoni_mangiano_diagonale=flag_damoni_mangiano_diagonale, 
																valore_damone=valore_casella_damone, 
																flag_damone_mangiato_solo_da_damoni=flag_damone_mangiato_solo_da_damoni
															   )
		#se la lunghezza e 0 oppure se non e obbligatorio mangiare provo l'altra lista
		if (len(mosse_calcolate) == 0) or ( not(flag_obbligatorio_mangiare) ):
			mosse_semplici = self.calcola_lista_mosse_normali(tabella_gioco=schema_gioco, 
															  numero_giocatori=numero_giocatori, 
															  flag_damone_mossa_diagonale=flag_damone_mossa_diagonale,
															  valore_casella_damone=valore_casella_damone)
			mosse_calcolate += mosse_semplici
		return mosse_calcolate

	#############################################################################################################################################################################
	#metodo con la logica per stabilire la prossima cella libera

	def calcola_cella_libera_per_pedina_aggiunta(self, tabella_gioco:List[List[int]]) -> Tuple[int, int]:
		#metodo per calcolare la prima cella libera in cui poter inserire una nuova casella
		#mi calcolo le dimensioni dello schema
		dim_v = len(tabella_gioco)
		dim_o = len(tabella_gioco[0])
		#adesso mi calcolo il vettore spostamento in base alla casa base
		#(r_casa_base, c_casa_base) = self.casa_base
		(r_casa_base, c_casa_base) = self.casa_base_originale
		if r_casa_base == 0:
			#allora devo andare verso il basso
			dr = +1
		else: 
			#devo muovermi verso l'alto
			dr = -1
		if c_casa_base == 0:
			#devo muovermi verso destra
			dc = +1
		else:
			#mi muovo verso sinistra
			dc = -1
		vettore_spostamento = (dr, dc)
		#adesso ricerco subito nella diagonale principale
		delta_diagonale = 0
		while (0 <= r_casa_base + (delta_diagonale*dr) < dim_v) and (0 <= c_casa_base + (delta_diagonale*dc) < dim_o):
			#verifico se la cella e libera
			valore_casella = tabella_gioco[r_casa_base + (delta_diagonale*dr)][c_casa_base + (delta_diagonale*dc)]
			if valore_casella == Costanti.numero_casella_vuota:
				#allora ho trovato una casewlla libera nella diagonale
				return (r_casa_base + (delta_diagonale*dr), c_casa_base + (delta_diagonale*dc))
			#altrimenti aumento il delta
			delta_diagonale += 1
		#se arrivo qui significa che la diagonale e sempre piena
		#adesso provo a scendere dalla diagonale partendo da 2 (per evitare di mangiare o essere mangiati) partendo sempre da destra
		delta_diagonale = 0
		while (0 <= r_casa_base + (delta_diagonale*dr) < dim_v) and (0 <= c_casa_base + (delta_diagonale*dc) < dim_o):
			#adesso vado a fare un deltas lungo verticale o orizzontale
			delta_spostamento_verticale = 2
			delta_spostamento_orizzontale = 2
			#adesso devo distinguere se spostarmi prima in verticale o in orizzontale
			#mi devo spostare prima a destra, quindi in orizzontale solo se una delle 2 case basi è 0 (ovvero in diagonale principale)
			if self.casa_base_originale.count(0) == 1:
				#prima mi sposto in colonna poi in riga
				while (0 <= c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc) < dim_o):
					#recupero il valore della casella
					valore_casella = tabella_gioco[r_casa_base + (delta_diagonale*dr)][c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc)]
					if valore_casella == Costanti.numero_casella_vuota:
						#posso inserirla li
						return (r_casa_base + (delta_diagonale*dr), c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc))
					#altrimenti incremento la disscesa
					delta_spostamento_orizzontale += 1
				#controllo spostamento in verticale
				while 0 <= r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr) < dim_v:
					#recupero il valore della casella
					valore_casella = tabella_gioco[r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr)][c_casa_base + (delta_diagonale*dc)]
					if valore_casella == Costanti.numero_casella_vuota:
						#posso inserirla li
						return (r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr), c_casa_base + (delta_diagonale*dc))
					#altrimenti incremento la disscesa
					delta_spostamento_verticale += 1
			else:
				#prima mi sposto in verticale e poi in orizzontale
				#controllo spostamento in verticale
				while 0 <= r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr) < dim_v:
					#recupero il valore della casella
					valore_casella = tabella_gioco[r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr)][c_casa_base + (delta_diagonale*dc)]
					if valore_casella == Costanti.numero_casella_vuota:
						#posso inserirla li
						return (r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr), c_casa_base + (delta_diagonale*dc))
					#altrimenti incremento la disscesa
					delta_spostamento_verticale += 1
				#controlo spostamento orizzontale
				while (0 <= c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc) < dim_o):
					#recupero il valore della casella
					valore_casella = tabella_gioco[r_casa_base + (delta_diagonale*dr)][c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc)]
					if valore_casella == Costanti.numero_casella_vuota:
						#posso inserirla li
						return (r_casa_base + (delta_diagonale*dr), c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc))
					#altrimenti incremento la disscesa
					delta_spostamento_orizzontale += 1
			#altrimenti aumento il delta
			delta_diagonale += 1
		#se arrivo qui significa che anche quelle sotto o sopra diagonale distanziate di 2 sono piene, ricerco anche quelle distanziate di 1
		delta_diagonale = 0
		while (0 <= r_casa_base + (delta_diagonale*dr) < dim_v) and (0 <= c_casa_base + (delta_diagonale*dc) < dim_o):
			#adesso vado a fare un deltas lungo verticale o orizzontale
			delta_spostamento_verticale = 2
			delta_spostamento_orizzontale = 2
			#adesso devo distinguere se spostarmi prima in verticale o in orizzontale
			#mi devo spostare prima a destra, quindi in orizzontale solo se una delle 2 case basi è 0 (ovvero in diagonale principale)
			if self.casa_base_originale.count(0) == 1:
				#prima mi sposto in colonna poi in riga
				if (0 <= c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc) < dim_o):
					#recupero il valore della casella
					valore_casella = tabella_gioco[r_casa_base + (delta_diagonale*dr)][c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc)]
					if valore_casella == Costanti.numero_casella_vuota:
						#posso inserirla li
						return (r_casa_base + (delta_diagonale*dr), c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc))
				#controllo spostamento in verticale
				if 0 <= r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr) < dim_v:
					#recupero il valore della casella
					valore_casella = tabella_gioco[r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr)][c_casa_base + (delta_diagonale*dc)]
					if valore_casella == Costanti.numero_casella_vuota:
						#posso inserirla li
						return (r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr), c_casa_base + (delta_diagonale*dc))
			else:
				#prima mi sposto in verticale e poi in orizzontale
				#controllo spostamento in verticale
				if 0 <= r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr) < dim_v:
					#recupero il valore della casella
					valore_casella = tabella_gioco[r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr)][c_casa_base + (delta_diagonale*dc)]
					if valore_casella == Costanti.numero_casella_vuota:
						#posso inserirla li
						return (r_casa_base + (delta_diagonale*dr) - (delta_spostamento_verticale*dr), c_casa_base + (delta_diagonale*dc))
				#controlo spostamento orizzontale
				if (0 <= c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc) < dim_o):
					#recupero il valore della casella
					valore_casella = tabella_gioco[r_casa_base + (delta_diagonale*dr)][c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc)]
					if valore_casella == Costanti.numero_casella_vuota:
						#posso inserirla li
						return (r_casa_base + (delta_diagonale*dr), c_casa_base + (delta_diagonale*dc) - (delta_spostamento_orizzontale*dc))
			#altrimenti aumento il delta
			delta_diagonale += 1
		#qui dovrei essere riuscito in un modo o nell'altro ad analizzare tutte le caselle dello shcema se ancora non ha trovato niente significa che c'è un errore procedurale
		raise Exception('Impossibile trovare una casella libera, ricontrollare procedure')



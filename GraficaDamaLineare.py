#script che contiene la gestione della grafica della dama lineare


from typing import Dict, List, Tuple
from PyQt5 import QtCore, QtGui, QtWidgets


import Ui_DamaLineare, Giocatore, Costanti, ThreadGrafica
from GiocoAlgoritmi import GiocoAlgoritmi

class GraficaDamaLineare(QtWidgets.QMainWindow):
	#estende la classe di qt

	grafica_ui : Ui_DamaLineare.Ui_DamaLineare
	partita_attuale : GiocoAlgoritmi
	#lista_celle_selezionate : List[List[Tuple[int, int]]] #non utilizzata perche dentro i giocatori
	giocatore_attuale : int #indice che indica quale giocatore deve muovere
	is_partita_finita : bool #indica se la partita e terminata o meno
	thread_attesa_mossa_pc : ThreadGrafica.ThreadGrafica
	id_partita_attuale : int
	dizionario_immagini:Dict[str, QtGui.QPixmap] #dizionario contenente le icone per evitare ogni volta di aprire un file, ho come chiavi i file
	

	def __init__(self) -> None:
		super().__init__()

		ui = Ui_DamaLineare.Ui_DamaLineare()
		ui.setupUi(self)

		self.grafica_ui = ui
		self.grafica_ui.label_giocatore_attuale.setText("")
		self.id_partita_attuale = 0
		self.dizionario_immagini = {}

	#########################################################################################################################################################################
	#metodi generali

	#metodo per aggiornare lo schema di gioco ricalcolandolo ogni volta
	def aggiorna_schema_gioco(self):
		#va a fare side effect sullo schema
		dim_v = len(self.partita_attuale.schema_gioco)
		dim_o = len(self.partita_attuale.schema_gioco[0])
		#vado solo a eliminarmi tutti gli elementi
		for r in range(dim_v):
			for c in range(dim_o):
				item_casella = self.grafica_ui.tableWidget_schema_gioco.item(r, c)
				self.grafica_ui.tableWidget_schema_gioco.removeCellWidget(r, c)
				del item_casella
		#per prima cosa svuota lo schema
		#self.grafica_ui.tableWidget_schema_gioco.setColumnCount(0)
		#self.grafica_ui.tableWidget_schema_gioco.setRowCount(0)
		#per poi ripopolarlo
		#self.grafica_ui.tableWidget_schema_gioco.setColumnCount(dim_o)
		#self.grafica_ui.tableWidget_schema_gioco.setRowCount(dim_v)
		#recupero la dimensione dei quadrati
		dimensione_quadrati = self.grafica_ui.spinBox_dimensione_celle.value()
		#aggiorno le dimensioni della griglia
		self.grafica_ui.tableWidget_schema_gioco.horizontalHeader().setDefaultSectionSize(dimensione_quadrati)
		self.grafica_ui.tableWidget_schema_gioco.horizontalHeader().setMinimumSectionSize(dimensione_quadrati)
		self.grafica_ui.tableWidget_schema_gioco.verticalHeader().setDefaultSectionSize(dimensione_quadrati)
		self.grafica_ui.tableWidget_schema_gioco.verticalHeader().setMinimumSectionSize(dimensione_quadrati)
		#adesso itero per tutte le righe e colonne e vado ad inserire gli item nella tabella
		for r in range(dim_v):
			for c in range(dim_o):
				nuovo_item = QtWidgets.QTableWidgetItem()
				#ci metto sempre il colore bianco come sfondo
				nuovo_item.setBackground(QtGui.QColor(Costanti.colore_sfondo_default))
				#aggiungo l'elemento alla tabella
				self.grafica_ui.tableWidget_schema_gioco.setItem(r, c, nuovo_item)
		for r in range(dim_v):
			for c in range(dim_o):
				#mi recupero il valore della casella
				valore_casella = self.partita_attuale.schema_gioco[r][c]
				#se il valore e il numero di default -1 allora non faccio niente, altrimenti mi recupero la tipologia e il colore della pedina
				if valore_casella != Costanti.numero_casella_vuota:
					valore_pedina = valore_casella // len(self.partita_attuale.lista_giocatori)
					id_giocatore = valore_casella % len(self.partita_attuale.lista_giocatori)
					colore_giocatore = self.partita_attuale.lista_giocatori[id_giocatore].colore
					#mi ricavo il nome dell'icona
					if valore_pedina == self.partita_attuale.valore_pedina_generatrice:
						file_icona = Costanti.nome_file_immagine_generatrice.format(colore=colore_giocatore)
					elif valore_pedina == self.partita_attuale.valore_pedina_semplice:
						#allora e una pedina semplice
						file_icona = Costanti.nome_file_immagine_pedina.format(colore=colore_giocatore)
					else:
						#significa che e un damone
						file_icona = Costanti.nome_file_immagine_damone.format(colore=colore_giocatore)
					#valuto se e gia presente nel dizionario
					if self.dizionario_immagini.get(file_icona) is not None:
						pic = self.dizionario_immagini.get(file_icona)
					else:
						pic =QtGui.QPixmap(file_icona)
						#la aggiungo al dizionario
						self.dizionario_immagini[file_icona] = pic
					assert isinstance(pic, QtGui.QPixmap)
					immagine_scalata = pic.scaledToHeight(dimensione_quadrati)
					label = QtWidgets.QLabel()
					label.setPixmap(immagine_scalata)
					label.setScaledContents(True)
					self.grafica_ui.tableWidget_schema_gioco.setCellWidget(r, c, label)
					#ci metto l'icona nell'item
					#nuovo_item.setIcon(QtGui.QIcon(immagine_scalata))
				
		#adesso vado ad inserire le case basi
		#for giocatore in self.partita_attuale.lista_giocatori:
		for giocatore in ( self.partita_attuale.lista_giocatori[self.giocatore_attuale+1:] + self.partita_attuale.lista_giocatori[:self.giocatore_attuale+1] ):
			#cosi il giocatore attuale e swempre l'ultimo ed eventualmente sovrappongo le sue celle selezionate
			(r,c) = giocatore.casa_base
			item_casa_base = self.grafica_ui.tableWidget_schema_gioco.item(r, c)
			item_casa_base.setBackground(QtGui.QColor(giocatore.colore))
			#adesso vado a selezionare le caselle in cui il giocatore si e mosso
			for (r_m, c_m) in giocatore.lista_celle_selezionate:
				item_selezione = self.grafica_ui.tableWidget_schema_gioco.item(r_m, c_m)
				item_selezione.setBackground(QtGui.QColor(giocatore.colore_selezione))
		giocatore = self.partita_attuale.lista_giocatori[self.giocatore_attuale]
		#adesso vado a verificare se devo mostrare le mosse possibili con la casella selezionata
		if len(giocatore.lista_celle_selezionate) >0:
			#significa che ha selezionato almeno una casella, le successive possibili le coloro di giallo
			if len(giocatore.lista_celle_selezionate) == 1:
				#e la prima mossa mostro tutte le possibili
				lista_mosse_disponibili = giocatore.calcola_lista_mosse_possibili_totale(schema_gioco=self.partita_attuale.schema_gioco, 
																flag_mangiare_proprie_pedine=self.partita_attuale.flag_mangiare_proprie_pedine, 
																numero_giocatori=len(self.partita_attuale.lista_giocatori), 
																flag_obbligatorio_mangiare=self.partita_attuale.flag_obbligatorieta_mangiare,
																flag_damone_mossa_diagonale=self.partita_attuale.flag_damone_mossa_diagonale,
																valore_casella_damone=self.partita_attuale.valore_pedina_damone,
																flag_damoni_mangiano_diagonale=self.partita_attuale.flag_damone_mangia_diagonale,
																flag_damone_mangiato_solo_da_damoni=self.partita_attuale.flag_damone_mangiato_solo_da_damoni)
				#verifico se tra le mosse disponibili ce ne una che parte con quella ultima
				celle_successive_disponibili = [ mossa[1] for mossa in lista_mosse_disponibili if mossa[0]==giocatore.lista_celle_selezionate[-1] ]
				for (r_next, c_next) in celle_successive_disponibili:
					#recupero la cella e la coloro di giallo
					item_next = self.grafica_ui.tableWidget_schema_gioco.item(r_next, c_next)
					item_next.setBackground(QtGui.QColor(Costanti.colore_selezione_casella))
			else:
				#devo valutare se l'ultima mossa e stata una mangiata e in tal caso mostro altre mangiate
				[ (r_penultima, c_penultima), (r_ultima, c_ultima) ] = giocatore.lista_celle_selezionate[-2:]
				#se non sono di mangiata non faccio niente
				if ( abs(r_ultima - r_penultima) == 2) or (abs(c_ultima - c_penultima) == 2 ):
					#allora ho mangiato, vedo se posso continuare a mangiare
					lista_mosse_disponibili = giocatore.calcola_lista_mosse_per_mangiare(tabella_gioco=self.partita_attuale.schema_gioco, 
																	flag_mangiare_proprie_pedine=self.partita_attuale.flag_mangiare_proprie_pedine, 
																	numero_giocatori=len(self.partita_attuale.lista_giocatori),
																	flag_damoni_mangiano_diagonale=self.partita_attuale.flag_damone_mangia_diagonale,
																	valore_damone=self.partita_attuale.valore_pedina_damone,
																	flag_damone_mangiato_solo_da_damoni=self.partita_attuale.flag_damone_mangiato_solo_da_damoni)
					#verifico se tra le mosse disponibili ce ne una che parte con quella ultima
					celle_successive_disponibili = [ mossa[1] for mossa in lista_mosse_disponibili if mossa[0]==giocatore.lista_celle_selezionate[-1] ]
					for (r_next, c_next) in celle_successive_disponibili:
						#recupero la cella e la coloro di giallo
						item_next = self.grafica_ui.tableWidget_schema_gioco.item(r_next, c_next)
						item_next.setBackground(QtGui.QColor(Costanti.colore_selezione_casella))
		#adesso metto le celle selezionate
		#for (r,c) in self.lista_celle_selezionate:
		#	item_casa_base = self.grafica_ui.tableWidget_schema_gioco.item(r, c)
		#	item_casa_base.setBackground(QtGui.QColor(Costanti.colore_selezione_casella))
		#adesso aggiorno la dimensione delle caselle
		for r in range(dim_v):
			self.grafica_ui.tableWidget_schema_gioco.setRowHeight(r, dimensione_quadrati)
		for c in range(dim_o):
			self.grafica_ui.tableWidget_schema_gioco.setColumnWidth(c, dimensione_quadrati)
		#self.ridimensiona_celle_tabella()
		#mostro la lista dei giocatori
		self.mostra_lista_giocatori()
		#self.mostra_lista_parametri()

	def mostra_lista_giocatori(self):
		#metodo per mostrare la lista dei giocatori nell'apposita sezione
		stringa_giocatori = ''
		for id_giocatore in range(len(self.partita_attuale.lista_giocatori)):
			giocatore = self.partita_attuale.lista_giocatori[id_giocatore]
			#mi calcolo la prossima pedina da inserire
			#mi calcolo il tipo giocatore
			if giocatore.is_umano:
				tipo_giocatore = 'umano'
			else:
				tipo_giocatore = 'computer'
			if giocatore.is_eliminato:
				stringa_eliminato = '(eliminato)'
			else:
				#if ((giocatore.pedine_aggiunte_dopo_ultima_aggiunta +1)%len(self.partita_attuale.lista_giocatori)) == 0:
				#	next_pedina= 'generatrice'
				#else:
				#	next_pedina = 'semplice'
				if self.partita_attuale.numero_pedine_prima_generatrice == 0:
					stringa_eliminato = '(non ci saranno altre generatrici)'
				else:
					stringa_eliminato = f"(pedine per la prossima generatrice: {self.partita_attuale.numero_pedine_prima_generatrice - 1 - giocatore.pedine_aggiunte_dopo_ultima_aggiunta})"
			#vado a popolare la stringa
			stringa_giocatori += f'Giocatore {id_giocatore+1} ({tipo_giocatore}): {giocatore.colore_traduzione} {stringa_eliminato}'
			#ci aggiungo anche l'acapo se serve
			if id_giocatore != len(self.partita_attuale.lista_giocatori)-1:
				stringa_giocatori += '\n'
		#inserisco in grafica la stringa
		self.grafica_ui.textBrowser_lista_giocatori.setText(stringa_giocatori)
	
	def mostra_lista_parametri(self):
		#inserisco anche i parametri della partita
		stringa_parametri = ''
		stringa_parametri += f'Algoritmo: {self.partita_attuale.algoritmo_ricerca}\n'
		stringa_parametri += f'Pedine per generatrice: {self.partita_attuale.numero_pedine_prima_generatrice}\n'
		stringa_parametri += f'Valore pedina: {self.partita_attuale.valore_pedina_semplice}\n'
		stringa_parametri += f'Valore generatrice: {self.partita_attuale.valore_pedina_generatrice}\n'
		stringa_parametri += f'Valore moltiplicatore: {self.partita_attuale.valore_moltiplicatore_mangiata}\n'
		stringa_parametri += f'Valore generatrice più vicina: {self.partita_attuale.valore_generatrice_piu_vicina}\n'
		stringa_parametri += f'Punteggio elim. giocatore: {self.partita_attuale.valore_eliminazione_giocatore}\n'
		stringa_parametri += f'Punteggio vincita: {self.partita_attuale.valore_vincita_partita}\n'
		stringa_parametri += f'Punteggio elim. tutte gen.: {self.partita_attuale.valore_senza_generatrici}\n'
		stringa_parametri += f'Punteggio occup. casa base: {self.partita_attuale.valore_occupazione_casa_base}\n'
		stringa_parametri += f'Pesa punteggi pedine: {"SI" if self.partita_attuale.flag_pesa_punteggi_numero_pedine else "NO"}\n'
		stringa_parametri += f'Sposta casa base: {"SI" if self.partita_attuale.flag_sposta_casa_base else "NO"}\n'
		stringa_parametri += f'Mangiare proprie pedine: {"SI" if self.partita_attuale.flag_mangiare_proprie_pedine else "NO"}\n'
		stringa_parametri += f'Obbligatorietà mangiare: {"SI" if self.partita_attuale.flag_obbligatorieta_mangiare else "NO"}\n'
		if self.partita_attuale.flag_crea_damoni:
			stringa_parametri += f'Presenza damoni: SI\n'
			stringa_parametri += f'Valore damone: {self.partita_attuale.valore_pedina_damone}\n'
			stringa_parametri += f'Damone solo angolo: {"SI" if self.partita_attuale.flag_damone_solo_angolo else "NO"}\n'
			stringa_parametri += f'Damone mossa diagonale: {"SI" if self.partita_attuale.flag_damone_mossa_diagonale else "NO"}\n'
			stringa_parametri += f'Damone mangia in diagonale: {"SI" if self.partita_attuale.flag_damone_mangia_diagonale else "NO"}\n'
			stringa_parametri += f'Damone generatrice: {"SI" if self.partita_attuale.flag_damone_funzione_generatrice else "NO"}\n'
			stringa_parametri += f'Damone mangiato solo da damoni: {"SI" if self.partita_attuale.flag_damone_mangiato_solo_da_damoni else "NO"}'
		else:
			#dico semplicemente che non ci sono i damoni
			stringa_parametri += f'Presenza damoni: NO'
		#vado ad inserire la stringa
		self.grafica_ui.textBrowser_parametri.setText(stringa_parametri)
		

	def mostra_messaggio_info(self, titolo:str, testo:str):
		#mostro un messaggio all'utente informandolo
		msg = QtWidgets.QMessageBox()
		msg.setIcon(QtWidgets.QMessageBox.Information)
		msg.setText(testo)
		msg.setWindowTitle(titolo)
		msg.exec_()

	###################################################################################################################################################################
	#metodi per gestire la grafica generale

	#funzione per ridimensionare le celle della tabella
	def ridimensiona_celle_tabella(self):
		#metodo per ridimensionare le celle della tabella
		'''
		#recupero la dimensione dei q2uadrati
		dimensione_quadrati = self.grafica_ui.spinBox_dimensione_celle.value()
		#metto le dimensioni delle colonne
		for r in range(self.grafica_ui.tableWidget_schema_gioco.rowCount()):
			self.grafica_ui.tableWidget_schema_gioco.setRowHeight(r, dimensione_quadrati)
		for c in range(self.grafica_ui.tableWidget_schema_gioco.columnCount()):
			self.grafica_ui.tableWidget_schema_gioco.setColumnWidth(c, dimensione_quadrati)
		#adesso vado in tuti gli item a ridimensionare eventualmente l'icona
		for r in range(self.grafica_ui.tableWidget_schema_gioco.rowCount()):
			for c in range(self.grafica_ui.tableWidget_schema_gioco.columnCount()):
				item_posizione = self.grafica_ui.tableWidget_schema_gioco.item(r,c)
				icona = item_posizione.icon()
				if icona is not None:
					#ricavo l'immagine pixmap
					immagine_pixmap = icona.pixmap(dimensione_quadrati, dimensione_quadrati)
					#mi creo una nuova icona per quella casella
					nuova_icona = QtGui.QIcon(immagine_pixmap)
					item_posizione.setIcon(nuova_icona)
		'''
		self.aggiorna_schema_gioco()
					

	#metodi per controllare il numero dei giocatori
	def controlla_numero_giocatori_pc(self):
		#mi ricavo il numero di giocatori umani
		giocatori_umani = self.grafica_ui.spinBox_numero_giocatori_umani.value()
		#mi ricavo il numero di giocatori del pc
		giocatori_pc = self.grafica_ui.spinBox_numero_giocatori_pc.value()
		#se la somma supera 4 signifcia che devo modificarne uno
		#itero fino a quando il numero e inferiore a 2
		while (giocatori_pc + giocatori_umani) <2:
			self.grafica_ui.spinBox_numero_giocatori_umani.setValue(giocatori_umani +1)
			giocatori_umani += 1
		if (giocatori_pc + giocatori_umani) >4:
			#modifico quelli del pc
			self.grafica_ui.spinBox_numero_giocatori_pc.setValue(4 - giocatori_umani)
		#aggiorno l'algoritmo
		self.aggiorna_algoritmo_mosse_simulate()

	def controlla_numero_giocatori_umani(self):
		#mi ricavo il numero di giocatori umani
		giocatori_umani = self.grafica_ui.spinBox_numero_giocatori_umani.value()
		#mi ricavo il numero di giocatori del pc
		giocatori_pc = self.grafica_ui.spinBox_numero_giocatori_pc.value()
		#itero fino a quando il numero e inferiore a 2
		while (giocatori_pc + giocatori_umani) <2:
			self.grafica_ui.spinBox_numero_giocatori_pc.setValue(giocatori_pc +1)
			giocatori_pc += 1
		#se la somma supera 4 signifcia che devo modificarne uno
		if (giocatori_pc + giocatori_umani) >4:
			#modifico quelli del pc
			self.grafica_ui.spinBox_numero_giocatori_umani.setValue(4 - giocatori_pc)
		#aggiorno l'algoritmo
		self.aggiorna_algoritmo_mosse_simulate()

	'''
	#metodo per cambiare la stringaa per dire se si considera il numero delle pedine 
	def aggiorna_label_modalita_gioco(self, stringa):
		#mi ricavo quale sia il checkbox selezionato
		print('aggiorna_label_modalita_gioco')
		#mi ricavo il checkbox selezionato
		if self.grafica_ui.radioButton_numero_pedine.isChecked():
			stringa = self.grafica_ui.radioButton_numero_pedine.text()
		else:
			stringa = self.grafica_ui.radioButton_percentuale.text()
		#cambio la label
		self.grafica_ui.label_modalita_partita_scelta.setText(stringa)
	'''

	def aggiorna_algoritmo_mosse_simulate(self):
		#metodo per aggiornare l'algoritmo delle mosse utilizzate ed eventualmente disabilitare il combobox
		#poiche posso giocare anche con l'algoritmo di min/max per piu giocatori, ritorno subito
		return
		#l'algoritmo min max viene utilizzato solo in caso di 2 giocatori, per il resto viene disabilitato
		#mi ricavo il numero di giocatori umani
		giocatori_umani = self.grafica_ui.spinBox_numero_giocatori_umani.value()
		#mi ricavo il numero di giocatori del pc
		giocatori_pc = self.grafica_ui.spinBox_numero_giocatori_pc.value()
		if (giocatori_pc + giocatori_umani) == 2:
			#abilito il combobox per scelta
			self.grafica_ui.comboBox_modalita_mosse_simulate.setDisabled(False)
			#metto min/max
			self.grafica_ui.comboBox_modalita_mosse_simulate.setCurrentIndex(0)
		else:
			#giocatori diversi da 2 metto l'indice 1
			#metto minmax e disabilito combobox
			self.grafica_ui.comboBox_modalita_mosse_simulate.setCurrentIndex(1)
			#lo disabilito
			self.grafica_ui.comboBox_modalita_mosse_simulate.setDisabled(True)

	def verifica_valori_pedine(self):
		#metodo che verifica se le pedine hanno valori diversi, deve essere cosi
		valore_generatrice = self.grafica_ui.spinBox_valore_pedina_generatrice.value()
		valore_pedina = self.grafica_ui.spinBox_valore_pedina_semplice.value()
		valore_damone = self.grafica_ui.spinBox_valore_pedina_damone.value()
		if valore_generatrice <= valore_pedina:
			#aumento di uno quello della generatrice
			valore_generatrice = valore_pedina +1
			self.grafica_ui.spinBox_valore_pedina_generatrice.setValue(valore_generatrice)
		if valore_damone <= valore_generatrice:
			valore_damone = valore_generatrice +1
			self.grafica_ui.spinBox_valore_pedina_damone.setValue(valore_damone)

	#comando per iniziare una nuova partita
	def inizia_nuova_partita(self):
		print('inizia nuova partita')
		#prova per mandare in errore
		#variabile_errore = 1/0
		#mi ricavo i parametri che mi servono per il gioco
		dimensione_scacchiera = self.grafica_ui.spinBox_dimensione_scacchiera.value()
		#mi ricavo il numero dei giocatori
		giocatori_pc = self.grafica_ui.spinBox_numero_giocatori_pc.value()
		giocatori_umani = self.grafica_ui.spinBox_numero_giocatori_umani.value()
		#mi ricavo i valori delle pedine
		valore_pedina = self.grafica_ui.spinBox_valore_pedina_semplice.value()
		valore_generatrice = self.grafica_ui.spinBox_valore_pedina_generatrice.value()
		#recupero il tipo di algoritmo
		id_algoritmo = self.grafica_ui.comboBox_modalita_mosse_simulate.currentIndex()
		algoritmo = self.grafica_ui.comboBox_modalita_mosse_simulate.currentText()
		#recupero i valori di vincita ed eliminazione
		vincita_partia = self.grafica_ui.spinBox_punteggio_vincita_partita.value()
		eliminazione_giocatore = self.grafica_ui.spinBox_eliminazione_giocatore.value()
		#recupero i valori dei flag
		flag_auto_mangiare = self.grafica_ui.checkBox_mangiare_proprie.isChecked()
		flag_obbligo_mangiare = self.grafica_ui.checkBox_obbligatorio_mangiare.isChecked()
		#recupero il valore che indica dopo quante pedine si puo generare una generatrice
		pedine_generatrice = self.grafica_ui.spinBox_modalita_generatrici.value()
		valore_moltiplicatore = self.grafica_ui.doubleSpinBox_valore_moltiplicatore.value()
		valore_generatrice_vicina = self.grafica_ui.doubleSpinBox_valore_generatrice_piu_vicina.value()
		valore_occupazione_casa_base = self.grafica_ui.spinBox_valore_occupazione_casa_base.value()
		valore_senza_generatrici = self.grafica_ui.spinBox_senza_generatrici.value()
		flag_pesa_punteggio_pedine = self.grafica_ui.checkBox_pesa_pepunteggi_numero_pedine.isChecked()
		flag_sposta_casa_base = self.grafica_ui.checkBox_sposta_casa_base.isChecked()
		flag_usa_damoni = self.grafica_ui.checkBox_crea_damoni.isChecked()
		valore_damoni = self.grafica_ui.spinBox_valore_pedina_damone.value()
		flag_damoni_solo_angolo = self.grafica_ui.checkBox_damoni_solo_angolo.isChecked()
		flag_damoni_mossa_diagonale = self.grafica_ui.checkBox_damone_mossa_diagonale.isChecked()
		flag_damone_generatrice = self.grafica_ui.checkBox_damone_generatrice.isChecked()
		flag_damone_mangia_diagonale = self.grafica_ui.checkBox_damone_mangiare_diagonale.isChecked()
		flag_damone_mangiato_solo_damone = self.grafica_ui.checkBox_damone_mangiato_solo_da_damoni.isChecked()
		#posso crearmi l'oggetto per gestire il gioco
		self.id_partita_attuale += 1
		nuovo_gioco = GiocoAlgoritmi(dim_v=dimensione_scacchiera,
									dim_o=dimensione_scacchiera,
									numero_giocatori_umani=giocatori_umani,
									numero_giocatori_pc=giocatori_pc,
									valore_semplice=valore_pedina,
									valore_generatrice=valore_generatrice,
									#id_algoritmo=id_algoritmo,
									algoritmo=algoritmo,
									valore_eliminazione_giocatore=eliminazione_giocatore,
									valore_vincita_partita=vincita_partia,
									mangiare_proprie_pedine=flag_auto_mangiare,
									obbligatorio_mangiare=flag_obbligo_mangiare,
									pedine_prima_generatrice=pedine_generatrice,
									id_partita=self.id_partita_attuale,
									valore_moltiplicatore=valore_moltiplicatore,
									generatrice_piu_vicina=valore_generatrice_vicina,
									valore_occupazione_casa_base=valore_occupazione_casa_base,
									valore_senza_generatrici=valore_senza_generatrici,
									flag_pesa_punteggi=flag_pesa_punteggio_pedine,
									flag_sposta_casa_base=flag_sposta_casa_base,
									flag_crea_damoni=flag_usa_damoni,
									valore_damoni=valore_damoni,
									flag_damone_solo_angolo=flag_damoni_solo_angolo,
									flag_damoni_mossa_diagonale=flag_damoni_mossa_diagonale,
									flag_damoni_generatrice=flag_damone_generatrice,
									flag_damone_mangia_diagonale=flag_damone_mangia_diagonale,
									flag_damone_mangiato_solo_damoni=flag_damone_mangiato_solo_damone)
		#me lo salvo
		self.partita_attuale = nuovo_gioco
		self.is_partita_finita = False
		self.giocatore_attuale = -1
		if hasattr(self, "thread_attesa_mossa_pc"):
			self.thread_attesa_mossa_pc.killTimer(0)
			del self.thread_attesa_mossa_pc
		#se inizio una nuova partita mi rimuovo tutte le icone
		self.dizionario_immagini.clear()
		#vado a farmi la tabella di dimensioni prestabilite
		self.grafica_ui.tableWidget_schema_gioco.setColumnCount(dimensione_scacchiera)
		self.grafica_ui.tableWidget_schema_gioco.setRowCount(dimensione_scacchiera)
		#self.lista_celle_selezionate = []
		#mi calcolo la grafica
		#self.aggiorna_schema_gioco()
		#vado a mettere le informazioni per i giocatori per indicare chi sono
		#self.mostra_lista_giocatori()
		#inizio il primo turno
		self.mostra_lista_parametri() #i parametri li aggiorno una sola volta
		self.inizia_nuovo_turno()
		#vado nella tab dello schema
		self.grafica_ui.toolBox.setCurrentIndex(2)
		#vado a mettere la scoll in fondo
		#self.grafica_ui.page_5.scroll(0, 100)
		#massimizzo la finestra
		self.showMaximized()
		#self.scroll(100, 100)

	##############################################################################################################################################################################
	#metodo per gestire il click di una casella
	def gestisci_processo_click_casella(self, r:int, c:int) :
		#metodo che si occupa di gestire il processo di click con anche terminazione del turno se serve
		#se la partita e finita non faccio niente
		if not(self.is_partita_finita):
			giocatore_in_turno = self.partita_attuale.lista_giocatori[self.giocatore_attuale]
			flag_turno_finito = False
			flag_verifica_mosse_successive_mangiare = False
			#per prima cosa verifico se umano, altrimenti non faccio njiente
			if giocatore_in_turno.is_umano:
				if len(giocatore_in_turno.lista_celle_selezionate) == 0:
					#il giocatore non aveva selezionato ancora nessuna casella verifgico se la casella scelta appartiene al giocatore
					if (self.partita_attuale.schema_gioco[r][c] != Costanti.numero_casella_vuota) and ((self.partita_attuale.schema_gioco[r][c] % len(self.partita_attuale.lista_giocatori)) == giocatore_in_turno.id_giocatore):
						#allora e del giocatore, la seleziono
						giocatore_in_turno.lista_celle_selezionate.append((r,c))
				elif len(giocatore_in_turno.lista_celle_selezionate) == 1: 
					if (r,c) == giocatore_in_turno.lista_celle_selezionate[-1]:
						#ho cliccato nuovamente l'ultima quindi la tolgo da quelle selezionate solo se la lista delle celle ha un solo elemento
						giocatore_in_turno.lista_celle_selezionate.remove((r,c))
					else:
						#devo calcolare le mosse possibli e vedere se e una mossa valida
						lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_possibili_totale(schema_gioco=self.partita_attuale.schema_gioco, 
																			flag_mangiare_proprie_pedine=self.partita_attuale.flag_mangiare_proprie_pedine, 
																			numero_giocatori=len(self.partita_attuale.lista_giocatori), 
																			flag_obbligatorio_mangiare=self.partita_attuale.flag_obbligatorieta_mangiare,
																			flag_damone_mossa_diagonale=self.partita_attuale.flag_damone_mossa_diagonale,
																			valore_casella_damone=self.partita_attuale.valore_pedina_damone,
																			flag_damoni_mangiano_diagonale=self.partita_attuale.flag_damone_mangia_diagonale,
																			flag_damone_mangiato_solo_da_damoni=self.partita_attuale.flag_damone_mangiato_solo_da_damoni)
						if ( giocatore_in_turno.lista_celle_selezionate[-1], (r,c) ) in lista_mosse_disponibili:
							(r_attuale, c_attuale) = giocatore_in_turno.lista_celle_selezionate[-1]
							#allora aggiungo la cella alla lista e valuto se il turno e terminato
							giocatore_in_turno.lista_celle_selezionate.append((r,c))
							#eseguo la mossa nello schema, non si torna piu indietro
							punteggio_mossa = self.partita_attuale.esegui_mossa(origine= (r_attuale, c_attuale), destinazione=(r, c) )
							print(f'Punteggio giocatore {giocatore_in_turno.colore_traduzione}: {punteggio_mossa}')
							#adesso verifico se ha fatto una mossa semplice o se devo vedere se ci sono altri percorsi da mangiare
							if ( abs(r-r_attuale) == 1) or (abs(c-c_attuale) == 1 ):
								#mossa semplice
								flag_turno_finito = True
							else:
								#ho mangiato, verifico successivamente se il turno e finito
								flag_verifica_mosse_successive_mangiare = True
				else:
					#lunghezza almeno 2 quindi ha gia mangiato, verifico se la mossa e accettabile e in tal caso la faccio
					lista_mosse_disponibili = giocatore_in_turno.calcola_lista_mosse_per_mangiare(tabella_gioco=self.partita_attuale.schema_gioco, 
																	flag_mangiare_proprie_pedine=self.partita_attuale.flag_mangiare_proprie_pedine, 
																	numero_giocatori=len(self.partita_attuale.lista_giocatori),
																	flag_damoni_mangiano_diagonale=self.partita_attuale.flag_damone_mangia_diagonale,
																	valore_damone=self.partita_attuale.valore_pedina_damone,
																	flag_damone_mangiato_solo_da_damoni=self.partita_attuale.flag_damone_mangiato_solo_da_damoni)
					if ( giocatore_in_turno.lista_celle_selezionate[-1], (r,c) ) in lista_mosse_disponibili:
							(r_attuale, c_attuale) = giocatore_in_turno.lista_celle_selezionate[-1]
							#allora aggiungo la cella alla lista e valuto se il turno e terminato
							giocatore_in_turno.lista_celle_selezionate.append((r,c))
							#eseguo la mossa nello schema, non si torna piu indietro
							punteggio_mossa = self.partita_attuale.esegui_mossa(origine= (r_attuale, c_attuale), destinazione=(r, c) )
							print(f'Punteggio giocatore {giocatore_in_turno.colore_traduzione}: {punteggio_mossa}')
							#verifico se il turno e terminato
							flag_verifica_mosse_successive_mangiare = True
				#a questo punto verifico se devo guardare per mosse successive in base al flag delle mangiate
				if flag_verifica_mosse_successive_mangiare:
					lista_mosse_disponibili_mangiare = giocatore_in_turno.calcola_lista_mosse_per_mangiare(tabella_gioco=self.partita_attuale.schema_gioco,
																				flag_mangiare_proprie_pedine=self.partita_attuale.flag_mangiare_proprie_pedine, 
																				numero_giocatori=len(self.partita_attuale.lista_giocatori),
																				flag_damoni_mangiano_diagonale=self.partita_attuale.flag_damone_mangia_diagonale,
																				valore_damone=self.partita_attuale.valore_pedina_damone,
																				flag_damone_mangiato_solo_da_damoni=self.partita_attuale.flag_damone_mangiato_solo_da_damoni)
					#verifico se posso continuare a mangiare
					celle_successive = [ mossa[1] for mossa in lista_mosse_disponibili_mangiare if mossa[0] == giocatore_in_turno.lista_celle_selezionate[-1] ]
					#se non ho altre mosse ho finito il turno
					if len(celle_successive) == 0:
						flag_turno_finito = True
						#disabilito il pulsante di fine turno
						self.grafica_ui.pushButton_termina_turno.setDisabled(True)
					#altrimenti valuto se devo abilitare il pulsante di terminazione del turno
					elif not(self.partita_attuale.flag_obbligatorieta_mangiare):
						#abilito il pulsante
						self.grafica_ui.pushButton_termina_turno.setEnabled(True)
			#al termine aggiorno la grafica
			self.aggiorna_schema_gioco()
			#verifico se ho terminato il turno
			if flag_turno_finito:
				self.inizia_nuovo_turno()

	###############################################################################################################################################################################################
	#metodi per gestire l'inizio e la fine del turno
	

	
	def inizia_nuovo_turno(self):
		#metodo per iniziare un turno successivo
		#se la partita e finita non faccio niente
		if not(self.is_partita_finita):
			#verifico se sono stati eliminati dei giocatori che non ancora sono stati eliminati
			flag_trovato_eliminato = False
			for giocatore in self.partita_attuale.lista_giocatori:
				if not(giocatore.is_eliminato) and giocatore.verifica_giocatore_eliminato(tabella_gioco=self.partita_attuale.schema_gioco, numero_giocatori=len(self.partita_attuale.lista_giocatori)):
					#il giocatore i e stato eliminato
					giocatore.is_eliminato = True
					flag_trovato_eliminato = True
					#informo l'utente che il giocatore i e stato eliminato
					testo= f'Il giocatore {giocatore.id_giocatore+1} ({"umano" if giocatore.is_umano else "computer"}) ({giocatore.colore_traduzione}) è stato eliminato'
					self.mostra_messaggio_info(titolo='Giocatore eliminato', testo=testo)
			#adesso verifico se la partita e finita
			if flag_trovato_eliminato:
				lista_giocatori_attivi = [ i for i in self.partita_attuale.lista_giocatori if not(i.is_eliminato) ]
				if len(lista_giocatori_attivi) == 1:
					#rimasto un solo giocaotore, ha vinto
					giocatore_vincitore = lista_giocatori_attivi[0]
					testo = f'Ha vinto il giocatore {giocatore_vincitore.id_giocatore+1} ({"umano" if giocatore_vincitore.is_umano else "computer"}) ({giocatore_vincitore.colore_traduzione})'
					self.mostra_messaggio_info(titolo='Partita finita', testo=testo)
					#ritorno, non c'e altro da fare
					self.is_partita_finita = True
					return
			#al termine aumento il turno
			self.giocatore_attuale += 1
			#faccio il modulo al numero di giocatori
			self.giocatore_attuale = self.giocatore_attuale%len(self.partita_attuale.lista_giocatori)
			#inizio il successivo turno
			self.gestisci_turno()

	def gestisci_turno(self):
		#metodo per gestire il turno ed aggiornare la grafica
		#se la partita e finita non faccio niente
		if not(self.is_partita_finita):
			#mi ricavo il giocatoire in turno
			giocatore_in_tunro = self.partita_attuale.lista_giocatori[self.giocatore_attuale]
			#aggiorno la grafica
			testo = f'È il turno del giocatore {giocatore_in_tunro.colore_traduzione}'
			self.grafica_ui.label_giocatore_attuale.setText(testo)
			#svuoto la lista delle sue selezionate
			giocatore_in_tunro.lista_celle_selezionate.clear()
			flag_inizia_turno_successivo = False
			flag_fai_partire_thread = False
			#verifico che non sia stato eliminato
			if giocatore_in_tunro.is_eliminato:
				#cambio turno nuovamente
				flag_inizia_turno_successivo = True
				
				#self.inizia_nuovo_turno()
			else:
				#il giocatore non e stato eliminato, gli faccio fare il turno
				#verifico quante mosse puo fare, se non puo fare mosse il giocatore viene eliminato e tutte le sue pedien annullate
				lista_mosse_disponibili = giocatore_in_tunro.calcola_lista_mosse_possibili_totale(schema_gioco=self.partita_attuale.schema_gioco, 
																flag_mangiare_proprie_pedine=self.partita_attuale.flag_mangiare_proprie_pedine, 
																flag_obbligatorio_mangiare=self.partita_attuale.flag_obbligatorieta_mangiare, 
																numero_giocatori=len(self.partita_attuale.lista_giocatori),
																flag_damone_mossa_diagonale=self.partita_attuale.flag_damone_mossa_diagonale,
																valore_casella_damone=self.partita_attuale.valore_pedina_damone,
																flag_damoni_mangiano_diagonale=self.partita_attuale.flag_damone_mangia_diagonale,
																flag_damone_mangiato_solo_da_damoni=self.partita_attuale.flag_damone_mangiato_solo_da_damoni)
				if len(lista_mosse_disponibili) == 0:
					#elimino il giocatore
					giocatore_in_tunro.is_eliminato = True
					#informo l'utente che il giocatore i e stato eliminato
					testo= f'Il giocatore {giocatore_in_tunro.id_giocatore+1} ({"umano" if giocatore_in_tunro.is_umano else "computer"}) ({giocatore_in_tunro.colore_traduzione}) è stato eliminato perchè non ha mosse disponibili, le sue pedine verranno rimosse'
					self.mostra_messaggio_info(titolo='Giocatore eliminato', testo=testo)
					#rimuovo le pedine del giocatore
					self.partita_attuale.rimuovi_pedine_giocatore(id_giocatore=giocatore_in_tunro.id_giocatore)
					#verifico se la partita e stata vinta
					lista_giocatori_attivi = [ i for i in self.partita_attuale.lista_giocatori if not(i.is_eliminato) ]
					if len(lista_giocatori_attivi) == 1:
						#rimasto un solo giocaotore, ha vinto
						giocatore_vincitore = lista_giocatori_attivi[0]
						testo = f'Ha vinto il giocatore {giocatore_vincitore.id_giocatore+1} ({"umano" if giocatore_vincitore.is_umano else "computer"}) ({giocatore_vincitore.colore_traduzione})'
						self.mostra_messaggio_info(titolo='Partita finita', testo=testo)
						#dico che la partita e finita
						self.is_partita_finita = True
					else:
						#devo iniziare un nuovo turno
						flag_inizia_turno_successivo = True
				#se e umano non fa niente, se e pc fa il turno del pc
				elif giocatore_in_tunro.is_umano:
					#aggiorno la grafica
					self.aggiorna_schema_gioco()
				else:
					#deve gestire il turno del pc
					'''
					#inizio il turno successivo
					flag_inizia_turno_successivo = True
					#mi recupero il valore della profondita
					profondita_inserita = self.grafica_ui.spinBox_profondita.value()
					numero_massimo_thread = self.grafica_ui.spinBox_masssima_parallelizzazione.value()
					esito_mossa_pc = self.partita_attuale.calcola_mossa_pc(giocatore_attuale=self.giocatore_attuale, profondita=profondita_inserita, numero_massimo_thread=numero_massimo_thread)
					#adesso eseguo la serie di mosse calcolate
					self.partita_attuale.esegui_successione_mosse(lista_mosse=esito_mossa_pc.lista_mosse)
					#metto le caselle selezionate come mossa
					giocatore_in_tunro.lista_celle_selezionate=esito_mossa_pc.lista_mosse[:]
					'''
					#aggiorno la grafica
					self.aggiorna_schema_gioco()
					flag_inizia_turno_successivo = False
					#utilizzo i thread
					self.thread_attesa_mossa_pc = ThreadGrafica.ThreadGrafica(grafica=self)
					self.thread_attesa_mossa_pc.finished.connect(self.termina_mossa_computer)
					#self.connect(self.thread_attesa_mossa_pc,  QtCore.pyqtSignal("finished()"), self.termina_mossa_computer)
					#self.thread_attesa_mossa_pc.start()
					flag_fai_partire_thread = True
					
			#al termien aggiorno la grafica
			if flag_inizia_turno_successivo:
				self.inizia_nuovo_turno()
			#se devo far partire il thread adesso lo faccio partire
			if flag_fai_partire_thread:
				self.thread_attesa_mossa_pc.start()

	def termina_mossa_computer(self):
		#metodo per gestire il termine della mossa di un computer
		#praticamente attende il tempo necessario, aggiorna lo schema di gioco e inizia un nuovo turno
		
		#al termien aggiorno la grafica
		self.aggiorna_schema_gioco()
		self.inizia_nuovo_turno()

			

		
#script che contiene la classe per gestire il thread per la grafica con l'emissione dei segnali

from __future__ import annotations

import time

from PyQt5.QtCore import QThread, pyqtSignal
import GraficaDamaLineare

class ThreadGrafica(QThread):
	grafica_dama_lineare : GraficaDamaLineare.GraficaDamaLineare
	finished:pyqtSignal = pyqtSignal()

	def __init__(self, grafica:GraficaDamaLineare.GraficaDamaLineare) -> None:
		super().__init__()
		self.grafica_dama_lineare = grafica

	def run(self) -> None:
		#mi ricavo il giocatoire in turno
		#mi calcolo il tempo di inizio per non aspettare troppo
		tempo_inizio = time.time()
		#mi calcolo lo schema attuale cosi non rischio di fare casini
		prtita_in_gioco = self.grafica_dama_lineare.partita_attuale
		id_partita_iniziale = self.grafica_dama_lineare.id_partita_attuale
		schema_bk = [riga[:] for riga in prtita_in_gioco.schema_gioco]
		case_basi_iniziali = [ i.casa_base for i in prtita_in_gioco.lista_giocatori ]
		#mi calcolo anche il numero di pedine per ogni giocatore
		numero_pedine_aggiunte_dopo_generatrice = [ i.pedine_aggiunte_dopo_ultima_aggiunta for i in prtita_in_gioco.lista_giocatori ]
		lista_flag_eliminati_giocaotori=[ i.is_eliminato for i in prtita_in_gioco.lista_giocatori ]
		giocatore_in_tunro = prtita_in_gioco.lista_giocatori[self.grafica_dama_lineare.giocatore_attuale]
		#mi recupero il valore della profondita
		profondita_inserita = self.grafica_dama_lineare.grafica_ui.spinBox_profondita.value()
		numero_massimo_thread = self.grafica_dama_lineare.grafica_ui.spinBox_masssima_parallelizzazione.value()
		esito_mossa_pc = prtita_in_gioco.calcola_mossa_pc(giocatore_attuale=self.grafica_dama_lineare.giocatore_attuale, profondita=profondita_inserita, numero_massimo_thread=numero_massimo_thread)
		#queste operazioni le eseguo se la partita e ancora la stessa
		if self.grafica_dama_lineare.partita_attuale.id_partita == id_partita_iniziale:
			#adesso eseguo la serie di mosse calcolate
			#mi ripristino lo schema ed eseguo la mossa
			prtita_in_gioco.ripristina_schema(schema_bk=schema_bk, numero_pedine=numero_pedine_aggiunte_dopo_generatrice, lista_eliminati=lista_flag_eliminati_giocaotori, lista_case_basi=case_basi_iniziali)
			prtita_in_gioco.esegui_successione_mosse(lista_mosse=esito_mossa_pc.lista_mosse)
			print(f'{giocatore_in_tunro.colore_traduzione}: {esito_mossa_pc.lista_mosse}')
			#metto le caselle selezionate come mossa
			giocatore_in_tunro.lista_celle_selezionate=esito_mossa_pc.lista_mosse[:]
			print('fine calcolo mossa thread')
			#termino il thread
			#ricavo il tempo di attesa
			tempo_attesa = self.grafica_dama_lineare.grafica_ui.spinBox_tempo_mossa_computer.value()
			#attendo quel tempo
			#print(f'inizio attesa computer per {tempo_attesa} secondi')
			while (time.time() - tempo_inizio) < tempo_attesa:
				pass
			self.finished.emit()
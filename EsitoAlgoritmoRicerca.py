#classe per gestire l'esito degli algoritmi di ricerca

from typing import List, Tuple


class EsitoAlgoritmoRicerca:
	punteggio : float
	lista_mosse : List[Tuple[int, int]]
	matrice_finale : List[List[int]]
	profondita_matrice_finale : int
	lista_punteggi_giocatori : List[float]
	numero_mosse_per_vincere:float
	numero_percorsi: int #numero che indica il numero di percorsi di biforcazione successivi

	def __init__(self, numero_giocatori:int=0) -> None:
		self.punteggio = 0
		self.lista_mosse = []
		self.matrice_finale = []
		self.profondita_matrice_finale = 0
		self.lista_punteggi_giocatori = [0 for i in range(numero_giocatori)]

	def calcola_punteggio(self, id_giocatore:int):
		#metodo per popolarsi il punteggio 
		#il punteggio del giocatore selezionato viene sommato, gli altri vengono sottratti
		#print(self.lista_punteggi_giocatori)
		self.punteggio = self.lista_punteggi_giocatori[id_giocatore]
		self.punteggio -= sum( [ self.lista_punteggi_giocatori[i] for i in range(len(self.lista_punteggi_giocatori)) if i!= id_giocatore ] )